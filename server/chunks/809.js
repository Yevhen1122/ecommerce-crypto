exports.id = 809;
exports.ids = [809];
exports.modules = {

/***/ 1750:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ ImageCropper)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2706);
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(cropperjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var cropperjs_dist_cropper_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7676);
/* harmony import */ var cropperjs_dist_cropper_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(cropperjs_dist_cropper_min_css__WEBPACK_IMPORTED_MODULE_3__);




function ImageCropper({ src , maxWidth , maxHeight , onCrop  }) {
    const [isCropping, setIsCropping] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const cropperRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (cropperRef.current) {
            const cropper = new (cropperjs__WEBPACK_IMPORTED_MODULE_2___default())(cropperRef.current, {
                aspectRatio: maxWidth / maxHeight,
                viewMode: 1,
                ready () {
                    cropper.setDragMode("crop");
                }
            });
            cropperRef.current.cropper = cropper;
            return ()=>{
                cropper.destroy();
            };
        }
    }, [
        cropperRef,
        maxWidth,
        maxHeight
    ]);
    const handleCrop = ()=>{
        if (!cropperRef.current) return;
        const canvas = cropperRef.current.cropper?.getCroppedCanvas({
            width: maxWidth,
            height: maxHeight
        });
        canvas?.toBlob((blob)=>{
            onCrop(blob);
            setIsCropping(false);
        });
    };
    const handleCancel = ()=>{
        setIsCropping(false);
    };
    const handleImageLoad = (event)=>{
        const { naturalWidth , naturalHeight  } = event.currentTarget;
        if (naturalWidth > maxWidth || naturalHeight > maxHeight) {
            setIsCropping(true);
        }
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: isCropping ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    ref: cropperRef,
                    src: src,
                    alt: "Image to crop",
                    onLoad: handleImageLoad
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                    onClick: handleCrop,
                    children: "Crop"
                })
            ]
        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
            src: src,
            alt: "Image"
        })
    });
}


/***/ }),

/***/ 9690:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ ProtectedRoute)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8762);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1083);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_3__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_4__]);
([_context_authcontext__WEBPACK_IMPORTED_MODULE_3__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);





function ProtectedRoute({ children  }) {
    const { user  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_3__/* .useAuth */ .a)();
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(true);
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_4__/* .useUserData */ .v)();
    // based on userType go to company or users
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        if (!user && !userData && user?.uid === null) {
            router.push("/login");
        } else {
            setLoading(false);
        }
    }, [
        user
    ]);
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: user ? children : null
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7676:
/***/ (() => {



/***/ })

};
;