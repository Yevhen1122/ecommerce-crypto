"use strict";
exports.id = 329;
exports.ids = [329];
exports.modules = {

/***/ 5329:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "K": () => (/* binding */ Storage)
/* harmony export */ });
/* unused harmony export keyName */
const keyName = "discord-props";
function parseJSON(item) {
    let parsedItem;
    try {
        if (item) parsedItem = JSON.parse(item);
    } catch (error) {
        // Parsing error occurred, item is not a valid stringified JSON
        parsedItem = item;
    }
    return parsedItem;
}
const getItem = (key)=>{
    try {
        const item = localStorage.getItem(key);
        const parsedItem = parseJSON(item);
        return parsedItem;
    } catch (error) {
        console.error(`Error retrieving item from localStorage for key "${key}":`, error);
        return null;
    }
};
const Storage = {
    setInviteCode: (inviteCode)=>{
        if (inviteCode) localStorage.setItem(keyName, inviteCode);
    },
    getInviteCode: ()=>{
        return getItem(keyName);
    },
    setDiscordAccessToken: (accessToken)=>{
        localStorage.setItem(`${keyName}-TOKEN`, JSON.stringify(accessToken));
    },
    setDiscordRefreshToken: (refreshToken)=>{
        localStorage.setItem(`${keyName}-REFRESH-TOKEN`, JSON.stringify(refreshToken));
    },
    getDiscordAccessToken: ()=>{
        return getItem(`${keyName}-TOKEN`);
    },
    getDiscordRefreshToken: ()=>{
        if (false) {} else {
            return "";
        }
    },
    setUserData: (userData)=>{
        localStorage.setItem(`${keyName}-USER`, userData);
    },
    getUserData: ()=>{
        return getItem(`${keyName}-USER`);
    },
    setGiveawayId: (giveawayId)=>{
        localStorage.setItem("url", giveawayId);
    },
    getGiveawayId: ()=>{
        return getItem("url");
    },
    clearAll: ()=>{
        localStorage.removeItem(keyName);
        localStorage.removeItem(`${keyName}-TOKEN`);
        localStorage.removeItem(`${keyName}-USER`);
    },
    setTwitterAuth_1: (data)=>{
        localStorage.setItem("twitter-auth-1", JSON.stringify(data));
    },
    getTwitterAuth_1: ()=>getItem("twitter-auth-1"),
    setTwitterAuthToken: (data)=>localStorage.setItem("twitter-auth-token", JSON.stringify(data)),
    getTwitterAuthToken: ()=>getItem("twitter-auth-token"),
    setTweetData: (data)=>localStorage.setItem("tweet-data", JSON.stringify(data)),
    getTweetData: ()=>getItem("tweet-data"),
    clearTwitterData: ()=>{
        localStorage.removeItem("tweet-data");
        localStorage.removeItem("twitter-auth-1");
    },
    setTwitterFollowerData: (userId)=>localStorage.setItem("follow-data", JSON.stringify(userId)),
    getFollowerData: ()=>getItem("follow-data"),
    setIsRetry: (statue)=>localStorage.setItem("isRetry", JSON.stringify(statue.toString())),
    getIsRetry: ()=>getItem("isRetry")
};


/***/ })

};
;