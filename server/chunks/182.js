exports.id = 182;
exports.ids = [182];
exports.modules = {

/***/ 6630:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "F": () => (/* binding */ twitterApi),
/* harmony export */   "v": () => (/* binding */ twitterAuth)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
/* harmony import */ var _helpers_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5329);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__]);
axios__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const twitterAuth = {
    login: async ()=>{
        const result = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/authorize");
        console.log(result);
        _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setTwitterAuth_1 */ .K.setTwitterAuth_1(result.data);
        if (result.data.url) {
            window.open(result.data.url, "_self");
        }
    },
    getTwitterToken: async (code, state)=>{
        const twitterData = _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuth_1 */ .K.getTwitterAuth_1();
        if (twitterData) {
            try {
                const { codeVerifier , state: sessionState  } = twitterData;
                const payload = {
                    codeVerifier,
                    state,
                    code,
                    sessionState
                };
                const data = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/authorize", payload);
                _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setTwitterAuthToken */ .K.setTwitterAuthToken(data.data);
                console.log(data.data);
                return {
                    hasError: false,
                    status: "success"
                };
            } catch (error) {
                return {
                    hasError: true,
                    status: "error" + error
                };
            }
        }
    }
};
const twitterApi = axios__WEBPACK_IMPORTED_MODULE_0__["default"].create({
    baseURL: "/api/twitter-v2",
    headers: {
        "Content-Type": "application/json"
    }
});
twitterApi.interceptors.response.use((response)=>response, async (error)=>{
    debugger;
    const request = error.config;
    const { code: status  } = error.response.data.error;
    const isRetry = _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getIsRetry */ .K.getIsRetry();
    if (status == 403 && !isRetry) {
        _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setIsRetry */ .K.setIsRetry(true);
        const isSuccess = await refreshToken();
        if (isSuccess) return (0,axios__WEBPACK_IMPORTED_MODULE_0__["default"])(request);
    }
    if (status == 401 && !isRetry) {
        _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setIsRetry */ .K.setIsRetry(true);
        twitterAuth.login();
    }
    return Promise.reject(error.response);
});
const refreshToken = async ()=>{
    const userData = await _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
    const payload = {
        refreshToken: userData?.refreshToken,
        token: userData?.accessToken,
        method: "refreshToken"
    };
    try {
        const response = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].post(`/twitter-v2`, payload);
        const { data  } = response;
        return true;
    } catch (e) {
        return false;
    }
};

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5960:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "C": () => (/* binding */ showToast),
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6201);
/* harmony import */ var _helpers_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5329);
/* harmony import */ var _twitterAuth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6630);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_hot_toast__WEBPACK_IMPORTED_MODULE_0__, _twitterAuth__WEBPACK_IMPORTED_MODULE_2__]);
([react_hot_toast__WEBPACK_IMPORTED_MODULE_0__, _twitterAuth__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
    tweet: async (status)=>{
        try {
            const token = await _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            const response = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.post */ .F.post("/", {
                status,
                token: token?.accessToken,
                method: "tweet"
            });
            return response.data;
        } catch (error) {
            // Handle the error and show a toast notification
            console.error("Error in tweet:", error);
            // Show the error toast
            showToast("An error occurred while tweeting.");
            throw error; // Optional: Rethrow the error if necessary
        }
    },
    retweet: async (tweetId)=>{
        debugger;
        try {
            const userData = await _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            const response = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.put */ .F.put("/", {
                loggedUserId: userData?.currentUser.id,
                tweetId,
                token: userData?.accessToken,
                method: "retweet"
            });
            return response.data;
        } catch (error) {
            // Handle the error and show a toast notification
            console.error("Error in retweet:", error);
            // Show the error toast
            // showToast("An error occurred while retweeting.");
            throw error; // Optional: Rethrow the error if necessary
        }
    },
    followUser: async (userId)=>{
        debugger;
        const username = userId.match(/twitter\.com\/([^/]+)/i)[1];
        try {
            const userData = await _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            const userInfo = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.post */ .F.post("/", {
                token: userData?.accessToken,
                username,
                method: "userInfo"
            });
            console.log(userInfo);
            const userId1 = userInfo.data.data.id;
            const isFollowingRes = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.post */ .F.post("/", {
                token: userData?.accessToken,
                method: "isFollowing",
                userId: userId1
            });
            const isFollow = isFollowingRes.data.data.some((user)=>user.username === userData?.currentUser.username);
            if (isFollow) {
                return {
                    data: {
                        following: true,
                        pending_follow: false
                    }
                };
            }
            _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setTwitterFollowerData */ .K.setTwitterFollowerData(userId1);
            const response = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.patch */ .F.patch("/", {
                loggedUserId: userData?.currentUser.id,
                userId: userId1,
                token: userData?.accessToken,
                method: "followUser"
            });
            return response.data;
        } catch (error) {
            // Handle the error and show a toast notification
            console.error("Error in followUser:", error);
            // Show the error toast
            // showToast("An error occurred while following the user.");
            throw error; // Optional: Rethrow the error if necessary
        }
    },
    isFollowing: async (userId)=>{
        const userData = await _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
        const isFollowingRes = await _twitterAuth__WEBPACK_IMPORTED_MODULE_2__/* .twitterApi.post */ .F.post("/", {
            token: userData?.accessToken,
            method: "isFollowing",
            userId
        });
        console.log(isFollowingRes);
    }
});
function showToast(message) {
    // Implement your toast notification logic here
    // This function is just a placeholder
    react_hot_toast__WEBPACK_IMPORTED_MODULE_0__.toast.error(message);
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 4602:
/***/ (() => {



/***/ })

};
;