exports.id = 756;
exports.ids = [756];
exports.modules = {

/***/ 4869:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/add_icon.8935fe70.svg","height":40,"width":40});

/***/ }),

/***/ 6881:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/giftbox_big.4a80c8b7.svg","height":210,"width":210});

/***/ }),

/***/ 3303:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/info_icon.a1edea57.svg","height":24,"width":24});

/***/ }),

/***/ 9251:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/link-2.6e481f2b.svg","height":16,"width":16});

/***/ }),

/***/ 2109:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/search_icon.d368d700.svg","height":20,"width":20});

/***/ }),

/***/ 2612:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/viewAll_arrow_icon.cdc0f797.svg","height":12,"width":20});

/***/ }),

/***/ 3234:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/xmark.b865ba28.png","height":16,"width":16,"blurDataURL":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAMAAADz0U65AAAAGFBMVEX///////9MaXH///////////////////9oteHBAAAAB3RSTlPHEQC4QbUl73yVWwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAC9JREFUeJxFi0EKADEQg0xmQv7/47IstDdBZUe2ZkmRaBCFIixa5AdXzR8Pm2/PHhc0AKUp4shiAAAAAElFTkSuQmCC","blurWidth":8,"blurHeight":8});

/***/ }),

/***/ 2723:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ CountryItem)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8762);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_2__]);
_context_authcontext__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];



function CountryItem(title) {
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_2__/* .useAuth */ .a)();
    // remove title from restricted countries on click
    const removeCountry = (title)=>{
        const newCountries = data?.restrictedCountries?.filter((country)=>country.name !== title.title);
        setFormValues({
            restrictedCountries: newCountries
        });
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "flex items-center justify-between",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                    className: " text-base leading-5",
                    children: title?.title
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                    onClick: ()=>removeCountry(title),
                    className: "text-red text-base leading-5 underline underline-offset-2 decoration-1",
                    children: "Remove"
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 1192:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ CountrySearch)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown_select__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8787);
/* harmony import */ var react_dropdown_select__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown_select__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_assets_images_search_icon_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2109);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _models_country__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9034);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8762);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_6__]);
_context_authcontext__WEBPACK_IMPORTED_MODULE_6__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];







function CountrySearch() {
    const [selected, setSelected] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_6__/* .useAuth */ .a)();
    // updated selected if it gets changed in data.restrictedCountries
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        // compare selected and data.restrictedCountries.name if not similar updated selected
        if (data?.restrictedCountries?.length !== selected?.length) {
            setSelected(data?.restrictedCountries);
        }
    }, [
        data?.restrictedCountries
    ]);
    // setFormValues when selected got changed
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (selected?.length > 0) setFormValues({
            restrictedCountries: selected
        });
    }, [
        selected
    ]);
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "relative rounded-md shadow-sm ",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "w-full text-black leading-[20px] border-[1.5px] border-jade-100 rounded-full bg-white placeholder:text-black",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "absolute top-[10px] left-[15px]",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default()), {
                        className: "text-black border-jade-100 bg-teal-900 ",
                        src: _public_assets_images_search_icon_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                        alt: "search_icon",
                        layout: "intrinsic"
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_dropdown_select__WEBPACK_IMPORTED_MODULE_2___default()), {
                    style: {
                        border: "none",
                        backgroundColor: "transparent",
                        outline: "transparent",
                        boxShadow: "none",
                        borderRadius: "0",
                        textAlign: "left",
                        padding: "0 0 0 52px"
                    },
                    options: _models_country__WEBPACK_IMPORTED_MODULE_5__/* .countries */ .h,
                    placeholder: "Select Restricted Countries",
                    labelField: "name",
                    valueField: "name",
                    onChange: (values)=>setSelected(values),
                    searchBy: "name",
                    keepSelectedInList: true,
                    clearOnBlur: false,
                    clearOnSelect: false,
                    dropdownGap: 4,
                    multi: true,
                    values: selected
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 1026:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ PostEntry)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1492);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6201);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8762);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1083);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(236);
/* harmony import */ var _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2987);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, _firebase__WEBPACK_IMPORTED_MODULE_7__, _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_8__]);
([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, _firebase__WEBPACK_IMPORTED_MODULE_7__, _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_8__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);









function PostEntry({ formStep , nextFormStep , prevFormStep  }) {
    const { data , setFormValues , user , isNotificationEnabled , setIsNotificationEnable , isEmailEnabled , setIsEmailEnabled  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_5__/* .useAuth */ .a)();
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_6__/* .useUserData */ .v)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    // const [isNotificationEnabled, setIsNotificationEnable] = useState(false);
    // const [isEmailEnabled, setIsEmailEnabled] = useState(false);
    function generateUID() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    function reSetNotifications() {
        setIsEmailEnabled(false);
        setIsNotificationEnable(false);
    }
    // to save as draft
    const onSubmitAsTemplate = async ()=>{
        // add a property to data name draft
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            template: true,
            draft: false,
            participatedUsers: [],
            winners: [],
            status: "open",
            totalEntries: 0,
            isNotificationEnabled: true,
            isEmailEnabled: true,
            closedAt: {}
        };
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.setDoc)(ref, tempData);
            if (userData?.isNotificationEnabled || tempData?.isNotificationEnabled) {
                await (0,_utils_campaignEvents__WEBPACK_IMPORTED_MODULE_8__/* .handleNotification */ .R)(userData, tempData, user);
            }
            // if (userData?.isEmailEnabled || tempData?.isEmailEnabled) {
            //   await sendEmail(userData, tempData, user);
            // }
            reSetNotifications();
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].success("GiveAway Saved as Template");
        } catch (e) {
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error(e);
        }
        setFormValues("submitted");
        console.log(data);
        next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
    };
    const goToPreview = ()=>{
        next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway/preview");
    };
    async function submitCampaign() {
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            template: data.template === true || data.template === undefined || data.template === null ? false : data.template,
            draft: false,
            participatedUsers: [],
            winners: [],
            status: "open",
            totalEntries: 0,
            isNotificationEnabled: true,
            isEmailEnabled: true,
            closedAt: {}
        };
        console.log("this is data", tempData);
        setLoading(true);
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.setDoc)(ref, tempData);
            if (userData?.isNotificationEnabled || tempData?.isNotificationEnabled) {
                await (0,_utils_campaignEvents__WEBPACK_IMPORTED_MODULE_8__/* .handleNotification */ .R)(userData, tempData, user, "updated");
            }
            // if (userData?.isEmailEnabled || tempData?.isEmailEnabled) {
            //   await sendEmail(userData, tempData, user, "updated");
            // }
            setLoading(false);
            reSetNotifications();
            next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].success("GiveAway Created");
        } catch (e) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error(e);
        }
        setLoading(false);
        setFormValues("submitted");
    }
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "flex flex-col lg:flex-row",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "w-full lg:max-w-[594px]",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "space-y-6 md:space-y-10",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "border-[1.5px] border-jade-100 rounded-md",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "p-6 md:py-[30px] md:px-8",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                            className: "text-[20px] leading-6 font-familyMedium mb-6 md:mb-8 lg:text-xl",
                                            children: "Email This Giveaway to your Followers"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                id: "email",
                                                type: "checkbox",
                                                className: "inputCheckbox"
                                            })
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "p-6 md:py-[30px] md:px-8",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                            className: "text-[20px] leading-6 font-familyMedium mb-6 md:mb-8 lg:text-xl",
                                            children: "Send out Notification about this giveaway"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                id: "notis",
                                                type: "checkbox",
                                                className: "inputCheckbox"
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto px-1",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                onClick: onSubmitAsTemplate,
                                className: "buttonPrimary",
                                children: "Save as a New Template"
                            }),
                            !data?.template && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                onClick: goToPreview,
                                className: "buttonPrimary",
                                children: "Preview"
                            }),
                            data?.template && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                onClick: submitCampaign,
                                className: "buttonPrimary",
                                children: "Publish"
                            })
                        ]
                    })
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7883:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ PrizeItem)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/legacy/image.js
var legacy_image = __webpack_require__(9755);
var image_default = /*#__PURE__*/__webpack_require__.n(legacy_image);
;// CONCATENATED MODULE: ./public/assets/images/giftbox.svg
/* harmony default export */ const giftbox = ({"src":"/_next/static/media/giftbox.297d553f.svg","height":54,"width":54});
// EXTERNAL MODULE: ./public/assets/images/viewAll_arrow_icon.svg
var viewAll_arrow_icon = __webpack_require__(2612);
;// CONCATENATED MODULE: ./components/PrizeItem.tsx





function PrizeItem(props) {
    return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "border-[1.5px] border-jade-100 rounded-md p-4",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "flex gap-x-4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "border-[1.5px] border-jade-100 rounded-2sm flex items-center justify-center p-4",
                        children: props.prize?.image ? /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                            src: props.prize?.image,
                            alt: "gift_box",
                            width: 54,
                            height: 54
                        }) : /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                            width: 54,
                            height: 54,
                            src: giftbox,
                            alt: "gift_box",
                            layout: "intrinsic"
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "flex flex-col justify-between",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                        className: " text-base leading-5 font-Ubuntu-Medium mb-2",
                                        children: props.prize?.prizeName
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h6", {
                                        className: " text-sm font-Ubuntu-Medium",
                                        children: [
                                            "(",
                                            props.prize?.value,
                                            ")"
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                className: "flex items-center mt-auto md:justify-center",
                                onClick: props.onEdit,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        className: "text-xs uppercase mr-2.5",
                                        children: "View prize"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                        src: viewAll_arrow_icon/* default */.Z,
                                        alt: "arrow_icon",
                                        layout: "fixed",
                                        width: 18
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: "flex items-center mt-auto md:justify-center",
                                onClick: props.onDelete,
                                children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: "text-xs uppercase mr-2.5",
                                    children: "Delete"
                                })
                            })
                        ]
                    })
                ]
            })
        })
    });
}


/***/ }),

/***/ 4474:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Prizes)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3069);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _PrizeItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7883);
/* harmony import */ var _public_assets_images_giftbox_big_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6881);
/* harmony import */ var _public_assets_images_add_icon_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4869);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4602);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8762);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5641);
/* harmony import */ var _public_assets_images_xmark_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3234);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3392);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(236);
/* harmony import */ var _loader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3628);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(6201);
/* harmony import */ var _ImageCopper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(1750);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hook_form__WEBPACK_IMPORTED_MODULE_9__, firebase_storage__WEBPACK_IMPORTED_MODULE_11__, _firebase__WEBPACK_IMPORTED_MODULE_12__, react_hot_toast__WEBPACK_IMPORTED_MODULE_14__]);
([_context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hook_form__WEBPACK_IMPORTED_MODULE_9__, firebase_storage__WEBPACK_IMPORTED_MODULE_11__, _firebase__WEBPACK_IMPORTED_MODULE_12__, react_hot_toast__WEBPACK_IMPORTED_MODULE_14__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);
















function Prizes({ formStep , nextFormStep , prevFormStep  }) {
    // FOR POPUP
    const [open, setOpen] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [edited, setEdited] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [index, setIndex] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_8__/* .useAuth */ .a)();
    const [prizes, setPrizes] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.prizes || []);
    const [imageToPost, setImageToPost] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    const inputRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const filepickerRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const [percent, setPercent] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [imageEdit, setImageEdit] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_9__.useForm)({
        mode: "onBlur"
    });
    const { register , handleSubmit , formState: { errors  } , watch , setValue , reset  } = methods;
    const formValues = watch();
    const [selectedImage, setSelectedImage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const [imageLoading, setImageLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [croppedImage, setCroppedImage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const handleClick = (event)=>{
        // 👉️ ref could be null here
        if (inputRef.current != null) {
            inputRef.current.click();
        }
    };
    // load data from parent or manage when its between
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            if (data?.prize && data?.prize.length > 0 && data?.prize != null) {
                setPrizes(data?.prize);
            }
        }, 10);
    }, [
        data
    ]);
    // update Data when prizes updated
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            setFormValues({
                prize: prizes
            });
        }, 1);
    }, [
        prizes
    ]);
    const onOpenModal = ()=>{
        setOpen(true);
    };
    const onCloseModal = ()=>{
        setOpen(false);
        reset();
    };
    const onEdit = (index)=>{
        console.log(prizes[index]);
        setEdited(true);
        setImageEdit(true);
        setIndex(index);
        setValue("prizeName", prizes[index]?.prizeName);
        setValue("description", prizes[index]?.description);
        setValue("noOfWinners", prizes[index]?.noOfWinners);
        setValue("value", prizes[index]?.value);
        setSelectedImage(prizes[index]?.image);
        onOpenModal();
    };
    const handleNext = (data)=>{
        nextFormStep();
    };
    const handlePrev = (data)=>{
        prevFormStep();
    };
    const imageChange = (e)=>{
        const file = e.target.files[0];
        if (file) {
            // check image size
            if (file.size > 10000000) {
                react_hot_toast__WEBPACK_IMPORTED_MODULE_14__["default"].error("Image size should be less than 1MB");
                return;
            }
            // check image type
            if (file.type !== "image/png" && file.type !== "image/jpg" && file.type !== "image/jpeg") {
                react_hot_toast__WEBPACK_IMPORTED_MODULE_14__["default"].error("Image type should be png, jpg or jpeg");
                return;
            }
            const img = document.createElement("img");
            img.src = URL.createObjectURL(file);
            img.onload = ()=>{
                const width = img.naturalWidth;
                const height = img.naturalHeight;
                const aspectRatio = width / height;
                URL.revokeObjectURL(img.src);
                if (width > 400 || height > 400 || aspectRatio !== 1) {
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_14__["default"].error("Please upload an image with a 1:1 aspect ratio.");
                    setCroppedImage(file);
                    setImageLoading(true);
                    return;
                } else {
                    setSelectedImage(file);
                    setImageToPost(URL.createObjectURL(file));
                }
            };
        }
    };
    const handleCrop = (image)=>{
        //console.log('Test', formValues);
        const croppedImageFile = new File([
            image
        ], Math.random().toString(36).substring(2, 15) + ".png");
        setSelectedImage(croppedImageFile);
        setImageToPost(URL.createObjectURL(croppedImageFile));
        setCroppedImage(null);
        setImageLoading(false);
        setImageEdit(false);
    };
    // handle close crop image
    const handleClose = ()=>{
        setCroppedImage(null);
        setImageLoading(false);
        setSelectedImage(null);
        setImageToPost(null);
        setImageEdit(false);
    };
    const removeSelectedImage = ()=>{
        setSelectedImage(undefined);
        setImageEdit(false);
    };
    const onSubmit = async ()=>{
        const formData = formValues;
        //console.log(formData);
        if (edited) {
            console.log("edit here ", formData);
            if (prizes[index]?.image !== formData.image || prizes[index]?.image !== URL.createObjectURL(selectedImage)) {
                // update the image in backend
                console.log("edit here image ", formData);
                const storageRef = (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.ref)(_firebase__WEBPACK_IMPORTED_MODULE_12__/* .storage */ .tO, `/files/${selectedImage?.name || "random"}`); // progress can be paused and resumed. It also exposes progress updates. // Receives the storage reference and the file to upload.
                const uploadTask = (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.uploadBytesResumable)(storageRef, selectedImage);
                uploadTask.on("state_changed", (snapshot)=>{
                    setLoading(true);
                    const percent = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100); // update progress
                    setPercent(percent);
                });
                await (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.getDownloadURL)(uploadTask.snapshot.ref).then(async (url)=>{
                    setImageToPost(url);
                    formData.image = await url;
                    prizes[index] = formData;
                    setPrizes(prizes);
                    setEdited(false);
                    reset();
                    setSelectedImage(null);
                    setLoading(false);
                });
            } else {
                console.log("edit here no image updated ", formData);
                prizes[index] = formData;
                setPrizes(prizes);
                setEdited(false);
                setLoading(false);
                reset();
            }
        } else {
            if (selectedImage) {
                const storageRef1 = (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.ref)(_firebase__WEBPACK_IMPORTED_MODULE_12__/* .storage */ .tO, `/files/${selectedImage?.name || "random"}`); // progress can be paused and resumed. It also exposes progress updates. // Receives the storage reference and the file to upload.
                const uploadTask1 = (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.uploadBytesResumable)(storageRef1, selectedImage);
                uploadTask1.on("state_changed", (snapshot)=>{
                    setLoading(true);
                    const percent = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100); // update progress
                    setPercent(percent);
                }, (err)=>console.log(err), ()=>{
                    // download url
                    (0,firebase_storage__WEBPACK_IMPORTED_MODULE_11__.getDownloadURL)(uploadTask1.snapshot.ref).then(async (url)=>{
                        setImageToPost(url);
                        formData.image = await url;
                        const newPrizes = [
                            ...prizes,
                            formData
                        ];
                        setPrizes(newPrizes);
                        reset();
                        setSelectedImage(null);
                        setLoading(false);
                    });
                });
            } else {
                const newPrizes = [
                    ...prizes,
                    formData
                ];
                setPrizes(newPrizes);
                reset();
                setSelectedImage(null);
            }
        }
        onCloseModal();
    };
    function onDelete(index) {
        const newPrizes = [
            ...prizes
        ];
        newPrizes.splice(index, 1);
        setPrizes(newPrizes);
    }
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_loader__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                show: loading
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex justify-between",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                className: "text-xl font-Ubuntu-Medium mb-6",
                                children: "Current prizes"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                className: "buttonPrimary",
                                onClick: onOpenModal,
                                children: "Add prize"
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "lg:border-t-[1.5px] lg:border-jade-100 mt-5 lg:pt-6",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "grid grid-cols-1 items-center gap-y-6 md:gap-x-6 md:gap-y-7 md:grid-cols-2 lg:grid-cols-3",
                            children: prizes.map((prize, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_PrizeItem__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                                    prize: prize,
                                    index: index,
                                    onEdit: ()=>{
                                        onEdit(index);
                                    },
                                    onDelete: ()=>{
                                        onDelete(index);
                                    }
                                }, index))
                        })
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto py-12 ",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                    children: [
                        formStep > 0 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            onClick: handlePrev,
                            className: "buttonPrimary",
                            children: "Previous"
                        }),
                        formStep < 4 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            onClick: handleNext,
                            className: "buttonPrimary",
                            children: "Next"
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                open: open,
                onClose: onCloseModal,
                center: true,
                showCloseIcon: false,
                classNames: {
                    overlay: "popupTasksOverlay",
                    modal: "!w-full !p-6 !m-0 md:!max-w-[844px] !bg-transparent"
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "bg-[#101B1B] border-[1.5px] border-jade-100 rounded-md p-6 md:p-10",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "w-full flex flex-col gap-y-6 md:flex-row md:gap-x-10",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "w-full md:w-[52%]",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "space-y-6",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                    className: " text-base leading-5 font-Ubuntu-Medium md:text-[18px] md:leading-[22.14px]",
                                                    children: "Prize name"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full mt-2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "relative",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            ...register("prizeName", {
                                                                required: "Enter Prize"
                                                            }),
                                                            className: "inputField",
                                                            placeholder: "What you will gift in this giveaway?"
                                                        })
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                    className: " text-base leading-5 font-Ubuntu-Medium md:text-[18px] md:leading-[22.14px]",
                                                    children: "How many winners will win the prize"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full mt-2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "relative",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            ...register("noOfWinners", {
                                                                required: "Enter No of winners"
                                                            }),
                                                            className: "inputField",
                                                            placeholder: "1"
                                                        })
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                    className: " text-base leading-5 font-Ubuntu-Medium md:text-[18px] md:leading-[22.14px]",
                                                    children: "Prize value in USD per winner (Optional)"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full mt-2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "relative",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            ...register("value"),
                                                            className: "inputField",
                                                            placeholder: "$999"
                                                        })
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                    className: " text-base leading-5 font-Ubuntu-Medium md:text-[18px] md:leading-[22.14px]",
                                                    children: "Description (Optional)"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full mt-2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "relative",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                            ...register("description"),
                                                            className: "inputArea",
                                                            placeholder: "Description about the prize"
                                                        })
                                                    })
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: " flex absolute top-2 right-[18px]",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                    onClick: onCloseModal,
                                    src: _public_assets_images_xmark_png__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z,
                                    alt: "link_icon",
                                    layout: "intrinsic"
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "w-full md:w-[48%]",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                        className: " text-base leading-5 font-Ubuntu-Medium md:text-[18px] md:leading-[22.14px]",
                                        children: "Add prize image (Optional)"
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "relative border border-teal-100/20 bg-teal-900/5 rounded-md mt-2 h-[348px] flex items-center justify-center mb-6 md:mb-10",
                                        children: [
                                            selectedImage && !imageEdit && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                src: URL.createObjectURL(selectedImage),
                                                alt: "gift_box_big",
                                                layout: "fill"
                                            }),
                                            selectedImage && imageEdit && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                src: selectedImage,
                                                alt: "gift_box_big",
                                                layout: "fill"
                                            }),
                                            !selectedImage && !croppedImage && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                src: _public_assets_images_giftbox_big_svg__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
                                                alt: "gift_box_big",
                                                layout: "intrinsic"
                                            }),
                                            croppedImage && imageLoading && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_ImageCopper__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                                                        src: URL.createObjectURL(croppedImage),
                                                        maxWidth: 912,
                                                        maxHeight: 912,
                                                        onCrop: handleCrop
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        onClick: handleClose,
                                                        children: "Cancel"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: " absolute top-4 right-4 cursor-pointer",
                                                children: [
                                                    !selectedImage && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                                onClick: handleClick,
                                                                src: _public_assets_images_add_icon_svg__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z,
                                                                alt: "add_icon",
                                                                layout: "intrinsic"
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                type: "file",
                                                                id: "file",
                                                                ref: inputRef,
                                                                onChange: imageChange,
                                                                className: "hidden"
                                                            })
                                                        ]
                                                    }),
                                                    selectedImage && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                            onClick: removeSelectedImage,
                                                            src: _public_assets_images_xmark_png__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z,
                                                            alt: "link_icon",
                                                            layout: "intrinsic",
                                                            className: "border border-teal-100/20 bg-teal-900/5 rounded-md"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: onSubmit,
                                            className: "buttonPrimary",
                                            children: "Save"
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                })
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 6542:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ TasksTagItem)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


function TasksTagItem(props) {
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "w-full text-center text-sm font-Ubuntu-Medium py-2 px-8 bg-jade-900 rounded-sm cursor-pointer md:w-auto",
            onClick: props.onclick,
            children: props?.name
        })
    });
}


/***/ }),

/***/ 7795:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Tasks)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_assets_images_link_2_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9251);
/* harmony import */ var _public_assets_images_xmark_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3234);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4602);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3069);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _TaskTagItem__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(6542);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8762);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5641);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hook_form__WEBPACK_IMPORTED_MODULE_9__]);
([_context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hook_form__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);










const inviteBotUrl = process.env.NEXT_PUBLIC_INVITEBOT_URL;
function Tasks({ formStep , nextFormStep , prevFormStep  }) {
    // default Test tasks
    const defaultTasks = [
        {
            title: "Retweet this Twitter post",
            description: "Retweet this Twitter post and follow @Binance",
            link: "twitter.com",
            noOfEntries: "1",
            dailyTask: true
        },
        {
            title: "Follow us on Twitter",
            description: "Follow us on Twitter and retweet this post",
            link: "twitter.com",
            noOfEntries: "5",
            dailyTask: false
        }
    ];
    // Modal States
    const [open, setOpen] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [edited, setEdited] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [title, setTitle] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    const [index, setIndex] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
    // Modal Functions
    const onOpenModal = ()=>{
        setOpen(true);
    };
    const onCloseModal = ()=>{
        setOpen(false);
        setEdited(false);
        setValue("description", "");
        setValue("link", "");
        setValue("noOfEntries", "");
        setValue("dailyTasks", false);
    };
    // data management for whole multi step form
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_8__/* .useAuth */ .a)();
    // local tasks
    const [tasks, setTasks] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.tasks || []);
    // form states and values
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_9__.useForm)({
        mode: "onBlur"
    });
    const { register , handleSubmit , formState: { errors  } , watch , setValue , reset  } = methods;
    const formValues = watch();
    // map Tasks from data?.tasks to formValues
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            if (data?.tasks !== undefined && data?.tasks !== null && data?.tasks.length !== 0) {
                setValue("tasks", data?.tasks);
                setTasks(data?.tasks);
            } else {
                setTasks(defaultTasks);
            }
        }, 1);
    }, [
        setValue
    ]);
    const handleNext = (data)=>{
        // setFormValues(data);
        nextFormStep();
    };
    const handlePrev = (data)=>{
        //setFormValues(data);
        prevFormStep();
    };
    // add tasks to local tasks and global as well
    const addTask = ()=>{
        // get values from formValue
        const temp = {
            title: title,
            description: formValues.description,
            link: formValues.link,
            noOfEntries: formValues.noOfEntries,
            dailyTask: formValues.dailyTasks
        };
        // Add this task to Tasks
        const newTask = [
            ...tasks,
            temp
        ];
        setTasks(newTask);
        // set Default formValues
        setValue("description", "");
        setValue("link", "");
        setValue("noOfEntries", "");
        setValue("dailyTasks", false);
        reset();
        setOpen(false);
    };
    const updateTask = ()=>{
        // get values from formValue
        const temp = {
            title: tasks[index].title,
            description: formValues.description,
            link: formValues.link,
            noOfEntries: formValues.noOfEntries,
            dailyTask: formValues.dailyTasks
        };
        // update task in tasks
        const tempTasks = [
            ...tasks
        ];
        tempTasks[index] = temp;
        setTasks(tempTasks);
        // set Default formValues
        reset();
        setValue("description", "");
        setValue("link", "");
        setValue("noOfEntries", "");
        setValue("dailyTasks", false);
        setOpen(false);
        setEdited(false);
    };
    const removeTask = (index)=>{
        console.log(index);
        const temp = [
            ...tasks
        ];
        temp.splice(index, 1);
        setTasks(temp);
    };
    const editTask = (index)=>{
        setEdited(true);
        //console.log(index);
        const temp = [
            ...tasks
        ];
        //console.log(temp[index]);
        setIndex(index);
        setOpen(true);
        setTitle(temp[index].title);
        setValue("description", temp[index].description);
        setValue("link", temp[index].link);
        setValue("noOfEntries", temp[index].noOfEntries);
        setValue("dailyTasks", temp[index].dailyTask);
    };
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        //  console.log(formValues)
        // setFormValues(formValues)
        setTimeout(()=>{
            setFormValues({
                tasks: tasks
            });
        }, 100);
    //console.log(data);
    }, [
        tasks
    ]);
    const taskElements = tasks.map((task, index)=>{
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "w-1/2 px-2",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "border-[1.5px] border-jade-100 rounded-md p-6 md:pt-3.5 md:pb-5 md:px-8",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "grid grid_tasks_area",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "grid_tasks_heading",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                    className: "text-[20px] leading-[25px] font-Ubuntu-Medium md:text-base md:leading-5",
                                    children: task.title
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "grid_tasks_buttons mt-7 md:mt-0 md:justify-self-end",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: " flex items-center gap-x-4 justify-between md:justify-end",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "flex items-center",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                    htmlFor: "Daily task",
                                                    className: " text-sm text-extraLightWhite mr-1",
                                                    children: "Daily task"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    disabled: true,
                                                    name: "checkbox-button",
                                                    id: "Daily task",
                                                    type: "checkbox",
                                                    onChange: ()=>{},
                                                    className: "inputCheckbox",
                                                    checked: task.dailyTask
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: ()=>{
                                                editTask(index);
                                            },
                                            className: " w-[77px] h-8 border border-lightSecondaryGreen bg-jade-900 rounded-sm text-sm font-Ubuntu-Medium",
                                            children: "Edit"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: ()=>{
                                                removeTask(index);
                                            },
                                            className: " w-[77px] h-8 border border-lightSecondaryGreen bg-jade-900 rounded-sm text-sm font-Ubuntu-Medium",
                                            children: "Remove"
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "grid_tasks_content mt-6 md:mt-3.5",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-col items-center gap-4 mb-4 md:flex-row",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "w-full md:w-1/2",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "relative",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            disabled: true,
                                                            className: "inputField",
                                                            value: task.link
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: " flex absolute top-2 right-[18px]",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                                src: _public_assets_images_link_2_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                                                alt: "link_icon",
                                                                layout: "intrinsic"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "w-full md:w-1/2",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "relative",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                        disabled: true,
                                                        className: "inputField",
                                                        value: task.noOfEntries
                                                    })
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "w-full",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "relative",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                disabled: true,
                                                className: "inputArea",
                                                value: task.description
                                            })
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                }, index)
            })
        });
    });
    // update
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                className: "text-xl font-Ubuntu-Medium mb-6 lg:mb-8",
                children: "Tasks"
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex flex-col gap-y-6",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "flex flex-wrap gap-y-6",
                        children: taskElements
                    }, 1),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "border-[1.5px] border-jade-100 rounded-md py-6 px-8",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                className: " text-[20px] leading-[25px] font-Ubuntu-Medium md:text-base md:leading-5 mb-6",
                                children: "Add a way to enter"
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex flex-col w-full items-center gap-y-3 md:flex-row md:flex-wrap md:gap-2",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Visiting a website",
                                        onclick: ()=>{
                                            setTitle("Visit a URL");
                                            onOpenModal();
                                        }
                                    }, 1),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Share on Facebook",
                                        onclick: ()=>{
                                            setTitle("Share on Facebook");
                                            onOpenModal();
                                        }
                                    }, 211),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Discord",
                                        onclick: ()=>{
                                            setTitle("Join Discord");
                                            onOpenModal();
                                        }
                                    }, 5),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Follow us on Twitter",
                                        onclick: ()=>{
                                            setTitle("Follow us on Twitter");
                                            onOpenModal();
                                        }
                                    }, 9),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Post on Twitter",
                                        onclick: ()=>{
                                            setTitle("Post on Twitter");
                                            onOpenModal();
                                        }
                                    }, 10),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Retweet this Twitter post",
                                        onclick: ()=>{
                                            setTitle("Retweet this Twitter post");
                                            onOpenModal();
                                        }
                                    }, 91),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TaskTagItem__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                        name: "Telegram",
                                        onclick: ()=>{
                                            setTitle("Follow on Telegram");
                                            onOpenModal();
                                        }
                                    }, 12)
                                ]
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto py-12 ",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                    children: [
                        formStep > 0 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            onClick: handlePrev,
                            className: "buttonPrimary",
                            children: "Previous"
                        }),
                        formStep < 4 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            onClick: handleNext,
                            className: "buttonPrimary",
                            children: "Next"
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_responsive_modal__WEBPACK_IMPORTED_MODULE_6__.Modal, {
                open: open,
                onClose: onCloseModal,
                center: true,
                showCloseIcon: false,
                classNames: {
                    overlay: "popupTasksOverlay",
                    modal: "!w-full !p-6 !m-0 md:!w-[594px] !bg-transparent"
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "bg-[#101B1B] border-[1.5px] border-jade-100 rounded-md p-6 md:pt-3.5 md:pb-5 md:px-8",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "grid grid_tasks_area",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "grid_tasks_heading",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                    className: " text-[20px] leading-[25px] font-Ubuntu-Medium md:text-base md:leading-5",
                                    children: title
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "grid_tasks_buttons mt-7 md:mt-0 md:justify-self-end",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: " flex items-center gap-x-4 justify-between md:justify-end",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "flex items-center",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                    htmlFor: "Daily task",
                                                    className: " text-sm text-extraLightWhite mr-1",
                                                    children: "Daily task"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    id: "Daily task",
                                                    type: "checkbox",
                                                    ...register("dailyTasks"),
                                                    className: "inputCheckbox"
                                                })
                                            ]
                                        }),
                                        !edited && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: addTask,
                                            className: " w-[77px] h-8 border border-transparent bg-jade-900 rounded-sm text-sm font-Ubuntu-Medium",
                                            children: "Add"
                                        }),
                                        title === "Join Discord" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: ()=>{
                                                window.open(inviteBotUrl, "_blank");
                                            },
                                            className: " w-[77px] h-8 border border-lightSecondaryGreen bg-jade-900 rounded-sm text-sm font-Ubuntu-Medium",
                                            children: "Invite Bot"
                                        }),
                                        edited && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: updateTask,
                                            className: " w-[77px] h-8 border border-transparent bg-jade-900 rounded-sm text-sm font-Ubuntu-Medium",
                                            children: "Update"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: " flex absolute top-2 right-[18px]",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                onClick: onCloseModal,
                                                src: _public_assets_images_xmark_png__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
                                                alt: "link_icon",
                                                layout: "intrinsic"
                                            })
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "grid_tasks_content mt-6 md:mt-3.5",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-col items-center gap-4 mb-4 md:flex-row",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "w-full md:w-1/2",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "relative",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            className: "inputField",
                                                            placeholder: "Add a url or server id",
                                                            ...register("link", {
                                                                required: "Enter your share link / server id for Discord"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: " flex absolute top-2 right-[18px]",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                                src: _public_assets_images_link_2_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                                                alt: "link_icon",
                                                                layout: "intrinsic"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "w-full md:w-1/2",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "relative",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                        className: "inputField",
                                                        placeholder: "Number of entries",
                                                        ...register("noOfEntries", {
                                                            required: "Enter no of entries"
                                                        })
                                                    })
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "w-full",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "relative",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                className: "inputArea",
                                                placeholder: "Add task description (Optional)",
                                                ...register("description")
                                            })
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                })
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5122:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ UserDeatils)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5641);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8762);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_3__]);
([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);




function UserDeatils({ formStep , nextFormStep , prevFormStep  }) {
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_3__/* .useAuth */ .a)();
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_2__.useForm)({
        mode: "onBlur"
    });
    const { register , handleSubmit , formState: { errors  } , watch , setValue  } = methods;
    const [reqFullName, setReqFullName] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqFullName || false);
    const [reqCrypto, setReqCrypto] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqCrypto || false);
    const [reqAge, setReqAge] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqAge || false);
    const [reqEmail, setReqEmail] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqEmail || false);
    const [reqCountry, setReqCountry] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqCountry || false);
    const [reqAddress, setReqAddress] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.reqAddress || false);
    const [optFullName, setOptFullName] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.optFullName || false);
    const [optCrypto, setOptCrypto] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.optCrypto || false);
    const [optAge, setOptAge] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.optAge || false);
    const [optEmail, setOptEmail] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.email || false);
    const [optCountry, setOptCountry] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.optCountry || false);
    const [optAddress, setOptAddress] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(data?.optAddress || false);
    const formValues = watch();
    // console.log(data);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            // set reqFullName false if its undefined
            if (data?.reqFullName === undefined) {
                setValue("reqFullName", false);
            } else {
                setValue("reqFullName", data?.reqFullName);
                setReqFullName(data?.reqFullName);
            }
            // set reqCrypto false if its undefined
            if (data?.reqCrypto === undefined) {
                setValue("reqCrypto", false);
            } else {
                setValue("reqCrypto", data?.reqCrypto);
                setReqCrypto(data?.reqCrypto);
            }
            // set reqAge false if its undefined
            if (data?.reqAge === undefined) {
                setValue("reqAge", false);
            } else {
                setValue("reqAge", data?.reqAge);
                setReqAge(data?.reqAge);
            }
            // set reqEmail false if its undefined
            if (data?.reqEmail === undefined) {
                setValue("reqEmail", false);
            } else {
                setValue("reqEmail", data?.reqEmail);
                setReqEmail(data?.reqEmail);
            }
            // set reqCountry false if its undefined
            if (data?.reqCountry === undefined) {
                setValue("reqCountry", false);
            } else {
                setValue("reqCountry", data?.reqCountry);
                setReqCountry(data?.reqCountry);
            }
            // set reqAddress false if its undefined
            if (data?.reqAddress === undefined) {
                setValue("reqAddress", false);
            } else {
                setValue("reqAddress", data?.reqAddress);
                setReqAddress(data?.reqAddress);
            }
            // set optFullName false if its undefined
            if (data?.optFullName === undefined) {
                setValue("optFullName", false);
            } else {
                setValue("optFullName", data?.optFullName);
                setOptFullName(data?.optFullName);
            }
            // set optCrypto false if its undefined
            if (data?.optCrypto === undefined) {
                setValue("optCrypto", false);
            } else {
                setValue("optCrypto", data?.optCrypto);
                setOptCrypto(data?.optCrypto);
            }
            // set optAge false if its undefined
            if (data?.optAge === undefined) {
                setValue("optAge", false);
            } else {
                setValue("optAge", data?.optAge);
                setOptAge(data?.optAge);
            }
            // set optEmail false if its undefined
            if (data?.optEmail === undefined) {
                setValue("optEmail", false);
            } else {
                setValue("optEmail", data?.optEmail);
                setOptEmail(data?.optEmail);
            }
            // set optCountry false if its undefined
            if (data?.optCountry === undefined) {
                setValue("optCountry", false);
            } else {
                setValue("optCountry", data?.optCountry);
                setOptCountry(data?.optCountry);
            }
            // set optAddress false if its undefined
            if (data?.optAddress === undefined) {
                setValue("optAddress", false);
            } else {
                setValue("optAddress", data?.optAddress);
                setOptAddress(data?.optAddress);
            }
        }, 10);
    }, [
        setValue
    ]);
    const handleNext = (data)=>{
        //setFormValues(data);
        nextFormStep();
    // previousFormStep();
    };
    const handlePrev = (data)=>{
        //setFormValues(data);
        prevFormStep();
    };
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        //  console.log(formValues)
        // setFormValues(formValues)
        setTimeout(()=>{
            setFormValues(formValues);
        }, 300);
    }, [
        formValues
    ]);
    const doNothing = ()=>{};
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_hook_form__WEBPACK_IMPORTED_MODULE_2__.FormProvider, {
            ...methods,
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                onSubmit: handleSubmit(doNothing),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "flex flex-col gap-y-6 md:gap-y-8 lg:gap-x-10 lg:flex-row",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "w-full flex flex-col space-y-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "border-[1.5px] border-jade-100 rounded-md",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "p-6 md:py-[21px] lg:py-[18px] lg:px-8",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "text-[20px] leading-6 font-Ubuntu-Medium mb-[18px] lg:text-xl lg:mb-6",
                                                children: "Select required fields"
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "grid grid-cols-1 gap-4 md:grid-cols-3",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optFullName,
                                                        className: !reqFullName ? "inputField" : "inputField inputFieldSelected",
                                                        // on click update the form value reqFullName
                                                        onClick: ()=>{
                                                            setReqFullName(!reqFullName);
                                                            setValue("reqFullName", !reqFullName);
                                                        },
                                                        children: "Full name"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optCrypto,
                                                        className: !reqCrypto ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setReqCrypto(!reqCrypto);
                                                            setValue("reqCrypto", !reqCrypto);
                                                        },
                                                        children: "Crypto Wallet address"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optAge,
                                                        className: !reqAge ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setReqAge(!reqAge);
                                                            setValue("reqAge", !reqAge);
                                                        },
                                                        children: "Age"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optEmail,
                                                        className: !reqEmail ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setReqEmail(!reqEmail);
                                                            setValue("reqEmail", !reqEmail);
                                                        },
                                                        children: "Email"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optCountry,
                                                        className: !reqCountry ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setReqCountry(!reqCountry);
                                                            setValue("reqCountry", !reqCountry);
                                                        },
                                                        children: "Country"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: optAddress,
                                                        className: !reqAddress ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setReqAddress(!reqAddress);
                                                            setValue("reqAddress", !reqAddress);
                                                        },
                                                        children: "Address"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "border-[1.5px] border-jade-100 rounded-md",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "p-6 md:py-[21px] lg:py-[18px] lg:px-8",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "text-[20px] leading-6 font-Ubuntu-Medium mb-[18px] lg:text-xl lg:mb-6",
                                                children: "Select optional fields"
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "grid grid-cols-1 gap-4 md:grid-cols-3",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqFullName,
                                                        className: !optFullName ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptFullName(!optFullName);
                                                            setValue("optFullName", !optFullName);
                                                        },
                                                        children: "Full name"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqCrypto,
                                                        className: !optCrypto ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptCrypto(!optCrypto);
                                                            setValue("optCrypto", !optCrypto);
                                                        },
                                                        children: "Crypto Wallet address"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqAge,
                                                        className: !optAge ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptAge(!optAge);
                                                            setValue("optAge", !optAge);
                                                        },
                                                        children: "Age"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqEmail,
                                                        className: !optEmail ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptEmail(!optEmail);
                                                            setValue("optEmail", !optEmail);
                                                        },
                                                        children: "Email"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqCountry,
                                                        className: !optCountry ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptCountry(!optCountry);
                                                            setValue("optCountry", !optCountry);
                                                        },
                                                        children: "Country"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        disabled: reqAddress,
                                                        className: !optAddress ? "inputField" : "inputField inputFieldSelected",
                                                        onClick: ()=>{
                                                            setOptAddress(!optAddress);
                                                            setValue("optAddress", !optAddress);
                                                        },
                                                        children: "Address"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto py-12 ",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                            children: [
                                formStep > 0 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: handlePrev,
                                    className: "buttonPrimary",
                                    children: "Previous"
                                }),
                                formStep < 4 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: handleNext,
                                    className: "buttonPrimary",
                                    children: "Next"
                                })
                            ]
                        })
                    })
                ]
            })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 1756:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ New)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1492);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6201);
/* harmony import */ var react_tabs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5973);
/* harmony import */ var react_tabs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_tabs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _loader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3628);
/* harmony import */ var _PostEntry__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1026);
/* harmony import */ var _Prizes__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4474);
/* harmony import */ var _ProtectedRoute__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9690);
/* harmony import */ var _Tasks__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(7795);
/* harmony import */ var _UserDeatils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5122);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(8762);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(1083);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(236);
/* harmony import */ var _Layouts_MainLayout__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3518);
/* harmony import */ var _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(291);
/* harmony import */ var _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(2987);
/* harmony import */ var _setup__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(7992);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_4__, _PostEntry__WEBPACK_IMPORTED_MODULE_7__, _Prizes__WEBPACK_IMPORTED_MODULE_8__, _ProtectedRoute__WEBPACK_IMPORTED_MODULE_9__, _Tasks__WEBPACK_IMPORTED_MODULE_10__, _UserDeatils__WEBPACK_IMPORTED_MODULE_11__, _context_authcontext__WEBPACK_IMPORTED_MODULE_12__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_13__, _firebase__WEBPACK_IMPORTED_MODULE_14__, _Layouts_MainLayout__WEBPACK_IMPORTED_MODULE_15__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_16__, _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_17__, _setup__WEBPACK_IMPORTED_MODULE_18__]);
([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_4__, _PostEntry__WEBPACK_IMPORTED_MODULE_7__, _Prizes__WEBPACK_IMPORTED_MODULE_8__, _ProtectedRoute__WEBPACK_IMPORTED_MODULE_9__, _Tasks__WEBPACK_IMPORTED_MODULE_10__, _UserDeatils__WEBPACK_IMPORTED_MODULE_11__, _context_authcontext__WEBPACK_IMPORTED_MODULE_12__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_13__, _firebase__WEBPACK_IMPORTED_MODULE_14__, _Layouts_MainLayout__WEBPACK_IMPORTED_MODULE_15__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_16__, _utils_campaignEvents__WEBPACK_IMPORTED_MODULE_17__, _setup__WEBPACK_IMPORTED_MODULE_18__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);



















function New({ giveaway  }) {
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_13__/* .useUserData */ .v)();
    const { isNotificationEnabled , isEmailEnabled , setIsEmailEnabled , setIsNotificationEnable  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_12__/* .useAuth */ .a)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    const [tabIndex, setTabIndex] = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(0);
    const { data , setFormValues , user  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_12__/* .useAuth */ .a)();
    const [inProgress, setInProgress] = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    const id = (next_router__WEBPACK_IMPORTED_MODULE_2___default().query.id);
    const [editMode, setEditMode] = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    // if id is not null or undefined and has a value then editmode is true
    (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(()=>{
        if (id !== null && id !== undefined && id !== "") {
            setEditMode(true);
        }
    }, [
        id
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(()=>{
        setLoading(!loading);
        // add redirection to users dashboard if userType == user
        if (userData?.userType === "user") {
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error("Access denied. Redirecting to your dashboard");
            window.location.href = "/users/dashboard";
        }
    }, [
        userData
    ]);
    function nextTab() {
        setTabIndex(tabIndex + 1);
    }
    function prevTab() {
        setTabIndex(tabIndex - 1);
    }
    function generateUID() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    // change here
    async function submitCampaign() {
        setInProgress(true);
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            template: data.template === true || data.template === undefined || data.template === null ? false : data.template,
            draft: false,
            isNotificationEnabled: true,
            isEmailEnabled
        };
        setLoading(true);
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_14__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.setDoc)(ref, tempData);
            if (userData?.isNotificationEnabled || tempData?.isNotificationEnabled) {
                await (0,_utils_campaignEvents__WEBPACK_IMPORTED_MODULE_17__/* .handleNotification */ .R)(userData, tempData, user);
            }
            // if (userData?.isEmailEnabled || isEmailEnabled) {
            //   await sendEmail(userData, tempData, user);
            // }
            reSetNotifications();
            setLoading(false);
            next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].success("GiveAway Created");
        } catch (e) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error(e);
        }
        setLoading(false);
        setFormValues("submitted");
    }
    async function submitCampaignAsDraft() {
        setInProgress(true);
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            draft: true
        };
        setLoading(true);
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_14__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.setDoc)(ref, tempData);
            setLoading(false);
            next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].success("GiveAway Created");
        } catch (e) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error(e);
        }
        setLoading(false);
        setFormValues("submitted");
    }
    function reSetNotifications() {
        setIsEmailEnabled(false);
        setIsNotificationEnable(false);
    }
    // update here
    async function updateCampaign() {
        setLoading(true);
        setInProgress(true);
        if (id !== null && id !== undefined && id !== "") {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_14__.db, "giveaway", id.toString());
            const tempData = {
                ...data,
                isNotificationEnabled: true,
                isEmailEnabled: true
            };
            // update ref
            try {
                await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.updateDoc)(ref, tempData);
                if (tempData.isNotificationEnabled) {
                    await (0,_utils_campaignEvents__WEBPACK_IMPORTED_MODULE_17__/* .handleNotification */ .R)(userData, tempData, user, "updated");
                }
                // if (tempData?.isEmailEnabled) {
                //   await sendEmail(userData,tempData, user, "updated");
                // }
                reSetNotifications();
                next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
                react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].success("GiveAway Updated");
                setLoading(false);
                setFormValues("submitted");
            } catch (e) {
                setLoading(false);
                react_hot_toast__WEBPACK_IMPORTED_MODULE_4__["default"].error(e);
            }
        }
    }
    function Cancel() {
        next_router__WEBPACK_IMPORTED_MODULE_2___default().push("/company/giveaway");
        setFormValues("submitted");
    }
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Layouts_MainLayout__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_ProtectedRoute__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_loader__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                    show: loading
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "mx-auto max-w-6xl",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "flex flex-col lg:flex-row w-full justify-between items-center",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h2", {
                                className: "flex space-x-3 items-center text-xl font-Ubuntu-Medium",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_16__.PlusCircleIcon, {
                                        className: "w-6 h-6 text-teal-600"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        children: "Create a Giveaway"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "mt-8 md:mt-22",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tabs, {
                                className: "flex flex-col grid_template_area",
                                focusTabOnClick: false,
                                selectedIndex: tabIndex,
                                onSelect: ()=>{},
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-col lg:flex-row w-full justify-between items-center",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabList, {
                                                className: "fbTabList grid_giveaway_tabs ",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tab, {
                                                        className: "fbTab",
                                                        selectedClassName: "bg-extraLightPrimaryGreen rounded-[7px]",
                                                        children: "Setup"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tab, {
                                                        // disabled={true}
                                                        className: "fbTab",
                                                        selectedClassName: "bg-extraLightPrimaryGreen rounded-[7px]",
                                                        children: "User details"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tab, {
                                                        // disabled={true}
                                                        className: "fbTab",
                                                        selectedClassName: "bg-extraLightPrimaryGreen rounded-[7px]",
                                                        children: "Tasks"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tab, {
                                                        // disabled={true}
                                                        className: "fbTab",
                                                        selectedClassName: "bg-extraLightPrimaryGreen rounded-[7px]",
                                                        children: "Prizes"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.Tab, {
                                                        className: "fbTab",
                                                        selectedClassName: "bg-extraLightPrimaryGreen rounded-[7px]",
                                                        children: "Post entry"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "grid_giveaway_buttons mt-20 md:mt-[60px] lg:mt-0",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end px-1",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                            onClick: Cancel,
                                                            className: "buttonPrimary",
                                                            children: "Discard"
                                                        }),
                                                        tabIndex < 4 && !editMode && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                            onClick: submitCampaignAsDraft,
                                                            className: "buttonPrimary py-2",
                                                            disabled: inProgress,
                                                            children: "Save As Draft"
                                                        }),
                                                        tabIndex === 4 && !editMode && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                            onClick: submitCampaign,
                                                            className: "buttonPrimary py-2",
                                                            disabled: inProgress,
                                                            children: "Publish"
                                                        }),
                                                        editMode && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                            onClick: updateCampaign,
                                                            className: "buttonPrimary",
                                                            disabled: inProgress,
                                                            children: "Update"
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "grid_giveaway_content mt-8 lg:mt-10 ",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabPanel, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_setup__WEBPACK_IMPORTED_MODULE_18__/* ["default"] */ .Z, {
                                                    formStep: tabIndex,
                                                    nextFormStep: nextTab
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabPanel, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_UserDeatils__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                                                    formStep: tabIndex,
                                                    nextFormStep: nextTab,
                                                    prevFormStep: prevTab
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabPanel, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Tasks__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                                                    formStep: tabIndex,
                                                    nextFormStep: nextTab,
                                                    prevFormStep: prevTab
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabPanel, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Prizes__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                                                    formStep: tabIndex,
                                                    nextFormStep: nextTab,
                                                    prevFormStep: prevTab
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_tabs__WEBPACK_IMPORTED_MODULE_5__.TabPanel, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_PostEntry__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                                    formStep: tabIndex,
                                                    nextFormStep: nextTab,
                                                    prevFormStep: prevTab
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7992:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Setup)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_assets_images_info_icon_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3303);
/* harmony import */ var _CountryItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2723);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8762);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5641);
/* harmony import */ var _CountrySearch__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1192);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6201);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_CountryItem__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, react_hook_form__WEBPACK_IMPORTED_MODULE_6__, _CountrySearch__WEBPACK_IMPORTED_MODULE_7__, react_hot_toast__WEBPACK_IMPORTED_MODULE_8__]);
([_CountryItem__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, react_hook_form__WEBPACK_IMPORTED_MODULE_6__, _CountrySearch__WEBPACK_IMPORTED_MODULE_7__, react_hot_toast__WEBPACK_IMPORTED_MODULE_8__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);









function Setup({ formStep , nextFormStep  }) {
    const toc = {
        "title": "Terms and Conditions",
        "version": "2.0",
        "content": "Welcome to our website. By accessing or using our website, you agree to be bound by the following terms and conditions:\n\nIntellectual Property: All content, including but not limited to text, graphics, images, and logos, on this website is the property of our company or our licensors and is protected by intellectual property laws. You may not use, reproduce, or distribute any content without our prior written consent.\n\nUser Conduct: You agree to use our website only for lawful purposes and in a way that does not infringe the rights of others or restrict or inhibit anyone else's use and enjoyment of the website.\n\nDisclaimer: We make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability, or availability of the website or the information, products, services, or related graphics contained on the website for any purpose. You use the website at your own risk.\n\nLimitation of Liability: We will not be liable for any loss or damage, whether direct or indirect, arising from your use of the website or reliance on any information, products, or services provided on the website.\n\nChanges to Terms and Conditions: We reserve the right to update or modify these terms and conditions at any time without prior notice. By continuing to use the website, you agree to be bound by the updated or modified terms and conditions. By using our website, you agree to these terms and conditions in full. If you do not agree with any part of these terms and conditions, please do not use our website.",
        "date": "2023-05-22"
    };
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_5__/* .useAuth */ .a)();
    const [terms, setTerms] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_6__.useForm)({
        mode: "onBlur"
    });
    const { register , handleSubmit , formState: { errors  } , watch , setValue  } = methods;
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            setValue("title", data?.title || "");
            setValue("endDate", data?.endDate || "");
            setValue("startDate", data?.startDate || "");
            setValue("description", data?.description || "");
            setValue("toc", data?.toc || toc?.content);
        }, 1);
    }, [
        setValue
    ]);
    const formValues = watch();
    const handleNext = (data)=>{
        //setFormValues(data);
        if (errors.title || errors.description || errors.endDate || errors.startDate || formValues.title === "" || formValues.description === "") {
            console.log(errors);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].error("You need to fill the required fields");
            return;
        }
        nextFormStep();
    };
    // if formvalues changes then updated setFormValues
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            setFormValues(formValues);
        }, 300);
    }, [
        formValues
    ]);
    // map country Names from data.restrictedCountries
    const countryNames = data?.restrictedCountries?.map((country)=>{
        return {
            name: country.name,
            code: country.code
        };
    });
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_hook_form__WEBPACK_IMPORTED_MODULE_6__.FormProvider, {
            ...methods,
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                onSubmit: handleSubmit(handleNext),
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "w-full space-y-8 md:space-y-10",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex w-full justify-between md:space-x-20 items-center gap-y-8 flex-col lg:flex-row lg:gap-y-0",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "w-full",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "text-lg leading-[22px] font-Ubuntu-Medium mb-2",
                                                children: "Giveaway title"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                placeholder: "Set Your Giveaway a Title",
                                                ...register("title", {
                                                    required: "Title is required"
                                                }),
                                                className: "inputField"
                                            }),
                                            errors.title && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                className: "text-red",
                                                children: errors.title.message
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-col w-full justify-between gap-y-8 md:gap-x-8 md:gap-y-0 md:flex-row lg:justify-end",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                        className: "text-lg leading-[22px] font-Ubuntu-Medium mb-2",
                                                        children: "Start Date"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                        ...register("startDate", {
                                                            required: "Starting Date is required"
                                                        }),
                                                        className: "inputField",
                                                        type: "datetime-local"
                                                    }),
                                                    errors.startDate && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "text-red",
                                                        children: errors.startDate.message
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                        className: "text-lg leading-[22px] font-Ubuntu-Medium mb-2",
                                                        children: "End Date"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                        ...register("endDate", {
                                                            required: "End Date is required"
                                                        }),
                                                        className: "inputField",
                                                        type: "datetime-local"
                                                    }),
                                                    errors.endDate && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "text-red",
                                                        children: errors.endDate.message
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "border rounded-md",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "p-6 md:py-[18px] md:px-8",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h2", {
                                            className: "text-[20px] leading-[24.6px] font-Ubuntu-Medium flex items-center mb-6 md:text-xl",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: "flex items-center justify-center mr-[10px] md:mr-[18px]",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                        src: _public_assets_images_info_icon_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                                        alt: "info_icon",
                                                        layout: "intrinsic"
                                                    })
                                                }),
                                                "About Giveaway"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "w-full",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                    placeholder: "Write a description about your giveaway",
                                                    ...register("description", {
                                                        required: "Description is required"
                                                    }),
                                                    className: "inputArea",
                                                    style: {
                                                        height: "120px"
                                                    }
                                                }),
                                                errors.description && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: "text-red",
                                                    children: errors.description.message
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "border rounded-md",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "p-6 md:py-[18px] md:px-8",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h2", {
                                            className: "text-[20px] leading-[24.6px] font-Ubuntu-Medium flex items-center mb-6 md:text-xl",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: "flex items-center justify-center mr-[10px] md:mr-[18px]",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                        src: _public_assets_images_info_icon_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                                        alt: "info_icon",
                                                        layout: "intrinsic"
                                                    })
                                                }),
                                                "Terms and conditions"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "w-full",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                    placeholder: "Write a description about your giveaway",
                                                    ...register("toc", {
                                                        required: "Terms and conditions are required or use Default"
                                                    }),
                                                    className: "inputArea",
                                                    style: {
                                                        height: "450px",
                                                        // no scroll bar
                                                        overflow: "hidden"
                                                    }
                                                }),
                                                errors.toc && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: "text-red",
                                                    children: errors.toc.message
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "border rounded-md",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "p-6 md:pt-[18px] md:pb-7 md:pl-8 md:pr-4",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "flex flex-col items-center justify-between mb-6 md:mb-[18px] md:flex-row ",
                                            children: [
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h2", {
                                                    className: "w-full text-[20px] leading-[24.6px] font-Ubuntu-Medium flex items-center mb-6 md:w-1/2 md:text-xl",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "flex items-center justify-center mr-[10px] md:mr-[18px]",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                                src: _public_assets_images_info_icon_svg__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                                                alt: "info_icon",
                                                                layout: "intrinsic"
                                                            })
                                                        }),
                                                        "Restriction on countries"
                                                    ]
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full md:w-1/2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        id: "dropdownSearch",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: "p-3",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "flex w-full ml-auto relative lg:w-[360px]",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CountrySearch__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {})
                                                            })
                                                        })
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "flex flex-wrap gap-y-6 md:gap-y-0 md:flex-row",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "w-full border-r-[1.5px] border-jade-100 ",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "grid grid-cols-2 md:gap-4",
                                                    children: countryNames?.map((countryName, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CountryItem__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                                                            title: countryName.name
                                                        }, index))
                                                })
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto py-12 ",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                            children: formStep < 4 && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                onClick: handleNext,
                                className: "buttonPrimary",
                                children: "Next"
                            })
                        })
                    })
                ]
            })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 9034:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "h": () => (/* binding */ countries)
/* harmony export */ });
const countries = [
    {
        name: "Afghanistan",
        code: "AF"
    },
    {
        name: "\xc5land Islands",
        code: "AX"
    },
    {
        name: "Albania",
        code: "AL"
    },
    {
        name: "Algeria",
        code: "DZ"
    },
    {
        name: "American Samoa",
        code: "AS"
    },
    {
        name: "AndorrA",
        code: "AD"
    },
    {
        name: "Angola",
        code: "AO"
    },
    {
        name: "Anguilla",
        code: "AI"
    },
    {
        name: "Antarctica",
        code: "AQ"
    },
    {
        name: "Antigua and Barbuda",
        code: "AG"
    },
    {
        name: "Argentina",
        code: "AR"
    },
    {
        name: "Armenia",
        code: "AM"
    },
    {
        name: "Aruba",
        code: "AW"
    },
    {
        name: "Australia",
        code: "AU"
    },
    {
        name: "Austria",
        code: "AT"
    },
    {
        name: "Azerbaijan",
        code: "AZ"
    },
    {
        name: "Bahamas",
        code: "BS"
    },
    {
        name: "Bahrain",
        code: "BH"
    },
    {
        name: "Bangladesh",
        code: "BD"
    },
    {
        name: "Barbados",
        code: "BB"
    },
    {
        name: "Belarus",
        code: "BY"
    },
    {
        name: "Belgium",
        code: "BE"
    },
    {
        name: "Belize",
        code: "BZ"
    },
    {
        name: "Benin",
        code: "BJ"
    },
    {
        name: "Bermuda",
        code: "BM"
    },
    {
        name: "Bhutan",
        code: "BT"
    },
    {
        name: "Bolivia",
        code: "BO"
    },
    {
        name: "Bosnia and Herzegovina",
        code: "BA"
    },
    {
        name: "Botswana",
        code: "BW"
    },
    {
        name: "Bouvet Island",
        code: "BV"
    },
    {
        name: "Brazil",
        code: "BR"
    },
    {
        name: "British Indian Ocean Territory",
        code: "IO"
    },
    {
        name: "Brunei Darussalam",
        code: "BN"
    },
    {
        name: "Bulgaria",
        code: "BG"
    },
    {
        name: "Burkina Faso",
        code: "BF"
    },
    {
        name: "Burundi",
        code: "BI"
    },
    {
        name: "Cambodia",
        code: "KH"
    },
    {
        name: "Cameroon",
        code: "CM"
    },
    {
        name: "Canada",
        code: "CA"
    },
    {
        name: "Cape Verde",
        code: "CV"
    },
    {
        name: "Cayman Islands",
        code: "KY"
    },
    {
        name: "Central African Republic",
        code: "CF"
    },
    {
        name: "Chad",
        code: "TD"
    },
    {
        name: "Chile",
        code: "CL"
    },
    {
        name: "China",
        code: "CN"
    },
    {
        name: "Christmas Island",
        code: "CX"
    },
    {
        name: "Cocos (Keeling) Islands",
        code: "CC"
    },
    {
        name: "Colombia",
        code: "CO"
    },
    {
        name: "Comoros",
        code: "KM"
    },
    {
        name: "Congo",
        code: "CG"
    },
    {
        name: "Congo, The Democratic Republic of the",
        code: "CD"
    },
    {
        name: "Cook Islands",
        code: "CK"
    },
    {
        name: "Costa Rica",
        code: "CR"
    },
    {
        name: "Cote D'Ivoire",
        code: "CI"
    },
    {
        name: "Croatia",
        code: "HR"
    },
    {
        name: "Cuba",
        code: "CU"
    },
    {
        name: "Cyprus",
        code: "CY"
    },
    {
        name: "Czech Republic",
        code: "CZ"
    },
    {
        name: "Denmark",
        code: "DK"
    },
    {
        name: "Djibouti",
        code: "DJ"
    },
    {
        name: "Dominica",
        code: "DM"
    },
    {
        name: "Dominican Republic",
        code: "DO"
    },
    {
        name: "Ecuador",
        code: "EC"
    },
    {
        name: "Egypt",
        code: "EG"
    },
    {
        name: "El Salvador",
        code: "SV"
    },
    {
        name: "Equatorial Guinea",
        code: "GQ"
    },
    {
        name: "Eritrea",
        code: "ER"
    },
    {
        name: "Estonia",
        code: "EE"
    },
    {
        name: "Ethiopia",
        code: "ET"
    },
    {
        name: "Falkland Islands (Malvinas)",
        code: "FK"
    },
    {
        name: "Faroe Islands",
        code: "FO"
    },
    {
        name: "Fiji",
        code: "FJ"
    },
    {
        name: "Finland",
        code: "FI"
    },
    {
        name: "France",
        code: "FR"
    },
    {
        name: "French Guiana",
        code: "GF"
    },
    {
        name: "French Polynesia",
        code: "PF"
    },
    {
        name: "French Southern Territories",
        code: "TF"
    },
    {
        name: "Gabon",
        code: "GA"
    },
    {
        name: "Gambia",
        code: "GM"
    },
    {
        name: "Georgia",
        code: "GE"
    },
    {
        name: "Germany",
        code: "DE"
    },
    {
        name: "Ghana",
        code: "GH"
    },
    {
        name: "Gibraltar",
        code: "GI"
    },
    {
        name: "Greece",
        code: "GR"
    },
    {
        name: "Greenland",
        code: "GL"
    },
    {
        name: "Grenada",
        code: "GD"
    },
    {
        name: "Guadeloupe",
        code: "GP"
    },
    {
        name: "Guam",
        code: "GU"
    },
    {
        name: "Guatemala",
        code: "GT"
    },
    {
        name: "Guernsey",
        code: "GG"
    },
    {
        name: "Guinea",
        code: "GN"
    },
    {
        name: "Guinea-Bissau",
        code: "GW"
    },
    {
        name: "Guyana",
        code: "GY"
    },
    {
        name: "Haiti",
        code: "HT"
    },
    {
        name: "Heard Island and Mcdonald Islands",
        code: "HM"
    },
    {
        name: "Holy See (Vatican City State)",
        code: "VA"
    },
    {
        name: "Honduras",
        code: "HN"
    },
    {
        name: "Hong Kong",
        code: "HK"
    },
    {
        name: "Hungary",
        code: "HU"
    },
    {
        name: "Iceland",
        code: "IS"
    },
    {
        name: "India",
        code: "IN"
    },
    {
        name: "Indonesia",
        code: "ID"
    },
    {
        name: "Iran, Islamic Republic Of",
        code: "IR"
    },
    {
        name: "Iraq",
        code: "IQ"
    },
    {
        name: "Ireland",
        code: "IE"
    },
    {
        name: "Isle of Man",
        code: "IM"
    },
    {
        name: "Israel",
        code: "IL"
    },
    {
        name: "Italy",
        code: "IT"
    },
    {
        name: "Jamaica",
        code: "JM"
    },
    {
        name: "Japan",
        code: "JP"
    },
    {
        name: "Jersey",
        code: "JE"
    },
    {
        name: "Jordan",
        code: "JO"
    },
    {
        name: "Kazakhstan",
        code: "KZ"
    },
    {
        name: "Kenya",
        code: "KE"
    },
    {
        name: "Kiribati",
        code: "KI"
    },
    {
        name: "Korea, Democratic People'S Republic of",
        code: "KP"
    },
    {
        name: "Korea, Republic of",
        code: "KR"
    },
    {
        name: "Kuwait",
        code: "KW"
    },
    {
        name: "Kyrgyzstan",
        code: "KG"
    },
    {
        name: "Lao People'S Democratic Republic",
        code: "LA"
    },
    {
        name: "Latvia",
        code: "LV"
    },
    {
        name: "Lebanon",
        code: "LB"
    },
    {
        name: "Lesotho",
        code: "LS"
    },
    {
        name: "Liberia",
        code: "LR"
    },
    {
        name: "Libyan Arab Jamahiriya",
        code: "LY"
    },
    {
        name: "Liechtenstein",
        code: "LI"
    },
    {
        name: "Lithuania",
        code: "LT"
    },
    {
        name: "Luxembourg",
        code: "LU"
    },
    {
        name: "Macao",
        code: "MO"
    },
    {
        name: "Macedonia, The Former Yugoslav Republic of",
        code: "MK"
    },
    {
        name: "Madagascar",
        code: "MG"
    },
    {
        name: "Malawi",
        code: "MW"
    },
    {
        name: "Malaysia",
        code: "MY"
    },
    {
        name: "Maldives",
        code: "MV"
    },
    {
        name: "Mali",
        code: "ML"
    },
    {
        name: "Malta",
        code: "MT"
    },
    {
        name: "Marshall Islands",
        code: "MH"
    },
    {
        name: "Martinique",
        code: "MQ"
    },
    {
        name: "Mauritania",
        code: "MR"
    },
    {
        name: "Mauritius",
        code: "MU"
    },
    {
        name: "Mayotte",
        code: "YT"
    },
    {
        name: "Mexico",
        code: "MX"
    },
    {
        name: "Micronesia, Federated States of",
        code: "FM"
    },
    {
        name: "Moldova, Republic of",
        code: "MD"
    },
    {
        name: "Monaco",
        code: "MC"
    },
    {
        name: "Mongolia",
        code: "MN"
    },
    {
        name: "Montserrat",
        code: "MS"
    },
    {
        name: "Morocco",
        code: "MA"
    },
    {
        name: "Mozambique",
        code: "MZ"
    },
    {
        name: "Myanmar",
        code: "MM"
    },
    {
        name: "Namibia",
        code: "NA"
    },
    {
        name: "Nauru",
        code: "NR"
    },
    {
        name: "Nepal",
        code: "NP"
    },
    {
        name: "Netherlands",
        code: "NL"
    },
    {
        name: "Netherlands Antilles",
        code: "AN"
    },
    {
        name: "New Caledonia",
        code: "NC"
    },
    {
        name: "New Zealand",
        code: "NZ"
    },
    {
        name: "Nicaragua",
        code: "NI"
    },
    {
        name: "Niger",
        code: "NE"
    },
    {
        name: "Nigeria",
        code: "NG"
    },
    {
        name: "Niue",
        code: "NU"
    },
    {
        name: "Norfolk Island",
        code: "NF"
    },
    {
        name: "Northern Mariana Islands",
        code: "MP"
    },
    {
        name: "Norway",
        code: "NO"
    },
    {
        name: "Oman",
        code: "OM"
    },
    {
        name: "Pakistan",
        code: "PK"
    },
    {
        name: "Palau",
        code: "PW"
    },
    {
        name: "Palestinian Territory, Occupied",
        code: "PS"
    },
    {
        name: "Panama",
        code: "PA"
    },
    {
        name: "Papua New Guinea",
        code: "PG"
    },
    {
        name: "Paraguay",
        code: "PY"
    },
    {
        name: "Peru",
        code: "PE"
    },
    {
        name: "Philippines",
        code: "PH"
    },
    {
        name: "Pitcairn",
        code: "PN"
    },
    {
        name: "Poland",
        code: "PL"
    },
    {
        name: "Portugal",
        code: "PT"
    },
    {
        name: "Puerto Rico",
        code: "PR"
    },
    {
        name: "Qatar",
        code: "QA"
    },
    {
        name: "Reunion",
        code: "RE"
    },
    {
        name: "Romania",
        code: "RO"
    },
    {
        name: "Russian Federation",
        code: "RU"
    },
    {
        name: "RWANDA",
        code: "RW"
    },
    {
        name: "Saint Helena",
        code: "SH"
    },
    {
        name: "Saint Kitts and Nevis",
        code: "KN"
    },
    {
        name: "Saint Lucia",
        code: "LC"
    },
    {
        name: "Saint Pierre and Miquelon",
        code: "PM"
    },
    {
        name: "Saint Vincent and the Grenadines",
        code: "VC"
    },
    {
        name: "Samoa",
        code: "WS"
    },
    {
        name: "San Marino",
        code: "SM"
    },
    {
        name: "Sao Tome and Principe",
        code: "ST"
    },
    {
        name: "Saudi Arabia",
        code: "SA"
    },
    {
        name: "Senegal",
        code: "SN"
    },
    {
        name: "Serbia and Montenegro",
        code: "CS"
    },
    {
        name: "Seychelles",
        code: "SC"
    },
    {
        name: "Sierra Leone",
        code: "SL"
    },
    {
        name: "Singapore",
        code: "SG"
    },
    {
        name: "Slovakia",
        code: "SK"
    },
    {
        name: "Slovenia",
        code: "SI"
    },
    {
        name: "Solomon Islands",
        code: "SB"
    },
    {
        name: "Somalia",
        code: "SO"
    },
    {
        name: "South Africa",
        code: "ZA"
    },
    {
        name: "South Georgia and the South Sandwich Islands",
        code: "GS"
    },
    {
        name: "Spain",
        code: "ES"
    },
    {
        name: "Sri Lanka",
        code: "LK"
    },
    {
        name: "Sudan",
        code: "SD"
    },
    {
        name: "Suriname",
        code: "SR"
    },
    {
        name: "Svalbard and Jan Mayen",
        code: "SJ"
    },
    {
        name: "Swaziland",
        code: "SZ"
    },
    {
        name: "Sweden",
        code: "SE"
    },
    {
        name: "Switzerland",
        code: "CH"
    },
    {
        name: "Syrian Arab Republic",
        code: "SY"
    },
    {
        name: "Taiwan, Province of China",
        code: "TW"
    },
    {
        name: "Tajikistan",
        code: "TJ"
    },
    {
        name: "Tanzania, United Republic of",
        code: "TZ"
    },
    {
        name: "Thailand",
        code: "TH"
    },
    {
        name: "Timor-Leste",
        code: "TL"
    },
    {
        name: "Togo",
        code: "TG"
    },
    {
        name: "Tokelau",
        code: "TK"
    },
    {
        name: "Tonga",
        code: "TO"
    },
    {
        name: "Trinidad and Tobago",
        code: "TT"
    },
    {
        name: "Tunisia",
        code: "TN"
    },
    {
        name: "Turkey",
        code: "TR"
    },
    {
        name: "Turkmenistan",
        code: "TM"
    },
    {
        name: "Turks and Caicos Islands",
        code: "TC"
    },
    {
        name: "Tuvalu",
        code: "TV"
    },
    {
        name: "Uganda",
        code: "UG"
    },
    {
        name: "Ukraine",
        code: "UA"
    },
    {
        name: "United Arab Emirates",
        code: "AE"
    },
    {
        name: "United Kingdom",
        code: "GB"
    },
    {
        name: "United States",
        code: "US"
    },
    {
        name: "United States Minor Outlying Islands",
        code: "UM"
    },
    {
        name: "Uruguay",
        code: "UY"
    },
    {
        name: "Uzbekistan",
        code: "UZ"
    },
    {
        name: "Vanuatu",
        code: "VU"
    },
    {
        name: "Venezuela",
        code: "VE"
    },
    {
        name: "Viet Nam",
        code: "VN"
    },
    {
        name: "Virgin Islands, British",
        code: "VG"
    },
    {
        name: "Virgin Islands, U.S.",
        code: "VI"
    },
    {
        name: "Wallis and Futuna",
        code: "WF"
    },
    {
        name: "Western Sahara",
        code: "EH"
    },
    {
        name: "Yemen",
        code: "YE"
    },
    {
        name: "Zambia",
        code: "ZM"
    },
    {
        name: "Zimbabwe",
        code: "ZW"
    }
];


/***/ }),

/***/ 2987:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "R": () => (/* binding */ handleNotification)
/* harmony export */ });
/* unused harmony export sendEmail */
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1492);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6201);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(236);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__, firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_2__, _firebase__WEBPACK_IMPORTED_MODULE_3__]);
([axios__WEBPACK_IMPORTED_MODULE_0__, firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, react_hot_toast__WEBPACK_IMPORTED_MODULE_2__, _firebase__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);




const handleNotification = async (userData, data, user, giveAwayStatus = "created")=>{
    const { text  } = getGiveAwayText(giveAwayStatus, userData?.company_name);
    const notificationPayload = {
        title: data?.title,
        body: text,
        icon: "logo-here.png",
        click_action: "https://freebling.io/company/giveaway/" + data?.uid
    };
    try {
        const followers = await getFollowersData(false, userData);
        const url = new URL("/api/sendNotifications", window.location.href);
        const finalRes = [];
        const excludeCurrentUser = followers.filter((elem)=>elem.uid !== user.uid);
        for (const follower of excludeCurrentUser){
            if (follower.token) {
                const payload = {
                    to: follower.token,
                    notificationPayload
                };
                const response = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].post(url.toString(), payload);
                finalRes.push(response.data);
            }
        }
    } catch (error) {
        console.log(error);
    }
};
const sendEmail = async (userData, data, user, giveAwayStatus = "created")=>{
    const followers = await getFollowersData(true, userData);
    const excludeCurrentUser = followers.filter((elem)=>elem !== user?.uid);
    for (const email of excludeCurrentUser){
        if (email) {
            const { title , text  } = getGiveAwayText(giveAwayStatus, userData?.company_name);
            const payload = {
                to: email,
                from: user.email,
                subject: title,
                text,
                html: `"<a href={https://freebling.io/company/giveaway/${data?.uid}}>Checkout ${giveAwayStatus === "created" ? "new" : "updated"} Giveaway</a>"`
            };
            try {
                const response = await axios.post("/api/email", payload);
                if (response.data.status === "success") {
                    toast.success("Email sent");
                    return;
                }
            } catch (error) {
                const errMsg = error?.response?.data?.message + "\n\n" + error?.response?.data?.error;
                toast.error(errMsg ?? "Email failed");
            }
        }
    }
};
const getFollowersData = async (isEmail = false, userData)=>{
    const followers = userData?.followers ?? [];
    const users = [];
    for (const userID of followers){
        const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_3__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.where)("uid", "==", userID));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.getDocs)(qry);
        querySnapshot.docs.forEach((doc)=>{
            const userData = doc.data();
            if (isEmail) {
                if (userData["emailNotifications"]) {
                    users.push(userData.email);
                }
            } else {
                if (userData.notifications || userData.notification) {
                    users.push({
                        uid: userData.uid,
                        token: userData.notificationToken
                    });
                }
            }
        });
    }
    return users;
};
function getGiveAwayText(giveAwayStatus, company_name) {
    const giveAwayText = {
        created: {
            title: "New GiveAway Created",
            text: `New Giveaway Has Been Created by ${company_name}`
        },
        updated: {
            title: "GiveAway Updated",
            text: `A Giveaway Has Been Updated by ${company_name}`
        }
    };
    return giveAwayText[giveAwayStatus];
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 4602:
/***/ (() => {



/***/ })

};
;