"use strict";
exports.id = 83;
exports.ids = [83];
exports.modules = {

/***/ 1083:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "v": () => (/* binding */ useUserData)
/* harmony export */ });
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(236);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _authcontext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8762);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1492);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_firebase__WEBPACK_IMPORTED_MODULE_0__, _authcontext__WEBPACK_IMPORTED_MODULE_2__, firebase_firestore__WEBPACK_IMPORTED_MODULE_3__]);
([_firebase__WEBPACK_IMPORTED_MODULE_0__, _authcontext__WEBPACK_IMPORTED_MODULE_2__, firebase_firestore__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);




function useUserData() {
    const { user  } = (0,_authcontext__WEBPACK_IMPORTED_MODULE_2__/* .useAuth */ .a)();
    const [userData, setUserData] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        // turn off realtime subscription
        let unsubscribe;
        const fetchUserData = async ()=>{
            if (user?.uid !== null) {
                const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_0__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.where)("uid", "==", user?.uid));
                const ref = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.getDocs)(qry);
                unsubscribe = ref.forEach((doc)=>{
                    const data = doc.data();
                    setUserData(data);
                });
            } else {
                setUserData(null);
            }
        };
        fetchUserData();
        return unsubscribe;
    }, [
        user
    ]);
    // get the updated data when needed or this method can be called from other classes
    // to get the updated data
    const updateUserData = async ()=>{
        if (user?.uid !== null) {
            const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_0__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.where)("uid", "==", user?.uid));
            const ref = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.getDocs)(qry);
            const unsubscribe = ref.forEach((doc)=>{
                const data = doc.data();
                setUserData(data);
                localStorage.setItem("user", JSON.stringify(data));
            });
        } else {
            setUserData(null);
        }
    };
    return {
        user,
        userData,
        updateUserData
    };
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;