"use strict";
exports.id = 760;
exports.ids = [760];
exports.modules = {

/***/ 5791:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/giveaway_img.8e8ceb33.png","height":192,"width":192,"blurDataURL":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAMAAADz0U65AAAAmVBMVEWbzMyJsbd2lZyVqFkmjppOen4Mbb1WkLA4Y2pwio/gs0vQqZQlmX4kb6GEgYFho9G5t7iWtcNaq89SpqsMlNaKY3GjJii/RFHjLSZ1eH4JZ8vG0c5jw8E1gcFHZXCvrrQMjKzUx8PmsjLkt29geoasl0a9rG3b5+XRvWdElM5QUFVuwM1mqNhyJDojlOu6Ym44g55KaIebclknrJSeAAAAGXRSTlP+/v7+/urq/v7+6v7+/v7+/v7+/v7+/v7+5Ww0fwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAEtJREFUeJwFwQUCgCAABLCzuxMBsbv//zg32K0Mn6waOmeYyRbUUHqVITxquItBqcVNmKOxs/jKIE1WlKRPgcbj4nzVG3pVibz8tB/HdQXhEZLtwAAAAABJRU5ErkJggg==","blurWidth":8,"blurHeight":8});

/***/ }),

/***/ 2612:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/viewAll_arrow_icon.cdc0f797.svg","height":12,"width":20});

/***/ }),

/***/ 2760:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ CampaignsList),
/* harmony export */   "y": () => (/* binding */ ExpiredNotice)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_assets_images_giveaway_img_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5791);
/* harmony import */ var _public_assets_images_viewAll_arrow_icon_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2612);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8762);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4449);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_countdown__WEBPACK_IMPORTED_MODULE_7__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_6__]);
_context_authcontext__WEBPACK_IMPORTED_MODULE_6__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];








const ExpiredNotice = (props)=>{
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: " text-lightWhite font-Ubuntu-Medium",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: "block text-xs mb-2"
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                className: "text-sm ml-0.5 text-white font-Ubuntu-Bold",
                children: props?.msg
            })
        ]
    });
};
const renderer = ({ days , hours , minutes , seconds , completed  })=>{
    if (completed) {
        // Render a complete state
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(ExpiredNotice, {});
    } else {
        // Render a countdown
        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
            className: "text-sm text-lightWhite font-Ubuntu-Medium",
            children: [
                "Ending in:",
                " ",
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "ml-0.5 text-white font-Ubuntu-Bold",
                    children: [
                        days,
                        "d:"
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "ml-0.5 text-white font-Ubuntu-Bold",
                    children: [
                        hours,
                        "h:"
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "ml-0.5 text-white font-Ubuntu-Bold",
                    children: [
                        minutes,
                        "m:"
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "ml-0.5 text-white font-Ubuntu-Bold",
                    children: [
                        seconds,
                        "s"
                    ]
                })
            ]
        });
    }
};
function CampaignsList(props) {
    const id = props?.data?.uid;
    const { setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_6__/* .useAuth */ .a)();
    const data = props?.data;
    const prize = data?.prize;
    const firstPrize = prize?.[0];
    const endDate = data?.endDate;
    const endDateTime = new Date(endDate).getTime(); // convert end date to milliseconds
    const nowDateTime = Date.now(); // get current date time in milliseconds
    const diffTime = endDateTime - nowDateTime;
    const [image, setImage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    function goToPreview() {
        next_router__WEBPACK_IMPORTED_MODULE_5___default().push("/company/giveaway/" + data?.uid);
        setFormValues(data);
    }
    // update prize image if any
    // add a loader until it gets the userData
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (firstPrize?.image) {
            setImage(firstPrize?.image);
        } else {
            setImage(undefined);
        }
    }, [
        firstPrize?.image
    ]);
    const editGiveaway = ()=>{
        // route to giveaway/id and pass the props.props sa object
        next_router__WEBPACK_IMPORTED_MODULE_5___default().push("/company/giveaway/edit/" + id);
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "border-[1.37px] border-jade-100 rounded-lg",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "py-4 px-4 md:py-6 md:pl-6 md:pr-8",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "flex flex-row items-stretch gap-x-8",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "block",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "rounded-3sm w-32 h-32 md:w-48 md:h-48",
                                children: image ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                    className: "rounded-3sm",
                                    src: image,
                                    alt: "giveaway_art",
                                    height: 192,
                                    width: 192
                                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                    className: "rounded-3sm",
                                    src: _public_assets_images_giveaway_img_png__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
                                    alt: "giveaway_art",
                                    height: 192,
                                    width: 192
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "w-full",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex flex-col h-full",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                className: "text-sm font-Ubuntu-Medium mb-2 md:text-xl",
                                                children: data?.title
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countdown__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                date: Date.now() + diffTime,
                                                renderer: renderer
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "mt-auto",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "flex flex-col justify-between md:flex-row",
                                            children: [
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "text-sm text-lightWhite font-Ubuntu-Medium",
                                                            children: "Total entries"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "text-[20px] leading-[25px] text-teal-100 font-Ubuntu-Bold md:text-3xl",
                                                            children: data?.totalEntries || 0
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: editGiveaway,
                                                    className: "flex items-center mt-auto md:justify-center",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "text-xs uppercase mr-2.5",
                                                        children: "Edit details"
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                    onClick: goToPreview,
                                                    className: "flex items-center mt-auto md:justify-center",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "text-xs uppercase mr-2.5",
                                                            children: "View details"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                            src: _public_assets_images_viewAll_arrow_icon_svg__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
                                                            alt: "arrow_icon",
                                                            layout: "fixed",
                                                            width: 18
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    ]
                })
            })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;