"use strict";
exports.id = 914;
exports.ids = [914];
exports.modules = {

/***/ 2914:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ PreviewData)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _PrizeTemp__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6704);
/* harmony import */ var _TasksList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(8311);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1083);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1492);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(236);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6201);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(8762);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _CampaignsList__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2760);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3069);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(4602);
/* harmony import */ var react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal_styles_css__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(4449);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_countdown__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _loader__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3628);
/* harmony import */ var _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(467);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TasksList__WEBPACK_IMPORTED_MODULE_4__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__, firebase_firestore__WEBPACK_IMPORTED_MODULE_6__, _firebase__WEBPACK_IMPORTED_MODULE_7__, react_hot_toast__WEBPACK_IMPORTED_MODULE_8__, _context_authcontext__WEBPACK_IMPORTED_MODULE_9__, _CampaignsList__WEBPACK_IMPORTED_MODULE_11__, _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__]);
([_TasksList__WEBPACK_IMPORTED_MODULE_4__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__, firebase_firestore__WEBPACK_IMPORTED_MODULE_6__, _firebase__WEBPACK_IMPORTED_MODULE_7__, react_hot_toast__WEBPACK_IMPORTED_MODULE_8__, _context_authcontext__WEBPACK_IMPORTED_MODULE_9__, _CampaignsList__WEBPACK_IMPORTED_MODULE_11__, _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);

















const renderer = ({ days , hours , minutes , seconds , completed  })=>{
    if (completed) {
        // Render a complete state
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CampaignsList__WEBPACK_IMPORTED_MODULE_11__/* .ExpiredNotice */ .y, {
            msg: "ENDED"
        });
    } else {
        // Render a countdown
        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "flex flex-col justify-center items-center text-white space-x-2",
            children: [
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "font-Ubuntu-Bold text-2xl",
                    children: [
                        days,
                        " ",
                        hours.toString().padStart(2, "0"),
                        ":",
                        minutes.toString().padStart(2, "0"),
                        ":",
                        seconds.toString().padStart(2, "0")
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "font-Ubuntu-Regular text-xs space-x-4",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: "Days"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: "Hours"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: "Minutes"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: "Seconds"
                        })
                    ]
                })
            ]
        });
    }
};
function PreviewData(props) {
    const { data , setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_9__/* .useAuth */ .a)();
    let id = (next_router__WEBPACK_IMPORTED_MODULE_10___default().query.id);
    const [localData, setLocalData] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const endDate = localData?.endDate;
    const endDateTime = new Date(endDate).getTime(); // convert end date to milliseconds
    const nowDateTime = Date.now(); // get current date time in milliseconds
    let diffTime = endDateTime - nowDateTime;
    const { userData , updateUserData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_5__/* .useUserData */ .v)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [viewMode, setViewMode] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [image, setImage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const firstPrize = data?.prize?.[0];
    const [percentage, setPercent] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("0%");
    const [userEntries, setUserEntries] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
    const [doneTasks, setDoneTasks] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const [open, setOpen] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const userParse = localStorage.getItem("user") || "";
    let user;
    if (userParse) {
        user = JSON.parse(userParse);
    }
    const [disabled, setDisabled] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const onOpenModal = ()=>{
        setOpen(true);
    };
    const onCloseModal = ()=>setOpen(false);
    const [companyData, setCompanyData] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    // update viewMode if id is not null , undefined or empty string using useEffect
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setLoading(true);
        if ((next_router__WEBPACK_IMPORTED_MODULE_10___default().query.id)) {
            id = (next_router__WEBPACK_IMPORTED_MODULE_10___default().query.id);
            setViewMode(true);
        }
    }, [
        id,
        (next_router__WEBPACK_IMPORTED_MODULE_10___default().query.id)
    ]);
    // getData if data is null or undefined from db in view mode
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (viewMode) {
            setLoading(true);
            const getData = async ()=>{
                const q = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.where)("uid", "==", id));
                const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.getDocs)(q);
                querySnapshot.forEach((doc)=>{
                    const data = doc.data();
                    const endDate = data?.endDate;
                    const endDateTime = new Date(endDate).getTime(); // convert end date to milliseconds
                    const nowDateTime = Date.now();
                    diffTime = endDateTime - nowDateTime;
                    setLocalData(data);
                    setFormValues(data);
                });
            };
            if (id && !localData?.uid) getData();
        }
        if (user && userData?.userType === "user" || user?.userType === "user") {
            const usertemp = userData || user;
            // calculate noOfEntries of this user from participatedGiveaways where giveawayId == id
            const tasksDoneByUser = usertemp?.participatedGiveaways?.filter((tasks)=>tasks.giveawayId === id);
            let sum = 0;
            setDoneTasks(tasksDoneByUser);
            tasksDoneByUser?.forEach((giveaway)=>{
                sum += Number(giveaway.noOfEntries);
            });
            setUserEntries(sum);
            const percentage = tasksDoneByUser?.length / localData?.tasks?.length * 100;
            console.log(percentage);
            setPercent(percentage + "%");
        } else setLoading(false);
    }, [
        userData?.uid,
        viewMode
    ]);
    // update image if firstPrize.image is not null or undefined
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (firstPrize?.image) {
            setImage(firstPrize?.image);
            console.log(diffTime);
        } else {
            setImage(undefined);
        }
    }, [
        firstPrize?.image
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (localData?.user_uid && viewMode) {
            const getUserData = async ()=>{
                const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.where)("uid", "==", localData?.user_uid));
                const ref = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.getDocs)(qry);
                ref.forEach((doc)=>{
                    const data = doc.data();
                    setCompanyData(data);
                });
            };
            getUserData();
            setLoading(false);
        } else {
            setCompanyData(userData);
            setLoading(false);
        }
    }, [
        localData
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        // check if user have the data required by the giveaway
        if (user) {
            if (localData?.reqFullName || localData?.reqEmail || localData?.reqCrypto || localData?.reqCountry) {
                if (// !user?.name ||
                !user?.firstName || !user?.lastName || !user?.email || !user?.erc20 || !user.bnb || !user?.country) {
                    if (user?.uid && user?.userType === "user" || userData?.userType === "user") react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].error("You need to finish your profile to participate");
                } else setDisabled(false);
            }
        }
    }, [
        user?.name,
        user?.firstName,
        user?.lastName,
        user?.email,
        user?.erc20,
        user?.bnb,
        user?.country,
        localData?.reqFullName,
        localData?.reqEmail,
        localData?.reqCrypto,
        localData?.reqAddress
    ]);
    function generateUID() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    // to save as draft
    async function saveAsTemp() {
        // add a property to data name draft
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            template: true,
            draft: false
        };
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.setDoc)(ref, tempData);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].success("GiveAway Saved as Template");
        } catch (e) {
            react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].error(e);
        }
        setFormValues("submitted");
        next_router__WEBPACK_IMPORTED_MODULE_10___default().push("/company/giveaway");
    }
    async function submitCampaign() {
        const tempData = {
            ...data,
            user_uid: userData?.uid,
            uid: generateUID(),
            template: false,
            draft: false
        };
        setLoading(true);
        try {
            const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway", tempData.uid);
            await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.setDoc)(ref, tempData);
            setLoading(false);
            next_router__WEBPACK_IMPORTED_MODULE_10___default().push("/company/giveaway");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].success("GiveAway Created");
        } catch (e) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_8__["default"].error(e);
        }
        setLoading(false);
        setFormValues("submitted");
    }
    async function updateLocalData() {
        const q = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_7__.db, "giveaway"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.where)("uid", "==", id));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_6__.getDocs)(q);
        querySnapshot.forEach((doc)=>{
            const data = doc.data();
            setLocalData(data);
            setFormValues(data);
            setLoading(false);
        });
        updateUserData();
        if (userData?.userType === "user" || user.userType === "user") {
            const usertemp = userData || user;
            // calculate noOfEntries of this user from participatedGiveaways where giveawayId == id
            const tasksDoneByUser = usertemp?.participatedGiveaways?.filter((tasks)=>tasks.giveawayId === id);
            let sum = 0;
            setDoneTasks(tasksDoneByUser);
            tasksDoneByUser?.forEach((giveaway)=>{
                sum += Number(giveaway.noOfEntries);
            });
            setUserEntries(sum);
            // calculate percentage of tasks done by user and total tasks in the giveaway
            const percentage = tasksDoneByUser?.length / localData?.tasks?.length * 100;
            console.log(percentage);
            setPercent(percentage + "%");
        }
    }
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "w-full mx-auto flex flex-col space-y-4",
        children: [
            loading && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_loader__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                show: loading
            }),
            !loading && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex flex-col bg-teal-400 px-3 py-6 md:px-5 md:py-8 rounded-sm w-full",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex flex-col md:flex-row spaxe-x-0 md:space-x-5 space-y-4 md:space-y-0 justify-between",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "w-full md:w-1/2",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex flex-row justify-center items-center mb-3.5 w-full",
                                                children: [
                                                    userData && userData?.userType !== "company" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "flex flex-col text-white text-center pr-6",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-Ubuntu-Bold text-xl md:text-2xl",
                                                                children: userEntries
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-Ubuntu-Regular text-xs",
                                                                children: "Your Entries"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex flex-col justify-center items-center md:border md:border-y-0 md:px-6 border-x-teal-300/20",
                                                        children: diffTime && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countdown__WEBPACK_IMPORTED_MODULE_14___default()), {
                                                            className: "font-Ubuntu-Bold text-xl md:text-2xl",
                                                            date: Date.now() + diffTime,
                                                            renderer: renderer
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "flex flex-col text-white text-center pl-6",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-Ubuntu-Bold text-xl md:text-2xl",
                                                                children: localData?.totalEntries || 0
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-Ubuntu-Regular text-xs",
                                                                children: "Total entries"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            image ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                src: image,
                                                className: "w-full",
                                                width: 529,
                                                height: 225,
                                                alt: "Giveaeway featured image"
                                            }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                src: "/assets/images/ft-image-placeholder-st.jpg",
                                                className: "w-full",
                                                width: 529,
                                                height: 225,
                                                alt: "Giveaeway featured image"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                className: "text-xl font-Ubuntu-Medium w-full md:text-2xl lg:w-auto my-3.5",
                                                children: localData?.title
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "flex font-Ubuntu-Bold items-center cursor-pointer mb-2 md:mb-0",
                                                children: companyData && companyData?.imgUrl ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                            src: companyData?.imgUrl,
                                                            className: "w-10 h-10 mr-3 rounded-full",
                                                            width: 25,
                                                            height: 25,
                                                            alt: "Company Logo"
                                                        }),
                                                        " ",
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "text-white hover:text-teal-300 text-lg",
                                                            children: companyData?.company_name || companyData?.name
                                                        })
                                                    ]
                                                }) : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                                            src: "/assets/images/user_profile.png",
                                                            className: "w-10 h-10 mr-3 rounded-full",
                                                            width: 25,
                                                            height: 25,
                                                            alt: "Company Logo"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                            className: "text-white hover:text-teal-300 text-lg"
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "bg-teal-500 py-2 px-3 rounded-sm w-full md:w-1/2",
                                        children: [
                                            user && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "w-full h-auto rounded-sm bg-teal-500 space-y-px",
                                                children: [
                                                    user?.userType === "user" && localData?.tasks?.map((task, index)=>{
                                                        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TasksList__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                                                            data: task,
                                                            giveawayId: id,
                                                            tasksDone: doneTasks,
                                                            disabled: disabled,
                                                            updateLocalData: updateLocalData
                                                        }, index);
                                                    }),
                                                    viewMode && userData?.userType === "user" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "flex flex-col mt-5",
                                                        children: [
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                                                className: "flex flex-row text-base font-Ubuntu-Regular justify-between",
                                                                children: [
                                                                    "Tasks Completed",
                                                                    " ",
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                        className: "text-teal-100 text-[20px]",
                                                                        children: [
                                                                            doneTasks?.length,
                                                                            "/",
                                                                            localData?.tasks?.length || 0
                                                                        ]
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "border-[2px] border-teal-900/20 relative h-[20px] w-full rounded-full p-1 mt-1",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                    style: {
                                                                        width: percentage
                                                                    },
                                                                    className: "rounded-full absolute top-0 left-0 flex h-full items-center justify-center text-xs font-Ubuntu-Regular text-white bg-gradient-to-r from-[#139BAD]/40 to-[#139BAD]"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    !viewMode && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "mt-10 md:mt-[60px] lg:mt-auto lg:ml-auto px-1 py-3",
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "flex flex-col-reverse gap-y-6 md:gap-y-0 md:gap-x-[34px] md:flex-row lg:gap-x-8 lg:justify-end",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                    onClick: saveAsTemp,
                                                                    className: "buttonPrimary",
                                                                    children: "Save as Template"
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                    onClick: submitCampaign,
                                                                    className: "buttonPrimary",
                                                                    children: "Save"
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            }),
                                            !user && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "w-full h-auto md:w-1/3 rounded-sm p-5 mt-10 md:mt-0 bg-teal-500",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex flex-1 flex-col mt-5",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                            className: "flex flex-row text-base font-Ubuntu-Regular justify-center",
                                                            children: "Sign In To Participate"
                                                        })
                                                    })
                                                })
                                            })
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "group",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                        className: "flex text-lg font-Ubuntu-Bold items-center space-x-1.5 mt-10",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__.BellAlertIcon, {
                                                className: "w-5 h-5 text-white"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "About Event"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: "text-white/40 font-Ubuntu-Regular text-sm group-hover:text-white ease-linear duration-300",
                                        children: localData?.description
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "group",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                        className: "flex text-lg font-Ubuntu-Bold items-center space-x-1.5 mt-7",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__.ListBulletIcon, {
                                                className: "w-5 h-5 text-white"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "How to Participate"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: "text-white/40 group-hover:text-white ease-linear duration-300",
                                        children: "Complete the tasks in Task list to participate"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                    className: "flex text-lg font-Ubuntu-Bold items-center space-x-1.5 mt-7",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__.InformationCircleIcon, {
                                            className: "w-5 h-5 text-white"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "hover:cursor-pointer hover:text-teal-300",
                                            onClick: onOpenModal,
                                            children: "Terms and Conditions"
                                        })
                                    ]
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex flex-col bg-teal-400 px-3 py-6 md:px-16 md:py-8 rounded-sm w-full",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "flex justify-between",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                    className: "flex text-lg font-Ubuntu-Bold items-center space-x-1.5 mb-4",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__.GiftIcon, {
                                            className: "w-5 h-5 text-white"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            children: "Prizes"
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "grid grid-cols-1 items-center gap-y-2.5 md:gap-x-2.5 md:gap-y-7 md:grid-cols-2 lg:grid-cols-4",
                                children: localData?.prize?.map((prize, index)=>{
                                    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_PrizeTemp__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                                        data: prize
                                    }, index);
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex flex-col bg-teal-400 px-3 py-6 md:px-16 md:py-8 rounded-sm w-full",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "flex justify-between",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
                                    className: "flex text-lg font-Ubuntu-Bold items-center space-x-1.5 mb-4",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_16__.TrophyIcon, {
                                            className: "w-5 h-5 text-white"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            children: "Leaderboard"
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex flex-col rounded-sm w-full",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex bg-teal-700 space-x-24 mb-3 rounded-sm",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "p-2 font-bold text-center w-1/4 rounded-sm",
                                                children: "Count"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "flex-1 p-2 font-bold rounded-sm",
                                                children: "Username"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "p-2 font-bold w-1/4 rounded-sm",
                                                children: "Entries"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-col",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-leaderb-100 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#1"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "Alpha123"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "91"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-leaderb-100/50 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#2"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "Atlantean"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "57"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-leaderb-100/25 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#3"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "xEmiel"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "56"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#4"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "ElectricDragon"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "55"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-teal-700 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#5"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "TangoGamer"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "55"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#6"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "Djjdh11"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "55"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-teal-700 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#7"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "Aeld2"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "54"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#8"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "DregSon"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "53"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-teal-700 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#9"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "TangoGamer"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "50"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#10"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "Thoth"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "49"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "flex bg-teal-300 space-x-24 rounded-sm",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 text-center w-1/4 rounded-sm",
                                                        children: "#111"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "flex-1 p-2 rounded-sm",
                                                        children: "TumblingTortoise (You)"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "p-2 w-1/4 rounded-sm",
                                                        children: "25"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_responsive_modal__WEBPACK_IMPORTED_MODULE_12___default()), {
                        open: open,
                        onClose: onCloseModal,
                        center: true,
                        classNames: {
                            overlay: "popupTasksOverlay",
                            modal: "!w-full !p-6 !m-0 md:!max-w-[844px] !bg-transparent"
                        },
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "bg-[#101B1B] border-[1.5px] border-jade-100 rounded-md p-6 md:p-10",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "w-full",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                        children: "Terms and Conditions"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        children: localData?.toc || ""
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 6704:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ prizeTmp)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_2__);



function prizeTmp(props) {
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "bg-teal-500 p-2.5 rounded-sm",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "flex gap-x-3",
            children: [
                props?.data?.image ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                    className: "rounded-sm",
                    src: props?.data?.image,
                    alt: "gift_box",
                    width: 100,
                    height: 100
                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                    className: "rounded-sm",
                    src: "/assets/images/giftbox.svg",
                    width: 100,
                    height: 100,
                    alt: "gift_box",
                    layout: "intrinsic"
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "flex flex-1 flex-row justify-between",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex-auto",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h6", {
                                className: "text-xs font-Ubuntu-Bold",
                                children: props?.data?.prizeName
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: " text-white/40 text-xs font-Ubuntu-Regular truncate w-full max-w-[105px] p-0 m-0",
                                children: "This items fulfills to GYRI. Fires healing energy at your allies. Can also be used to deal light damage to enemies."
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                className: "text-sm font-Ubuntu-Medium self-end",
                                children: [
                                    props?.data?.value,
                                    " $"
                                ]
                            })
                        ]
                    })
                })
            ]
        })
    });
}


/***/ }),

/***/ 8311:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ TasksList)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1492);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(236);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1083);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(6201);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(467);
/* harmony import */ var _helpers_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5329);
/* harmony import */ var _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6630);
/* harmony import */ var _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5960);
/* harmony import */ var _utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(5549);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_firestore__WEBPACK_IMPORTED_MODULE_4__, _firebase__WEBPACK_IMPORTED_MODULE_5__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, react_hot_toast__WEBPACK_IMPORTED_MODULE_7__, _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_9__, _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__, _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__, _utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__]);
([firebase_firestore__WEBPACK_IMPORTED_MODULE_4__, _firebase__WEBPACK_IMPORTED_MODULE_5__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, react_hot_toast__WEBPACK_IMPORTED_MODULE_7__, _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_9__, _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__, _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__, _utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);














const redirectUri = "https://app.freebling.io/redirect";
const clientId = "1058769840468410439";
function TasksList(props) {
    const appId = "906996760301312";
    const [done, setDone] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { userData , updateUserData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_6__/* .useUserData */ .v)();
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_8__.useRouter)();
    const { id , status , tweeterStatus: tStatus , data: Tdata  } = router.query;
    const [referralId, setReferralId] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    const [openModal, setOpenModal] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const url = id;
    const userStatus = status;
    const tweeterStatus = tStatus;
    const twitterData = Tdata;
    let uid = "";
    const userParse = localStorage.getItem("user") || "";
    let user;
    if (userParse) {
        user = JSON.parse(userParse);
        uid = user?.uid || "";
    }
    const handleDiscordUpdate = async (status)=>{
        if (status === "success") {
            router.replace({
                query: {
                    id: url
                }
            });
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.clearAll */ .K.clearAll();
            updateEntries();
        }
        if (status === "error") {
            router.replace({
                query: {
                    id: url
                }
            });
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.clearAll */ .K.clearAll();
            console.log("No Need of an entry");
        }
    };
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (props?.data?.title === "Join Discord") handleDiscordUpdate(userStatus);
    }, [
        userStatus
    ]);
    // titter update entries
    const handleTwitterStatus = async ()=>{
        const updateEntry = async ()=>{
            await updateEntries();
            router.replace({
                query: {
                    id: url
                }
            });
            return;
        };
        if (tweeterStatus === "twitter-error") {
            router.replace({
                query: {
                    id: url
                }
            });
            console.log("No Need of an entry");
            return;
        }
        if (tweeterStatus === "retweet") {
            const data = JSON.parse(twitterData);
            if (data.retweeted) {
                if (props?.data?.title === "Retweet this Twitter post") {
                    // Storage.clearTwitterData();
                    await updateEntry();
                }
            }
        }
        if (tweeterStatus === "tweet") {
            const data1 = JSON.parse(twitterData);
            if (data1.id) {
                if (props?.data?.title === "Post on Twitter") {
                    // Storage.clearTwitterData();
                    await updateEntry();
                }
            }
        }
        if (tweeterStatus === "followUser") {
            const data2 = JSON.parse(twitterData);
            if (data2.following) {
                if (props?.data?.title === "Follow us on Twitter") {
                    // Storage.clearTwitterData();
                    await updateEntry();
                }
            }
        }
    };
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        handleTwitterStatus();
    }, [
        tweeterStatus
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        // Load the Facebook SDK script
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            if (fjs.parentNode) fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk");
        // Initialize the Facebook SDK with your App ID
        window.fbAsyncInit = function() {
            window.FB.init({
                appId: appId,
                xfbml: true,
                version: "v12.0"
            });
        };
        localStorage.setItem("url", url);
    }, []);
    // setDOne to true if its in props?.tasksDone?.title == props?.data?.title
    // if its not in props.tasksDone then setDone to false
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (props?.tasksDone?.length > 0) {
            const temp = props?.tasksDone?.filter((task)=>task?.taskTitle === props?.data?.title);
            if (temp?.length > 0) {
                setDone(true);
            } else {
                setDone(false);
            }
        }
    }, [
        props?.tasksDone
    ]);
    async function shareOnFacebook(url) {
        console.log("share on facebook", url);
        window.FB.ui({
            method: "share",
            href: url
        }, function(response) {
            if (response && !response.error_message) {
                updateEntries();
                react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].success("Task completed successfully");
            // add a giveaway id and task title to the users collection in firestore
            // make a reference of userData
            } else {
                console.log("Error sharing post.");
                react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].error("Error sharing post.");
            }
        });
    }
    const handleToggleModal = ()=>{
        setOpenModal((open)=>!open);
    };
    const generateTelegramUId = ()=>{
        return Math.random().toString(36).substring(2, 7);
    };
    const joinTelegramChat = async ()=>{
        window.open(props?.data?.link, "_blank");
        const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.where)("uid", "==", userData?.uid));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.getDocs)(qry);
        const temp = querySnapshot.docs.map((doc)=>doc.ref.id);
        const userRef = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "users", temp[0]);
        const newTelegramId = generateTelegramUId();
        const ref2 = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "giveaway", props?.giveawayId);
        await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.setDoc)(userRef, {
            referralIds: {
                [newTelegramId]: false
            }
        }, {
            merge: true
        });
        await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.setDoc)(ref2, {
            referralIds: {
                [newTelegramId]: false
            }
        }, {
            merge: true
        });
        setDone(true);
        updateUserData();
        setReferralId(newTelegramId);
        handleToggleModal();
    };
    function performTask() {
        if (props?.data?.title === "Share on FaceBook") {
            shareOnFacebook(props?.data?.link);
        } else if (props?.data?.title === "Retweet this Twitter post") {
            rt(props?.data?.link);
        } else if (props?.data?.title === "Post on Twitter") {
            postTweet(props?.data?.description);
        } else if (props?.data?.title === "Follow us on Twitter") {
            followTwitterAccount(props?.data?.link);
        } else if (props?.data?.title === "Join Discord") {
            joinDiscordServer(props?.data?.link);
        } else if (props?.data?.title === "Follow on Telegram") {
            joinTelegramChat();
        } else if (props?.data?.title === "Visit a URL") {
            openWebsite(props?.data?.link);
        } else {
            react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].success("Task completed successfully");
            updateEntries();
        }
    }
    async function updateEntries() {
        const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.where)("uid", "==", uid || userData?.uid));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.getDocs)(qry);
        const temp = querySnapshot.docs.map((doc)=>{
            // console.log doc ref.id
            return doc.ref.id;
        });
        // get the ref of the doc where is equal to userData.uid
        const ref1 = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "users", temp[0]);
        // add a giveaway id and task title to the users collection in firestore
        await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.updateDoc)(ref1, {
            participatedGiveaways: (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.arrayUnion)({
                taskTitle: props?.data?.title,
                noOfEntries: props?.data?.noOfEntries,
                giveawayId: props?.giveawayId
            })
        });
        // get giveaway and update total entries by adding props.noOfEntries and add participated user to it
        const ref2 = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_5__.db, "giveaway", props?.giveawayId);
        await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.updateDoc)(ref2, {
            totalEntries: (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.increment)(props?.data?.noOfEntries),
            participatedUsers: (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_4__.arrayUnion)({
                userId: userData?.uid || user.uid,
                name: userData?.name || user.name,
                email: userData?.email || user.email
            })
        });
        setDone(true);
        props?.updateLocalData();
        console.log("data updated");
    }
    function getTweetIdFromLink(link) {
        const match = link.match(/\/status\/(\d+)/);
        if (match) {
            return match[1];
        } else {
            return undefined;
        }
    }
    async function rt(URL) {
        const tweetId = getTweetIdFromLink(URL);
        if (tweetId) {
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setGiveawayId */ .K.setGiveawayId(url);
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setTweetData */ .K.setTweetData({
                status: "retweet",
                tweetData: tweetId
            });
            const token = _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            if (token) {
                try {
                    const data = await _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* ["default"].retweet */ .Z.retweet(tweetId);
                    console.log(data);
                    if (data.data.retweeted) {
                        _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.clearTwitterData */ .K.clearTwitterData();
                        await updateEntries();
                    }
                } catch (error) {
                    debugger;
                    console.log(error);
                    (0,_utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* .showToast */ .C)(error?.data?.error?.error?.errors?.[0]?.message ?? "An error occurred while retweeting.");
                }
            } else {
                _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__/* .twitterAuth.login */ .v.login();
            }
        }
    }
    async function postTweet(data) {
        if (data) {
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setGiveawayId */ .K.setGiveawayId(url);
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setTweetData */ .K.setTweetData({
                status: "tweet",
                tweetData: data
            });
            const token = _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            if (token) {
                try {
                    const Tdata = await _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* ["default"].tweet */ .Z.tweet(data);
                    console.log(Tdata);
                    if (Tdata.data.id) {
                        _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.clearTwitterData */ .K.clearTwitterData();
                        await updateEntries();
                    }
                } catch (error) {
                    console.log(error);
                    (0,_utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* .showToast */ .C)("An error occurred while tweeting.");
                }
            } else {
                _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__/* .twitterAuth.login */ .v.login();
            }
        }
    }
    async function followTwitterAccount(screenName) {
        if (screenName) {
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setGiveawayId */ .K.setGiveawayId(url);
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setTweetData */ .K.setTweetData({
                status: "followUser",
                tweetData: screenName
            });
            const token = _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.getTwitterAuthToken */ .K.getTwitterAuthToken();
            if (token) {
                try {
                    const Tdata = await _utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* ["default"].followUser */ .Z.followUser(screenName);
                    console.log(Tdata);
                    if (Tdata.data.following) {
                        _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.clearTwitterData */ .K.clearTwitterData();
                        await updateEntries();
                    }
                } catch (error) {
                    console.log(error);
                    (0,_utils_twitterClientMethods__WEBPACK_IMPORTED_MODULE_12__/* .showToast */ .C)("An error occurred while following the user.");
                }
            } else {
                _utils_twitterAuth__WEBPACK_IMPORTED_MODULE_11__/* .twitterAuth.login */ .v.login();
            }
        }
    }
    // write a function to join a discord server through server invite code and get a call if user joins it
    async function joinDiscordServer(serverId) {
        // get the invite code from the link
        // store the invite code in local storage
        // redirect the user to discord for authentication
        _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setInviteCode */ .K.setInviteCode(JSON.stringify(serverId));
        const discordToken = await _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.getDiscordAccessToken */ .K.getDiscordAccessToken();
        if (discordToken) {
            const userServers = await (0,_utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__/* .getUsersGuilds */ .xk)(discordToken);
            if (userServers?.data && userServers?.data.length > 0) {
                const idSet = new Set(userServers?.data.map((obj)=>obj.id));
                const hasTargetId = idSet.has(serverId);
                if (hasTargetId) {
                    updateEntries();
                    return;
                }
            }
            const userResponse = await (0,_utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__/* .getDiscordUserInfo */ .hF)(discordToken);
            const userData = userResponse?.data;
            if (!userData) {
                react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].error("User data not found in response");
                return;
            }
            _helpers_storage__WEBPACK_IMPORTED_MODULE_10__/* .Storage.setUserData */ .K.setUserData(JSON.stringify(userData));
            const res = await (0,_utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__/* .joinDiscordServerGuild */ .A2)(userData.id, discordToken, serverId);
            console.log(res);
            if (!res.hasError) {
                react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].success(res.message);
            } else {
                react_hot_toast__WEBPACK_IMPORTED_MODULE_7__["default"].error(res.message);
            }
        } else {
            // Redirect the user to Discord for authentication
            (0,_utils_discordUtils__WEBPACK_IMPORTED_MODULE_13__/* .authenticateUser */ .So)(clientId, redirectUri);
        }
    }
    // TODO: Make a refer link and track sign ups through it
    function openWebsite(url) {
        console.log(url);
        if (url) window.open(url, "_blank");
        updateEntries();
    }
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_head__WEBPACK_IMPORTED_MODULE_3___default()), {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("script", {
                    async: true,
                    defer: true,
                    crossOrigin: "anonymous",
                    src: `https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v12.0&appId=${appId}&autoLogAppEvents=1`,
                    nonce: "krUv25gK"
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "group",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex items-center w-full",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                onClick: performTask,
                                disabled: done,
                                className: "flex items-center justify-between w-full space-x-3 text-left",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "flex justify-center bg-tasktw-100 w-10 h-10",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                            src: "/assets/images/task-icons/twitter.svg",
                                            className: "",
                                            width: 17,
                                            height: 17,
                                            alt: "Task Icon"
                                        })
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                        className: "flex-1 text-lg text-white font-Ubuntu-Regular p-0 m-0",
                                        children: [
                                            props?.data?.description,
                                            " "
                                        ]
                                    })
                                ]
                            }),
                            !done && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                className: "font-Ubuntu-Bold text-lg rounded-[3px] flex-none bg-teal-300 min-w-[42px] max-w-16 h-[30px] text-center",
                                children: [
                                    "+ ",
                                    props?.data?.noOfEntries
                                ]
                            }),
                            done && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                className: "borderFB2 px-2 py-1 text-lg rounded-[2px] flex-none min-w-[55px] text-center",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_9__.CheckCircleIcon, {
                                    className: "mx-auto text-teal-400 w-7 h-7"
                                })
                            })
                        ]
                    }),
                    openModal && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "fixed inset-0 z-[99] bg-black bg-opacity-30 backdrop-blur-sm flex items-center justify-center flex-col",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "bg-teal-500 p-[20px] rounded-md w-1/2",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "flex justify-between mb-[20px]",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "font-bold text-white text-[24px]",
                                            children: "Telegram Referral ID"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_9__.XMarkIcon, {
                                            className: "cursor-pointer",
                                            width: "30px",
                                            onClick: handleToggleModal
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        children: [
                                            "Your referral ID:",
                                            " ",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                className: "text-lg font-Ubuntu-Bold",
                                                children: referralId
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "Please enter this when asked in Telegram to verify completion of this task.",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "If you have already joined the group, please add",
                                            " ",
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                                                children: [
                                                    "/join/",
                                                    referralId
                                                ]
                                            }),
                                            " to the text box to complete the task"
                                        ]
                                    })
                                })
                            ]
                        })
                    })
                ]
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;