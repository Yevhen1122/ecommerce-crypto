"use strict";
exports.id = 937;
exports.ids = [937];
exports.modules = {

/***/ 9471:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "R": () => (/* binding */ discordApi)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
/* harmony import */ var _helpers_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5329);
/* harmony import */ var _discordUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5549);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__, _discordUtils__WEBPACK_IMPORTED_MODULE_2__]);
([axios__WEBPACK_IMPORTED_MODULE_0__, _discordUtils__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);



const clientId = "1058769840468410439";
const redirectUri = "https://app.freebling.io/redirect";
const discordApi = axios__WEBPACK_IMPORTED_MODULE_0__["default"].create({
    baseURL: "https://discord.com/api",
    headers: {
        "Content-Type": "application/x-www-form-urlencoded"
    }
});
const payload = {
    client_id: "1058769840468410439",
    client_secret: "lzP1T-XPtklLQ6s1wBjDW7xjEJtCdHGE",
    grant_type: "refresh_token",
    refresh_token: _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.getDiscordRefreshToken */ .K.getDiscordRefreshToken()
};
discordApi.interceptors.response.use((response)=>response, async (error)=>{
    debugger;
    const request = error.config;
    const { status  } = error.response;
    if (status == 401 && !request.retry) {
        request.retry = true;
        const isSuccess = await getDiscordRefreshToken();
        if (isSuccess) return discordApi(request);
        return (0,_discordUtils__WEBPACK_IMPORTED_MODULE_2__/* .authenticateUser */ .So)(clientId, redirectUri);
    }
    return Promise.reject(error.response);
});
async function getDiscordRefreshToken() {
    try {
        const refreshTokenRes = await discordApi.post("/oauth2/token", payload);
        if (refreshTokenRes) {
            _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setDiscordAccessToken */ .K.setDiscordAccessToken(refreshTokenRes.data.access_token);
            _helpers_storage__WEBPACK_IMPORTED_MODULE_1__/* .Storage.setDiscordRefreshToken */ .K.setDiscordRefreshToken(refreshTokenRes.data.refresh_token);
        }
        return true;
    } catch (e) {
        return false;
    }
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5549:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "A2": () => (/* binding */ joinDiscordServerGuild),
/* harmony export */   "So": () => (/* binding */ authenticateUser),
/* harmony export */   "hF": () => (/* binding */ getDiscordUserInfo),
/* harmony export */   "xk": () => (/* binding */ getUsersGuilds)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
/* harmony import */ var _discordAxios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9471);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__, _discordAxios__WEBPACK_IMPORTED_MODULE_1__]);
([axios__WEBPACK_IMPORTED_MODULE_0__, _discordAxios__WEBPACK_IMPORTED_MODULE_1__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);


async function getUsersGuilds(auth_token) {
    const discord_api_url = "/users/@me/guilds";
    const headers = {
        Authorization: `Bearer ${auth_token}`,
        "Content-Type": "application/x-www-form-urlencoded"
    };
    try {
        const response = await _discordAxios__WEBPACK_IMPORTED_MODULE_1__/* .discordApi.get */ .R.get(discord_api_url, {
            headers
        });
        return response;
    } catch (error) {
        console.error(error);
        return null;
    }
}
async function joinDiscordServerGuild(userId, token, serverId) {
    const url = new URL("/api/joinServer", window.location.href);
    let res = {
        hasError: false,
        message: ""
    };
    try {
        const response = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].post(url.toString(), {
            userId,
            token,
            serverId
        });
        console.log(response);
        const { data , message  } = response.data || {};
        if (!data) {
            //   redirectOnError(message);
            res = {
                hasError: true,
                message
            };
        }
        if (data && Object.keys(data).length) {
            const userServers = await getUsersGuilds(token);
            console.log(userServers);
            const idSet = new Set(userServers?.data.map((obj)=>obj.id));
            const hasTargetId = idSet.has(serverId);
            if (hasTargetId) {
                res = {
                    hasError: false,
                    message
                };
            } else {
                res = {
                    hasError: true,
                    message
                };
            }
        }
    } catch (error) {
        console.log(error);
        // redirectOnError("Something went wrong.try agin later");
        res = {
            ...error.response.data || {
                hasError: true,
                message: error?.response?.data.message || "Something went wrong.try agin later"
            }
        };
    }
    return res;
}
async function getDiscordUserInfo(accessToken) {
    let res = null;
    try {
        const userResponse = await _discordAxios__WEBPACK_IMPORTED_MODULE_1__/* .discordApi.get */ .R.get("/users/@me", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
        res = userResponse;
    } catch (error) {
        console.log(error);
        res = null;
    }
    return res;
}
const authenticateUser = (clientId, redirectUri)=>{
    window.location.href = `https://discord.com/api/oauth2/authorize?client_id=${clientId}&redirect_uri=${encodeURIComponent(redirectUri)}&response_type=code&scope=identify%20guilds.join%20guilds`;
};

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;