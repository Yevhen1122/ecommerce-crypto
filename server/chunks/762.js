"use strict";
exports.id = 762;
exports.ids = [762];
exports.modules = {

/***/ 9133:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const configuration = {
    firebase: {
        apiKey: "AIzaSyDB5IupbJOGZdF5Y7aOn6u4ckGIzDkwpYg",
        authDomain: "fb-portal-40078.firebaseapp.com",
        projectId: "fb-portal-40078",
        storageBucket: "fb-portal-40078.appspot.com",
        appId: "1:245861165524:web:9a61480ef22d747e3b8e41",
        messagingSenderId: "245861165524"
    },
    emulatorHost: "",
    emulator: "" === "true"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (configuration);


/***/ }),

/***/ 8762:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "H": () => (/* binding */ AuthContextProvider),
/* harmony export */   "a": () => (/* binding */ useAuth)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(401);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(236);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _firebase__WEBPACK_IMPORTED_MODULE_3__]);
([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _firebase__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);




const AuthContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)({});
const useAuth = ()=>(0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(AuthContext);
const AuthContextProvider = ({ children  })=>{
    const [user, setUser] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
        email: null,
        uid: null
    });
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const [data, setData] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({});
    const [whichAuth, setWhichAuth] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("sign-in");
    const [notification, setNotification] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    const [isNotificationEnabled, setIsNotificationEnable] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [isEmailEnabled, setIsEmailEnabled] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        const unsubscribe = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.onAuthStateChanged)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8, (user)=>{
            if (user) {
                setUser({
                    email: user.email,
                    uid: user?.uid
                });
            } else {
                setUser({
                    email: null,
                    uid: null
                });
            }
        });
        setLoading(false);
        return ()=>unsubscribe();
    }, []);
    const signUp = (email, password)=>{
        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.createUserWithEmailAndPassword)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8, email, password);
    };
    const logIn = (email, password)=>{
        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithEmailAndPassword)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8, email, password);
    };
    const logOut = async ()=>{
        setUser({
            email: null,
            uid: null
        });
        await (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signOut)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8);
        localStorage.clear();
        sessionStorage.clear();
    };
    //sign in using google
    const signInWithGoogle = ()=>(0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithPopup)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8, new firebase_auth__WEBPACK_IMPORTED_MODULE_2__.GoogleAuthProvider());
    //sign in using facebook
    const signInWithFacebook = ()=>(0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithPopup)(_firebase__WEBPACK_IMPORTED_MODULE_3__/* .auth */ .I8, new firebase_auth__WEBPACK_IMPORTED_MODULE_2__.FacebookAuthProvider());
    // creating a custom hook for maintaining states of campaign multi step form
    const setFormValues = (values)=>{
        setData((prevValues)=>({
                ...prevValues,
                ...values
            }));
        if (values == "submitted") {
            setData({});
        }
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(AuthContext.Provider, {
        value: {
            user,
            signUp,
            logIn,
            logOut,
            signInWithFacebook,
            signInWithGoogle,
            data,
            setFormValues,
            notification,
            setNotification,
            authModal: {
                whichAuth,
                setWhichAuth
            },
            isNotificationEnabled,
            setIsNotificationEnable,
            isEmailEnabled,
            setIsEmailEnabled
        },
        children: loading ? null : children
    });
};

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 236:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "I8": () => (/* binding */ auth),
/* harmony export */   "db": () => (/* binding */ db),
/* harmony export */   "tO": () => (/* binding */ storage)
/* harmony export */ });
/* unused harmony export postToJSON */
/* harmony import */ var _configuration__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9133);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3745);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(401);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1492);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3392);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_app__WEBPACK_IMPORTED_MODULE_1__, firebase_auth__WEBPACK_IMPORTED_MODULE_2__, firebase_firestore__WEBPACK_IMPORTED_MODULE_3__, firebase_storage__WEBPACK_IMPORTED_MODULE_4__]);
([firebase_app__WEBPACK_IMPORTED_MODULE_1__, firebase_auth__WEBPACK_IMPORTED_MODULE_2__, firebase_firestore__WEBPACK_IMPORTED_MODULE_3__, firebase_storage__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);


// Import the functions you need from the SDKs you need




// Initialize Firebase
// check if there is already an app intialuaze 
const app = !(0,firebase_app__WEBPACK_IMPORTED_MODULE_1__.getApps)().length ? (0,firebase_app__WEBPACK_IMPORTED_MODULE_1__.initializeApp)(_configuration__WEBPACK_IMPORTED_MODULE_0__/* ["default"].firebase */ .Z.firebase) : (0,firebase_app__WEBPACK_IMPORTED_MODULE_1__.getApps)()[0];
const auth = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.getAuth)();
// export firestore db 
const db = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_3__.getFirestore)(app);
// export firebase storage
const storage = (0,firebase_storage__WEBPACK_IMPORTED_MODULE_4__.getStorage)(app);
function postToJSON(doc) {
    const data = doc.data();
    return {
        ...data,
        // Gotcha! firestore timestamp NOT serializable to JSON. Must convert to milliseconds
        createdAt: data?.createdAt.toMillis() || 0,
        updatedAt: data?.updatedAt.toMillis() || 0
    };
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;