"use strict";
exports.id = 459;
exports.ids = [459];
exports.modules = {

/***/ 9459:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1083);
/* harmony import */ var _CampaignsList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2760);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8762);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1492);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(236);
/* harmony import */ var _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(291);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4449);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_countdown__WEBPACK_IMPORTED_MODULE_10__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_userDataHook__WEBPACK_IMPORTED_MODULE_3__, _CampaignsList__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_6__, firebase_firestore__WEBPACK_IMPORTED_MODULE_7__, _firebase__WEBPACK_IMPORTED_MODULE_8__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__]);
([_context_userDataHook__WEBPACK_IMPORTED_MODULE_3__, _CampaignsList__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_6__, firebase_firestore__WEBPACK_IMPORTED_MODULE_7__, _firebase__WEBPACK_IMPORTED_MODULE_8__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);











const renderer = ({ days , hours , minutes , seconds , completed  })=>{
    if (completed) {
        // Render a complete state
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CampaignsList__WEBPACK_IMPORTED_MODULE_4__/* .ExpiredNotice */ .y, {
            msg: "GIVEAWAY ENDED"
        });
    } else {
        // Render a countdown
        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
            className: "text-sm text-lightWhite font-Ubuntu-Medium",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: "block text-xs mb-2 text-white/50 font-Ubuntu-Regular",
                    children: "Ending in:"
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                    className: "flex text-md md:text-[28px] justify-center font-Ubuntu-Bold",
                    children: [
                        " ",
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            className: "text-white font-familySemibold flex flex-col text-center mr-3 space-y-2",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    children: String(days).padStart(2, "0")
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "text-sm font-Ubuntu-Regular",
                                    children: "days"
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            className: "text-white font-familySemibold flex flex-col text-center space-y-2",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    children: [
                                        hours,
                                        ":"
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: "text-sm font-Ubuntu-Regular",
                                    children: [
                                        "h",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "invisible md:visible",
                                            children: "rs"
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            className: "text-white font-familySemibold flex flex-col space-y-2",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    children: [
                                        minutes,
                                        ":"
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: "text-sm font-Ubuntu-Regular",
                                    children: [
                                        "m",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "invisible md:visible",
                                            children: "ins"
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            className: "text-white font-familySemibold flex flex-col space-y-2",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    children: seconds
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: "text-sm font-Ubuntu-Regular",
                                    children: [
                                        "s",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "invisible md:visible",
                                            children: "ecs"
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
};
function CardGiveaway(props) {
    const status = props?.status;
    const id = props?.giveaway?.uid;
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_3__/* .useUserData */ .v)();
    const { setFormValues  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_6__/* .useAuth */ .a)();
    const endDate = props?.giveaway?.endDate;
    const endDateTime = new Date(endDate).getTime(); // convert end date to milliseconds
    const nowDateTime = Date.now(); // get current date time in milliseconds
    const diffTime = endDateTime - nowDateTime;
    const prizes = props?.giveaway?.prize;
    // get PrizeValue by adding all prizes.[index].value
    let giftPrize = 0;
    const prizeValue = prizes?.forEach((item)=>{
        giftPrize += parseInt(item.value);
    });
    const firstPrize = prizes?.[0];
    const [image, setImage] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)();
    const [company, setCompany] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)();
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        if (firstPrize?.image) {
            setImage(firstPrize?.image);
        } else {
            setImage(undefined);
        }
    }, [
        firstPrize?.image
    ]);
    function goToPreview() {
        next_router__WEBPACK_IMPORTED_MODULE_5___default().push("/company/giveaway/" + props?.giveaway?.uid);
        setFormValues(props?.giveaway);
    }
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        if (props?.giveaway?.user_uid && !userData?.company_name) {
            const fetchUserData = async ()=>{
                const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_7__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_7__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_8__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_7__.where)("uid", "==", props?.giveaway?.user_uid));
                const ref = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_7__.getDocs)(qry);
                ref.forEach((doc)=>{
                    const data = doc.data();
                    setCompany(data);
                });
            };
            fetchUserData();
        }
    }, [
        props?.giveaway?.user_uid
    ]);
    const editGiveaway = ()=>{
        // route to giveaway/id and pass the props.props sa object
        next_router__WEBPACK_IMPORTED_MODULE_5___default().push("/company/giveaway/edit/" + id);
    };
    // go to company profile
    const goToCompanyProfile = ()=>{
        next_router__WEBPACK_IMPORTED_MODULE_5___default().push(`/company/profile/${props?.giveaway?.user_uid}`);
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("article", {
        className: `flex flex-col rounded-lg items-center flex-shrink-0  align-center w-full md:max-w-[234px] mx-auto snap-center hover:opacity-100 opacity-90 transition-opacity duation-200 overflow-hidden md:space-x-0 ${props.index === 0 && "customer-step5"}`,
        children: [
            image ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_1___default()), {
                onClick: ()=>{
                    goToPreview();
                },
                className: "group-hover:scale-105 transition duration-300 ease-in-out w-full h-auto object-cover cursor-pointer",
                src: image,
                alt: "Giveaway image",
                height: 206,
                width: 206
            }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_1___default()), {
                onClick: ()=>{
                    goToPreview();
                },
                className: "group-hover:scale-105 transition duration-300 ease-in-out w-full max-w-[100px] md:max-w-[206px] h-auto object-cover cursor-pointer",
                src: "/assets/images/giveaway_img.png",
                alt: "Giveaway image",
                width: "206",
                height: "206"
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "w-full bg-[#1E2223] rounded-b-lg",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "w-full px-[10px] py-[12px] flex items-center",
                        children: [
                            company?.imgUrl ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    onClick: goToCompanyProfile,
                                    className: "group-hover:scale-105 transition duration-300 ease-in-out rounded-full cursor-pointer",
                                    src: company?.imgUrl,
                                    alt: "Company logo",
                                    width: "45",
                                    height: "45"
                                })
                            }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_1___default()), {
                                onClick: goToCompanyProfile,
                                className: "group-hover:scale-105 transition duration-300 ease-in-out rounded-full cursor-pointer",
                                src: "/assets/images/user_profile.png",
                                alt: "Company logo",
                                width: "45",
                                height: "45"
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "flex flex-col space-y-2 min-w-[60px] w-full",
                                children: [
                                    status === "Live" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-row items-center justify-between w-full",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                onClick: goToCompanyProfile,
                                                className: "flex flex-row items-center text-xs text-white rounded-sm border-jade-100/40 truncate cursor-pointer hover:text-teal-300 ml-2",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__.CheckBadgeIcon, {
                                                        className: "w-5 h-5 mr-1 text-teal-300"
                                                    }),
                                                    " ",
                                                    company?.name || company?.company_name || "Company"
                                                ]
                                            }),
                                            [
                                                status
                                            ].map((label, index)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "flex items-center px-2 py-1 bg-teal-700 text-live text-xs font-bold rounded-full max-w-[60px]",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: "w-1.5 h-1.5 bg-live rounded-full animate-pulse mr-2 font-Ubuntu-Regular text-xs"
                                                        }),
                                                        label
                                                    ]
                                                }, index))
                                        ]
                                    }),
                                    status === "Ended" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-row items-center justify-between w-full",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                onClick: goToCompanyProfile,
                                                className: "flex flex-row items-center text-xs text-white rounded-sm border-jade-100/40 truncate cursor-pointer hover:text-teal-300 ml-2",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__.CheckBadgeIcon, {
                                                        className: "w-5 h-5 mr-1 text-teal-300"
                                                    }),
                                                    " ",
                                                    company?.name || company?.company_name || "Company"
                                                ]
                                            }),
                                            [
                                                status
                                            ].map((label, index)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "flex items-center px-2 py-1 bg-teal-700 text-[red] text-xs font-bold rounded-full max-w-[60px]",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: "w-1.5 h-1.5 bg-[red] rounded-full animate-pulse mr-1 font-Ubuntu-Regular text-xs"
                                                        }),
                                                        label
                                                    ]
                                                }, index))
                                        ]
                                    }),
                                    status === "Upcoming" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex flex-row items-center justify-between w-full",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                onClick: goToCompanyProfile,
                                                className: "flex flex-row items-center text-xs text-white rounded-sm border-jade-100/40 truncate cursor-pointer hover:text-teal-300 ml-2",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__.CheckBadgeIcon, {
                                                        className: "w-5 h-5 mr-1 text-teal-300"
                                                    }),
                                                    " ",
                                                    company?.name || company?.company_name || "Company"
                                                ]
                                            }),
                                            [
                                                status
                                            ].map((label, index)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "flex items-center px-2 py-2 bg-teal-700 text-[yellow] text-xs font-bold rounded-full max-w-[75px]",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: "w-1.5 h-1.5 bg-[yellow] rounded-full animate-pulse mr-1 font-Ubuntu-Regular text-xs"
                                                        }),
                                                        label
                                                    ]
                                                }, index))
                                        ]
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "w-full px-[10px] pb-[15px]",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                onClick: ()=>{
                                    goToPreview();
                                },
                                className: "w-full text-sm font-Ubuntu-Medium text-white md:mb-[10px] truncate cursor-pointer hover:text-teal-300",
                                children: props?.giveaway?.title
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                className: "py-2 text-xs text-white",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countdown__WEBPACK_IMPORTED_MODULE_10___default()), {
                                    date: Date.now() + diffTime,
                                    renderer: renderer
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "flex justify-between w-full",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                className: "flex-1 text-sm text-center font-Ubuntu-Bold text-white bg-teal-800 p-[10px] rounded-bl-lg",
                                children: [
                                    "$",
                                    giftPrize
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                onClick: ()=>{
                                    goToPreview();
                                },
                                className: "flex items-center text-xs text-white uppercase transition duration-300 ease-in-out group-hover:text-yellow py-[10px] px-[20px] bg-teal-600 rounded-br-lg cursor-pointer",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "text-xs font-Ubuntu-Regular uppercase",
                                        children: "Details"
                                    }),
                                    " ",
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_9__.ChevronRightIcon, {
                                        className: "text-white",
                                        width: "12",
                                        height: "12"
                                    })
                                ]
                            })
                        ]
                    })
                ]
            }),
            userData?.userType === "company" && props?.giveaway?.user_uid === userData?.uid && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                onClick: editGiveaway,
                className: "rounded-sm border border-fbyellow flex items-center text-xs text-white transition duration-300 ease-in-out hover:bg-fbyellow hover:text-black px-3 py-1 mt-5",
                children: "Edit Event"
            })
        ]
    });
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardGiveaway);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;