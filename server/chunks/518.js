"use strict";
exports.id = 518;
exports.ids = [518];
exports.modules = {

/***/ 3267:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/facebook_icon.718e8735.svg","height":36,"width":36});

/***/ }),

/***/ 4341:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/google_icon.8c02ece4.svg","height":36,"width":36});

/***/ }),

/***/ 8732:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"src":"/_next/static/media/metamask_icon.ba7d5079.svg","height":36,"width":36});

/***/ }),

/***/ 5545:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ AuthModal)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(291);
/* harmony import */ var _Login__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7753);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8762);
/* harmony import */ var _SignUp__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(882);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_headlessui_react__WEBPACK_IMPORTED_MODULE_2__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_3__, _Login__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _SignUp__WEBPACK_IMPORTED_MODULE_6__]);
([_headlessui_react__WEBPACK_IMPORTED_MODULE_2__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_3__, _Login__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _SignUp__WEBPACK_IMPORTED_MODULE_6__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);







function AuthModal({ setOpen , open  }) {
    // const [open, setOpen] = useState(true)
    const { authModal: { whichAuth  }  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_5__/* .useAuth */ .a)();
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Transition.Root, {
        show: open,
        as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Dialog, {
            as: "div",
            className: "fixed z-50 inset-0 overflow-y-auto",
            onClose: setOpen,
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Transition.Child, {
                        as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
                        enter: "ease-out duration-300",
                        enterFrom: "opacity-0",
                        enterTo: "opacity-100",
                        leave: "ease-in duration-200",
                        leaveFrom: "opacity-100",
                        leaveTo: "opacity-0",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Dialog.Overlay, {
                            className: "fixed inset-0 bg-black/70 backdrop-blur-sm transition-opacity"
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "hidden sm:inline-block sm:align-middle sm:h-screen",
                        "aria-hidden": "true",
                        children: "​"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Transition.Child, {
                        as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
                        enter: "ease-out duration-300",
                        enterFrom: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95",
                        enterTo: "opacity-100 translate-y-0 sm:scale-100",
                        leave: "ease-in duration-200",
                        leaveFrom: "opacity-100 translate-y-0 sm:scale-100",
                        leaveTo: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "relative inline-block align-bottom gradientBody rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "hidden sm:block absolute top-0 right-0 pt-4 pr-4",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                        type: "button",
                                        className: "focus:outline-none focus:ring-0",
                                        onClick: ()=>setOpen(false),
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                className: "sr-only",
                                                children: "Close"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_3__.XMarkIcon, {
                                                className: "cursor-pointer text-teal-600 w-8 h-8"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "sm:flex sm:items-start",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "mt-2",
                                            children: whichAuth === "sign-in" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Login__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SignUp__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {})
                                        })
                                    })
                                })
                            ]
                        })
                    })
                ]
            })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7753:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Login)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5641);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _public_assets_images_google_icon_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4341);
/* harmony import */ var _public_assets_images_facebook_icon_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3267);
/* harmony import */ var _public_assets_images_metamask_icon_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8732);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8762);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6201);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1083);
/* harmony import */ var _components_loader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3628);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(1492);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(236);
/* harmony import */ var wagmi__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(8998);
/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(1649);
/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(next_auth_react__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _moralisweb3_next__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(4146);
/* harmony import */ var _moralisweb3_next__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_moralisweb3_next__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var wagmi_connectors_metaMask__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(5350);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hot_toast__WEBPACK_IMPORTED_MODULE_9__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_10__, firebase_firestore__WEBPACK_IMPORTED_MODULE_12__, _firebase__WEBPACK_IMPORTED_MODULE_13__, wagmi__WEBPACK_IMPORTED_MODULE_14__, wagmi_connectors_metaMask__WEBPACK_IMPORTED_MODULE_17__]);
([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_8__, react_hot_toast__WEBPACK_IMPORTED_MODULE_9__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_10__, firebase_firestore__WEBPACK_IMPORTED_MODULE_12__, _firebase__WEBPACK_IMPORTED_MODULE_13__, wagmi__WEBPACK_IMPORTED_MODULE_14__, wagmi_connectors_metaMask__WEBPACK_IMPORTED_MODULE_17__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);


















function Login() {
    const { connectAsync  } = (0,wagmi__WEBPACK_IMPORTED_MODULE_14__.useConnect)();
    const { disconnectAsync  } = (0,wagmi__WEBPACK_IMPORTED_MODULE_14__.useDisconnect)();
    const { isConnected  } = (0,wagmi__WEBPACK_IMPORTED_MODULE_14__.useAccount)();
    const { signMessageAsync  } = (0,wagmi__WEBPACK_IMPORTED_MODULE_14__.useSignMessage)();
    const { requestChallengeAsync  } = (0,_moralisweb3_next__WEBPACK_IMPORTED_MODULE_16__.useAuthRequestChallengeEvm)();
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_7__.useRouter)();
    const { user  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_8__/* .useAuth */ .a)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_10__/* .useUserData */ .v)();
    // // redirect if userData or user updated
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (user?.uid !== null && userData && userData.userType === "user") {
            router.push("/users/dashboard");
        } else if (user?.uid !== null && userData && userData.userType === "company") {
            router.push("/company/dashboard");
        }
    }, [
        userData
    ]);
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_2__.useForm)({
        mode: "onBlur"
    });
    const { signInWithGoogle , signInWithFacebook , logIn , authModal: { whichAuth , setWhichAuth  }  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_8__/* .useAuth */ .a)();
    const { register , handleSubmit , formState: { errors  }  } = methods;
    // check if user is authenticated and its userType for redirection
    const onSubmit = async (data)=>{
        setLoading(true);
        try {
            const success = await logIn(data.email, data.password);
            if (success) {
                if (userData?.userType === "user") {
                    setLoading(false);
                    router.push("/users/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Logged In successfully");
                } else if (userData?.userType === "company") {
                    setLoading(false);
                    router.push("/company/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Logged In successfully");
                }
            }
        } catch (error) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].error(error.message);
        }
    };
    const signInGoogle = async ()=>{
        try {
            const success = await signInWithGoogle();
            if (success) {
                const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_13__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.where)("uid", "==", success.user?.uid));
                const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.getDocs)(qry);
                if (querySnapshot.empty) {
                    const doc = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.addDoc)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_13__.db, "users"), {
                        email: success.user?.email,
                        name: success.user?.displayName,
                        userType: "user",
                        uid: success.user?.uid,
                        isNew: true
                    });
                    console.log("Document written with ID: ", doc.id);
                    setLoading(false);
                    router.push("/users/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Account created successfully");
                } else {
                    setLoading(false);
                    router.push("/users/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Logged In successfully");
                }
            }
        } catch (error) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].error(error);
        }
    };
    const signInFacebook = async ()=>{
        try {
            const success = await signInWithFacebook();
            if (success) {
                const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_13__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.where)("uid", "==", success.user?.uid));
                const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.getDocs)(qry);
                if (querySnapshot.empty) {
                    const doc = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.addDoc)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_12__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_13__.db, "users"), {
                        email: success.user?.email,
                        name: success.user?.displayName,
                        userType: "user",
                        uid: success.user?.uid,
                        isNew: true
                    });
                    console.log("Document written with ID: ", doc.id);
                    setLoading(false);
                    router.push("/users/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Account created successfully");
                } else {
                    setLoading(false);
                    router.push("/users/dashboard");
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Logged In successfully");
                }
            }
        } catch (error) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].error(error);
        }
    };
    const signInMetamask = async ()=>{
        try {
            if (isConnected) {
                await disconnectAsync();
            }
            const { account , chain  } = await connectAsync({
                connector: new wagmi_connectors_metaMask__WEBPACK_IMPORTED_MODULE_17__.MetaMaskConnector()
            });
            const { message  } = await requestChallengeAsync({
                address: account,
                chainId: chain.id
            });
            const signature = await signMessageAsync({
                message
            });
            console.log(signature);
            // redirect user after success authentication to '/users/dashboard' page
            const { url  } = await (0,next_auth_react__WEBPACK_IMPORTED_MODULE_15__.signIn)("moralis-auth", {
                message,
                signature,
                redirect: false,
                callbackUrl: "/users/dashboard"
            });
            router.push("/users/dashboard");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].success("Logged In successfully");
        } catch (error) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_9__["default"].error(error);
        }
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "p-6",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_loader__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                    show: loading
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "w-full max-w-[450px] mx-auto",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "flex-col items-center text-center space-y-5 p-5",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "text-xl font-medium",
                                children: "Sign In"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_hook_form__WEBPACK_IMPORTED_MODULE_2__.FormProvider, {
                            ...methods,
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                                className: "flex flex-col",
                                onSubmit: handleSubmit(onSubmit),
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "space-y-5 mb-5",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                className: "w-full h-[45px] bg-transparent border border-jade-100 rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                placeholder: "Email",
                                                type: "email",
                                                ...register("email", {
                                                    required: "Email is required"
                                                })
                                            }),
                                            errors.email && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-red-400",
                                                children: errors.email.message
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                className: "w-full h-[45px] bg-transparent border border-jade-100 rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                placeholder: "Password",
                                                type: "password",
                                                ...register("password", {
                                                    required: "Password is required"
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                        type: "submit",
                                        className: "buttonPrimary",
                                        children: "Sign In"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: "mt-2 text-center text-[12px]",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: "text-fbyellow/60 hover:text-fbyellow",
                                            onClick: ()=>setWhichAuth(""),
                                            children: "Forgot password?"
                                        })
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                        className: "mt-5 text-center",
                                        children: [
                                            "Don't have an account?",
                                            " ",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                className: "text-fbyellow",
                                                onClick: ()=>setWhichAuth(""),
                                                children: "Get started."
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "my-7 text-base relative after:content-[''] after:absolute after:bg-teal-100 after:w-full after:h-[1px] after:opacity-20 after:top-1/2 after:left-1/2 after:-translate-x-1/2 after:translate-y-1/2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "block relative w-25 m-auto text-center",
                                            children: "or continue with"
                                        })
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "flex items-center space-x-5",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "socialSignup",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                                                    className: "relative z-10",
                                                    src: _public_assets_images_google_icon_svg__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
                                                    alt: "google_icon",
                                                    layout: "intrinsic",
                                                    onClick: signInGoogle
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "socialSignup",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                                                    className: "relative z-10",
                                                    src: _public_assets_images_facebook_icon_svg__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z,
                                                    alt: "facebook_icon",
                                                    layout: "intrinsic",
                                                    onClick: signInFacebook
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "socialSignup",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                                                    className: "relative z-10",
                                                    src: _public_assets_images_metamask_icon_svg__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z,
                                                    alt: "metamask_icon",
                                                    layout: "intrinsic",
                                                    onClick: signInMetamask
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 882:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ SingUp)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5641);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _public_assets_images_google_icon_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4341);
/* harmony import */ var _public_assets_images_facebook_icon_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3267);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8762);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(401);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(236);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1492);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1083);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(6201);
/* harmony import */ var _components_loader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3628);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_7__, firebase_auth__WEBPACK_IMPORTED_MODULE_8__, _firebase__WEBPACK_IMPORTED_MODULE_9__, firebase_firestore__WEBPACK_IMPORTED_MODULE_10__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_11__, react_hot_toast__WEBPACK_IMPORTED_MODULE_12__]);
([react_hook_form__WEBPACK_IMPORTED_MODULE_2__, _context_authcontext__WEBPACK_IMPORTED_MODULE_7__, firebase_auth__WEBPACK_IMPORTED_MODULE_8__, _firebase__WEBPACK_IMPORTED_MODULE_9__, firebase_firestore__WEBPACK_IMPORTED_MODULE_10__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_11__, react_hot_toast__WEBPACK_IMPORTED_MODULE_12__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);














function SingUp() {
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_6__.useRouter)();
    const { signUp  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_7__/* .useAuth */ .a)();
    const { user , authModal: { whichAuth , setWhichAuth  }  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_7__/* .useAuth */ .a)();
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_11__/* .useUserData */ .v)();
    const methods = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_2__.useForm)({
        mode: "onBlur"
    });
    const { register , handleSubmit , formState: { errors  } , watch , setValue  } = methods;
    // redirect if userData or user updated
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (user?.uid !== null && userData && userData.userType === "user") {
            router.push("/users/dashboard");
        } else if (user?.uid !== null && userData && userData.userType === "company") {
            router.push("/company/dashboard");
        }
    }, [
        userData
    ]);
    react__WEBPACK_IMPORTED_MODULE_1___default().useEffect(()=>{
        const subscription = watch((value, { name , type  })=>console.log());
        return ()=>subscription.unsubscribe();
    }, [
        watch
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setTimeout(()=>{
            setValue("userType", "user");
        }, 1);
    }, [
        setValue
    ]);
    const { signInWithGoogle , signInWithFacebook  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_7__/* .useAuth */ .a)();
    const onSubmit = async (data)=>{
        setLoading(true);
        try {
            const success = await signUp(data.email, data.password);
            console.log(success.user);
            (0,firebase_auth__WEBPACK_IMPORTED_MODULE_8__.updateProfile)(success.user, {
                displayName: data.companyName || data.name
            }).then(()=>{
                console.log("Profile updated");
            }).catch((error)=>{
                setLoading(false);
                react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(error);
            });
            // add this user to firestore db collection name as user
            try {
                const docRef = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.addDoc)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_9__.db, "users"), {
                    email: data.email,
                    name: data.companyName || data.name,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    userType: data.userType,
                    uid: success.user?.uid,
                    isNew: true
                });
            } catch (e) {
                setLoading(false);
                react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(e);
            }
            setLoading(false);
            if (data.userType === "company") router.push("/company/dashboard");
            else router.push("/users/dashboard");
            react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.success("Account created successfully");
        } catch (error) {
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(error.message);
        }
    };
    const signInGoogle = async ()=>{
        setLoading(true);
        try {
            const success = await signInWithGoogle();
            if (success) {
                try {
                    const docRef = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.addDoc)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_9__.db, "users"), {
                        email: success.user?.email,
                        name: success.user?.displayName,
                        userType: "user",
                        uid: success.user?.uid,
                        isNew: true
                    });
                } catch (e) {
                    setLoading(false);
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(e);
                }
                router.push("/users/dashboard");
                react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.success("Account created successfully");
            }
        } catch (error) {
            console.log(error);
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(error);
        }
    };
    const signInFacebook = async ()=>{
        setLoading(true);
        try {
            const success = await signInWithFacebook();
            if (success) {
                try {
                    const docRef = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.addDoc)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_9__.db, "users"), {
                        email: success.user?.email,
                        name: success.user?.displayName,
                        userType: "user",
                        uid: success.user?.uid,
                        isNew: true
                    });
                } catch (e) {
                    setLoading(false);
                    react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(e);
                }
                router.push("/users/dashboard");
                react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.success("Account created successfully");
            }
        } catch (error) {
            console.log(error);
            setLoading(false);
            react_hot_toast__WEBPACK_IMPORTED_MODULE_12__.toast.error(error);
        }
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "p-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_loader__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                    show: loading
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "w-full max-w-[450px] mx-auto",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "flex-col items-center text-center space-y-5 p-5",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "text-xl font-medium",
                                children: "Get Started"
                            })
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_hook_form__WEBPACK_IMPORTED_MODULE_2__.FormProvider, {
                            ...methods,
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                                    className: "flex flex-col",
                                    onSubmit: handleSubmit(onSubmit),
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "space-y-3 mb-4",
                                            children: [
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "User Type",
                                                    ...register("userType", {
                                                        required: "User Type is Required"
                                                    }),
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                            className: "text-black ",
                                                            value: "user",
                                                            children: "Customer"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                            className: "text-black ",
                                                            value: "company",
                                                            children: "Business"
                                                        })
                                                    ]
                                                }),
                                                errors.userType && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.userType.message
                                                }),
                                                watch("userType") === "company" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "Company User Name",
                                                    type: "text",
                                                    ...register("companyName", {
                                                        required: "Company User Name is required"
                                                    })
                                                }),
                                                watch("userType") === "company" && errors.companyName && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.companyName.message
                                                }),
                                                watch("userType") === "user" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "Username",
                                                    type: "text",
                                                    ...register("name", {
                                                        required: "Username is required"
                                                    })
                                                }),
                                                watch("userType") === "user" && errors.name && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.name.message
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "First Name",
                                                    type: "text",
                                                    ...register("firstName", {
                                                        required: "First Name is required"
                                                    })
                                                }),
                                                errors.firstName && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.firstName.message
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "Last Name",
                                                    type: "text",
                                                    ...register("lastName", {
                                                        required: "First Name is required"
                                                    })
                                                }),
                                                errors.lastName && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.lastName.message
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "Email",
                                                    type: "email",
                                                    ...register("email", {
                                                        required: "Email is required"
                                                    })
                                                }),
                                                errors.email && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.email.message
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                    className: "w-full h-[45px] bg-[#257d860d] border border-[#139bad33] rounded-sm px-5 py-2.5 text-white placeholder:text-lightWhite focus-visible:outline-0",
                                                    placeholder: "Create password",
                                                    type: "password",
                                                    ...register("password", {
                                                        required: "Password is required"
                                                    })
                                                }),
                                                errors.password && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-red",
                                                    children: errors.password.message
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: "buttonPrimary font-semibold",
                                            children: "Create Account"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                    className: "mt-5 text-center",
                                    children: [
                                        "Already have an account?",
                                        " ",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: "text-[#F6B519]",
                                            onClick: ()=>setWhichAuth("sign-in"),
                                            children: "Sign in instead"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "my-7 text-base relative after:content-[''] after:absolute after:bg-teal-100 after:w-full after:h-[1px] after:opacity-20 after:top-1/2 after:left-1/2 after:-translate-x-1/2 after:translate-y-1/2",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "block m-auto text-center",
                                        children: "or continue with"
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "flex items-center space-x-5",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "socialSignup",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                                                className: "relative z-10",
                                                src: _public_assets_images_google_icon_svg__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
                                                alt: "google_icon",
                                                layout: "intrinsic",
                                                onClick: signInGoogle
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "socialSignup",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                                                className: "relative z-10",
                                                src: _public_assets_images_facebook_icon_svg__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z,
                                                alt: "facebook_icon",
                                                layout: "intrinsic",
                                                onClick: signInFacebook
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 6826:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ HeaderBizUtility)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var firebase_messaging__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3512);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9755);
/* harmony import */ var next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_legacy_image__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1664);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1083);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8762);
/* harmony import */ var _AuthPanel_AuthModal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5545);
/* harmony import */ var _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(291);
/* harmony import */ var lottie_react__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6164);
/* harmony import */ var lottie_react__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lottie_react__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _public_assets_anim_logo_json__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(9269);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_headlessui_react__WEBPACK_IMPORTED_MODULE_2__, firebase_messaging__WEBPACK_IMPORTED_MODULE_3__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, _context_authcontext__WEBPACK_IMPORTED_MODULE_8__, _AuthPanel_AuthModal__WEBPACK_IMPORTED_MODULE_9__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__]);
([_headlessui_react__WEBPACK_IMPORTED_MODULE_2__, firebase_messaging__WEBPACK_IMPORTED_MODULE_3__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_6__, _context_authcontext__WEBPACK_IMPORTED_MODULE_8__, _AuthPanel_AuthModal__WEBPACK_IMPORTED_MODULE_9__, _heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);














function HeaderBizUtility(props) {
    // get userTYpe from userData
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_6__/* .useUserData */ .v)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const [open, setOpen] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { user , notification , setNotification , logOut , authModal: { whichAuth , setWhichAuth  }  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_8__/* .useAuth */ .a)();
    const [count, setCount] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    const userParse = localStorage.getItem("user") || "";
    let users;
    if (userParse) {
        users = JSON.parse(userParse);
    }
    // set loading to true if there is no userData
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setLoading(userData === null);
    }, [
        userData
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        const onMessageListener = ()=>{
            const messaging = (0,firebase_messaging__WEBPACK_IMPORTED_MODULE_3__.getMessaging)();
            return new Promise((resolve)=>{
                return (0,firebase_messaging__WEBPACK_IMPORTED_MODULE_3__.onMessage)(messaging, (payload)=>{
                    resolve(payload);
                });
            });
        };
        onMessageListener().then((payload)=>{
            console.log(payload);
            setNotification((prev)=>[
                    ...prev,
                    {
                        title: payload.notification.title,
                        body: payload.notification.body,
                        link: payload.fcmOptions.link
                    }
                ]);
            setCount((prev)=>prev ? prev + 1 : 1);
        }).catch((err)=>{
            console.log(err);
        });
    });
    const handleAuth = (value)=>{
        setOpen(true);
        setWhichAuth(value);
    };
    // go to profile
    const goToCompanyProfile = ()=>{
        userData && userData.userType === "company" ? next_router__WEBPACK_IMPORTED_MODULE_7___default().push(`/company/profile/`) : next_router__WEBPACK_IMPORTED_MODULE_7___default().push(`/users/profile`);
    };
    // go to dashboard
    const goToDashboard = ()=>{
        userData && userData.userType === "company" ? next_router__WEBPACK_IMPORTED_MODULE_7___default().push("/company/dashboard") : next_router__WEBPACK_IMPORTED_MODULE_7___default().push("/users/dashboard");
    };
    const handleLogout = ()=>{
        logOut();
        next_router__WEBPACK_IMPORTED_MODULE_7___default().push("/");
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex justify-end items-center w-full sticky top-0 z-50 px-5 md:px-6 py-3 bg-gradient-to-r from-fbblack-200 to-fbblack-100",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "text-left mx-auto w-[100px] lg:hidden ml-2",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_5___default()), {
                            href: "/",
                            className: "block",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((lottie_react__WEBPACK_IMPORTED_MODULE_11___default()), {
                                animationData: _public_assets_anim_logo_json__WEBPACK_IMPORTED_MODULE_12__,
                                loop: false
                            })
                        })
                    }),
                    userData || users ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "relative",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(NotificationPanel, {
                                        setCount: setCount,
                                        notification: notification
                                    }),
                                    count ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "absolute top-0 right-0 w-4 h-4 bg-red-500 rounded-full flex items-center justify-center text-white text-xs font-medium",
                                        children: count
                                    }) : null
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover, {
                                className: "relative",
                                children: ({ open  })=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover.Button, {
                                                className: `
                    ${open ? "" : "text-opacity-90"}
                    flex relative items-center space-x-3 max-w whitespace-nowrap px-3 py-2 rounded-full bg-teal-700/40 hover:bg-teal-700/80 focus:outline-none focus-visible:ring-white`,
                                                children: [
                                                    userData?.imgUrl || users?.imgUrl ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "w-[45px] h-[45px]",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default()), {
                                                            onClick: goToCompanyProfile,
                                                            className: "group-hover:scale-105 transition duration-300 ease-in-out rounded-full",
                                                            src: userData?.imgUrl || users?.imgUrl,
                                                            alt: "Company logo",
                                                            width: "45",
                                                            height: "45"
                                                        })
                                                    }) : userData?.imgUrl || users?.imgUrl ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default()), {
                                                        className: "group-hover:scale-105 transition duration-300 ease-in-out rounded-full",
                                                        src: userData?.imgUrl || users?.imgUrl,
                                                        alt: "User image",
                                                        width: "45",
                                                        height: "45"
                                                    }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_legacy_image__WEBPACK_IMPORTED_MODULE_4___default()), {
                                                        onClick: goToCompanyProfile,
                                                        className: "group-hover:scale-105 transition duration-300 ease-in-out rounded-full",
                                                        src: "/assets/images/user_profile.png",
                                                        alt: "Company logo",
                                                        width: "45",
                                                        height: "45"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "text-white truncate",
                                                        children: userData?.name || userData?.name || users.name
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.ChevronDownIcon, {
                                                        className: "w-3 h-3"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Transition, {
                                                as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
                                                enter: "transition ease-out duration-200",
                                                enterFrom: "opacity-0 translate-y-1",
                                                enterTo: "opacity-100 translate-y-0",
                                                leave: "transition ease-in duration-150",
                                                leaveFrom: "opacity-100 translate-y-0",
                                                leaveTo: "opacity-0 translate-y-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover.Panel, {
                                                    className: "bg-teal-700/90 backdrop-blur-sm rounded-sm absolute right-[0%] z-10 mt-3 w-fit sm:px-0 min-w-[222px]",
                                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "px-2 py-5 space-y-3",
                                                        children: [
                                                            userData && userData.userType === "company" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_5___default()), {
                                                                href: "/company/giveaway/",
                                                                className: "popoverLink",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.PlusCircleIcon, {
                                                                        className: "w-4 h-4 business-create"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Create Campaign"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                                className: "popoverLink w-full",
                                                                onClick: goToDashboard,
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.RectangleGroupIcon, {
                                                                        className: "w-4 h-4 business-create"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Dashboard"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                                className: "popoverLink w-full",
                                                                onClick: goToCompanyProfile,
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.UserIcon, {
                                                                        className: "w-4 h-4"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Your Profile"
                                                                    })
                                                                ]
                                                            }),
                                                            userData && userData.userType !== "company" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_5___default()), {
                                                                href: "/users/dashboard",
                                                                className: "popoverLink",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.FireIcon, {
                                                                        className: "w-4 h-4"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Favorites"
                                                                    })
                                                                ]
                                                            }),
                                                            userData && userData.userType === "company" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_5___default()), {
                                                                href: "/company/analytics",
                                                                className: "popoverLink",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.ChartPieIcon, {
                                                                        className: "w-4 h-4"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Analytics"
                                                                    })
                                                                ]
                                                            }),
                                                            userData && userData.userType === "company" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_5___default()), {
                                                                href: "/company/followers ",
                                                                className: "popoverLink",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.ChartPieIcon, {
                                                                        className: "w-4 h-4"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Followers"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                                className: "popoverLink w-full",
                                                                onClick: handleLogout,
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.ArrowRightOnRectangleIcon, {
                                                                        className: "w-4 h-4"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: "",
                                                                        children: "Sign Out"
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                })
                                            })
                                        ]
                                    })
                            })
                        ]
                    }) : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "md:inline-flex items-center space-x-5 px-2 py-2 md:px-0 md:py-0",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                className: "buttonTertiary my-0 hover:text-yellow",
                                onClick: ()=>handleAuth("sign-in"),
                                children: "Sign In"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                className: "buttonTertiary my-0 hover:text-yellow",
                                onClick: ()=>handleAuth(""),
                                children: "Sign Up"
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_AuthPanel_AuthModal__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                setOpen: setOpen,
                open: open
            })
        ]
    });
}
function NotificationPanel({ notification , setCount  }) {
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: ()=>{
            setCount(null);
        },
        className: "max-w-sm px-4",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover, {
            className: "relative",
            children: ({ open  })=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover.Button, {
                            className: `
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-md px-3 py-2 text-base font-medium text-white hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75`,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_solid__WEBPACK_IMPORTED_MODULE_10__.BellAlertIcon, {
                                className: "UtilityIcon"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Transition, {
                            as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
                            enter: "transition ease-out duration-200",
                            enterFrom: "opacity-0 translate-y-1",
                            enterTo: "opacity-100 translate-y-0",
                            leave: "transition ease-in duration-150",
                            leaveFrom: "opacity-100 translate-y-0",
                            leaveTo: "opacity-0 translate-y-1",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_headlessui_react__WEBPACK_IMPORTED_MODULE_2__.Popover.Panel, {
                                className: "absolute -left-1/2 z-10 mt-3 w-[250px] -translate-x-1/2 transform sm:px-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "overflow-hidden rounded-sm shadow-lg ring-1 ring-black ring-opacity-5",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "relative group flex flex-col bg-teal-600/60 backdrop-blur-sm py-3 group text-white",
                                        children: notification.length > 0 ? notification.map((item, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                href: item.link,
                                                className: "-m-3 flex p-6 items-center rounded-lg transition duration-150 ease-in-out group-hover:text-white hover:bg-[#162126] focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "ml-4",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            className: "text-sm font-medium",
                                                            children: item.title
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            className: "text-sm",
                                                            children: item.body
                                                        })
                                                    ]
                                                })
                                            }, index)) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "-m-3 flex p-6 items-center rounded-sm transition duration-150 ease-in-out",
                                            children: "No notifications"
                                        })
                                    })
                                })
                            })
                        })
                    ]
                })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 3518:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ MainLayout)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1492);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3628);
/* harmony import */ var _SideBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(8005);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1083);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(236);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7495);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_reactour_tour__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2505);
/* harmony import */ var _NavbarMobile__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2962);
/* harmony import */ var _HeaderBizUtility__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6826);
/* harmony import */ var _Marketing_Layouts_Footer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(8962);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, _SideBar__WEBPACK_IMPORTED_MODULE_4__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__, _firebase__WEBPACK_IMPORTED_MODULE_6__, _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_8__, _NavbarMobile__WEBPACK_IMPORTED_MODULE_9__, _HeaderBizUtility__WEBPACK_IMPORTED_MODULE_10__]);
([firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, _SideBar__WEBPACK_IMPORTED_MODULE_4__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_5__, _firebase__WEBPACK_IMPORTED_MODULE_6__, _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_8__, _NavbarMobile__WEBPACK_IMPORTED_MODULE_9__, _HeaderBizUtility__WEBPACK_IMPORTED_MODULE_10__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);
// Main Layout












function MainLayout({ children  }) {
    const { userData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_5__/* .useUserData */ .v)();
    const { setSteps , setIsOpen  } = (0,_reactour_tour__WEBPACK_IMPORTED_MODULE_7__.useTour)();
    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(false);
    const [giveaways, setGiveaways] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)([]);
    const [featuredCompanies, setFeaturesCompanies] = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)([]);
    // add a loader until it gets the userData
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        setLoading(false);
    }, [
        userData
    ]);
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        userData && setLoading(true);
        userData && getGiveaways();
        userData && getCompaniesData();
    }, []);
    const launchTutorial = ()=>{
        if (userData && userData?.isNew === true) {
            if (giveaways && giveaways.length > 0) {
                setSteps?.(_Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_8__/* .customerTutorialWithGiveaway */ .$L);
            } else {
                setSteps?.(_Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_8__/* .customerTutorialWithoutGiveaway */ .jg);
            }
            setIsOpen(true);
        }
    };
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        userData && launchTutorial();
    }, [
        userData
    ]);
    const getGiveaways = async ()=>{
        const q = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_6__.db, "giveaway"));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.getDocs)(q);
        const temp = querySnapshot.docs.map((doc)=>doc.data());
        const liveEvents = temp.filter((item)=>{
            const endDate = item?.endDate;
            const startDate = item?.startDate;
            const today = new Date();
            const date = new Date(endDate);
            const date2 = new Date(startDate);
            const diff = date.getTime() - today.getTime();
            const diff2 = today.getTime() - date2.getTime();
            return item && diff > 0 && diff2 > 0;
        });
        // filter out the ones that not drafts , where drafts == false
        const notDrafts = liveEvents.filter((item)=>{
            return item.draft == false;
        });
        setLoading(false);
        setGiveaways(notDrafts);
    };
    const getCompaniesData = async ()=>{
        const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_6__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.where)("userType", "==", "company"));
        const ref = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.getDocs)(qry);
        const temp = ref.docs.map((doc)=>doc.data());
        setFeaturesCompanies(temp);
        setLoading(false);
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "dashboardContainerWrapper background_lightBlack",
        children: [
            loading && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_loader__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                show: loading
            }),
            !loading && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SideBar__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "dashboardContainer",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_HeaderBizUtility__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {}),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "mainContainer",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("main", {
                                        className: "mainContent",
                                        children: children
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Marketing_Layouts_Footer__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {})
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_NavbarMobile__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                                UserType: "company"
                            })
                        ]
                    })
                ]
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 8962:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Layouts_Footer)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: external "react-spring"
var external_react_spring_ = __webpack_require__(4417);
// EXTERNAL MODULE: external "lottie-react"
var external_lottie_react_ = __webpack_require__(6164);
var external_lottie_react_default = /*#__PURE__*/__webpack_require__.n(external_lottie_react_);
;// CONCATENATED MODULE: ./public/Marketing/imgs/logo.json
const logo_namespaceObject = JSON.parse('{"nm":"Comp 1","mn":"","layers":[{"ty":0,"nm":"4K","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1920,725,0],"ix":1},"s":{"a":0,"k":[50,50,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[960,362.5,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"w":3840,"h":1450,"refId":"comp_0","ind":0}],"ddd":1,"h":725,"w":1920,"meta":{"a":"","k":"","d":"","g":"LottieFiles AE 3.1.1","tc":"#ffffff"},"v":"4.8.0","fr":60,"op":290,"ip":0,"assets":[{"nm":"","mn":"","layers":[{"ty":0,"nm":"Free Bling","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[4078.5,2294,0],"ix":1},"s":{"a":0,"k":[95.489,95.489,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[2072,890,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"w":8157,"h":4588,"refId":"comp_1","ind":0}],"id":"comp_0","fr":30},{"nm":"","mn":"","layers":[{"ty":0,"nm":"Diamond + Glow","mn":"","sr":1,"st":71,"op":1871,"ip":71,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1548.5,1548.5,0],"ix":1},"s":{"a":0,"k":[-13.664,13.664,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-39.5,1251,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.667,"y":1},"s":[0],"t":71},{"o":{"x":0.333,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":101},{"o":{"x":0.167,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":252},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":270}],"ix":11},"r":{"a":0,"k":-21,"ix":10}},"ef":[],"w":3097,"h":3097,"refId":"comp_2","ind":0,"parent":3},{"ty":0,"nm":"Diamond + Glow","mn":"","sr":1,"st":76,"op":1876,"ip":76,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1548.5,1548.5,0],"ix":1},"s":{"a":0,"k":[-8.664,8.664,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-82.5,1465,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.667,"y":1},"s":[0],"t":76},{"o":{"x":0.333,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":106},{"o":{"x":0.167,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":252},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":270}],"ix":11},"r":{"a":0,"k":-21,"ix":10}},"ef":[],"w":3097,"h":3097,"refId":"comp_2","ind":1,"parent":3},{"ty":0,"nm":"Diamond + Glow","mn":"","sr":1,"st":65,"op":1865,"ip":65,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1548.5,1548.5,0],"ix":1},"s":{"a":0,"k":[17.318,17.318,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[1505.5,1206,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.667,"y":1},"s":[0],"t":65},{"o":{"x":0.333,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":95},{"o":{"x":0.167,"y":0},"i":{"x":0.833,"y":1},"s":[100],"t":252},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":270}],"ix":11},"r":{"a":0,"k":26,"ix":10}},"ef":[],"w":3097,"h":3097,"refId":"comp_2","ind":2,"parent":3},{"ty":0,"nm":"FB","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[2000,2000,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":1,"k":[{"o":{"x":0.24,"y":0},"i":{"x":0.128,"y":1},"s":[5302.5,2294,0],"t":100},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[4078.5,2294,0],"t":153}],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"w":4000,"h":4000,"refId":"comp_4","ind":3},{"ty":4,"nm":"Layer 2 Outlines","mn":"","sr":1,"st":53,"op":1853,"ip":53,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[684.962,602.849,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[820.896,1968.512,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.667,"y":1},"s":[0],"t":53},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":83}],"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[-19.843,-61.434],[0,-36.637],[0.861,-9.77],[178.694,0],[0,0],[0,0],[0,0],[0,37.195],[37.194,0],[0,0],[0,0],[0,0],[0,37.195],[37.194,0],[0,0],[0,0],[0,0],[-15.004,-174.787],[0,-9.98],[10.677,-33.101],[48.175,-40.754]],"o":[[10.677,33.101],[0,9.979],[-15.004,174.786],[0,0],[0,0],[0,0],[37.194,0],[0,-37.171],[0,0],[0,0],[0,0],[37.194,0],[0,-37.172],[0,0],[0,0],[0,0],[178.694,0],[0.861,9.769],[0,36.636],[-19.843,61.433],[48.175,40.731]],"v":[[668.266,155.979],[684.712,261.005],[683.432,290.639],[343.095,602.598],[-279.148,602.598],[-279.148,290.639],[275.777,290.639],[343.095,223.298],[275.777,155.979],[78.821,155.979],[78.821,-155.98],[275.777,-155.98],[343.095,-223.321],[275.777,-290.639],[-684.712,-290.639],[-684.712,-602.598],[343.095,-602.598],[683.432,-290.639],[684.712,-261.004],[668.266,-155.98],[563.496,0.012]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[684.961,602.849],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":4,"parent":3},{"ty":4,"nm":"Layer 3 Outlines","mn":"","sr":1,"st":53,"op":1853,"ip":53,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[342.6,379.539,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[478.535,2191.821,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.667,"y":1},"s":[0],"t":53},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":83}],"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[342.6,379.539],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":5,"parent":3},{"ty":4,"nm":"Shape Layer 1","mn":"","sr":1,"st":18,"op":1818,"ip":18,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":true,"td":1,"ao":0,"ks":{"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":1,"k":[{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[5429,1999,0],"t":100},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[4349,1999,0],"t":153}],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"masksProperties":[{"nm":"Mask 1","mn":"","inv":false,"mode":"a","x":{"a":0,"k":0,"ix":4},"o":{"a":0,"k":100,"ix":3},"pt":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[603.5,-1233],[-3146.5,-1233],[-3146.5,1047],[603.5,1047]]},"ix":1}}],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Rectangle 1","ix":1,"cix":2,"np":3,"it":[{"ty":"rc","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Rect","nm":"Rectangle Path 1","d":1,"p":{"a":0,"k":[0,0],"ix":3},"r":{"a":0,"k":0,"ix":4},"s":{"a":0,"k":[5470,3290],"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[148.603,148.603],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[-1353.5,1],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":6,"parent":3},{"ty":0,"nm":"FREE BLING","mn":"","sr":1,"st":18,"op":1818,"ip":18,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":1,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[4078.5,2294,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":1,"k":[{"o":{"x":0.239,"y":0},"i":{"x":0.118,"y":1},"s":[4078,2294,0],"t":100},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[4806.731,2294,0],"t":153}],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"w":8157,"h":4588,"refId":"comp_5","ind":7}],"id":"comp_1","fr":30},{"nm":"","mn":"","layers":[{"ty":0,"nm":"Diamond","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1106,1106,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[1548.5,1548.5,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"w":2212,"h":2212,"refId":"comp_3","ind":0},{"ty":4,"nm":"Shape Layer 1","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[-591,-708.301,0],"ix":1},"s":{"a":0,"k":[49.657,69.349,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[1106,1506,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":180,"ix":10}},"ef":[{"ty":5,"mn":"ADBE Glo2","nm":"Glow","ix":1,"en":1,"ef":[{"ty":7,"mn":"ADBE Glo2-0001","nm":"Glow Based On","ix":1,"v":{"a":0,"k":2,"ix":1}},{"ty":0,"mn":"ADBE Glo2-0002","nm":"Glow Threshold","ix":2,"v":{"a":0,"k":255,"ix":2}},{"ty":0,"mn":"ADBE Glo2-0003","nm":"Glow Radius","ix":3,"v":{"a":0,"k":1000,"ix":3}},{"ty":0,"mn":"ADBE Glo2-0004","nm":"Glow Intensity","ix":4,"v":{"a":0,"k":1,"ix":4}},{"ty":7,"mn":"ADBE Glo2-0005","nm":"Composite Original","ix":5,"v":{"a":0,"k":2,"ix":5}},{"ty":7,"mn":"ADBE Glo2-0006","nm":"Glow Operation","ix":6,"v":{"a":0,"k":3,"ix":6}},{"ty":7,"mn":"ADBE Glo2-0007","nm":"Glow Colors","ix":7,"v":{"a":0,"k":1,"ix":7}},{"ty":7,"mn":"ADBE Glo2-0008","nm":"Color Looping","ix":8,"v":{"a":0,"k":3,"ix":8}},{"ty":0,"mn":"ADBE Glo2-0009","nm":"Color Loops","ix":9,"v":{"a":0,"k":1,"ix":9}},{"ty":0,"mn":"ADBE Glo2-0010","nm":"Color Phase","ix":10,"v":{"a":0,"k":0,"ix":10}},{"ty":0,"mn":"ADBE Glo2-0011","nm":"A & B Midpoint","ix":11,"v":{"a":0,"k":0.5,"ix":11}},{"ty":2,"mn":"ADBE Glo2-0012","nm":"Color A","ix":12,"v":{"a":0,"k":[1,1,1,0],"ix":12}},{"ty":2,"mn":"ADBE Glo2-0013","nm":"Color B","ix":13,"v":{"a":0,"k":[0,0,0,0],"ix":13}},{"ty":7,"mn":"ADBE Glo2-0014","nm":"Glow Dimensions","ix":14,"v":{"a":0,"k":1,"ix":14}}]},{"ty":5,"mn":"ADBE Glo2","nm":"Glow 2","ix":2,"en":1,"ef":[{"ty":7,"mn":"ADBE Glo2-0001","nm":"Glow Based On","ix":1,"v":{"a":0,"k":2,"ix":1}},{"ty":0,"mn":"ADBE Glo2-0002","nm":"Glow Threshold","ix":2,"v":{"a":0,"k":255,"ix":2}},{"ty":0,"mn":"ADBE Glo2-0003","nm":"Glow Radius","ix":3,"v":{"a":0,"k":0,"ix":3}},{"ty":0,"mn":"ADBE Glo2-0004","nm":"Glow Intensity","ix":4,"v":{"a":0,"k":1,"ix":4}},{"ty":7,"mn":"ADBE Glo2-0005","nm":"Composite Original","ix":5,"v":{"a":0,"k":2,"ix":5}},{"ty":7,"mn":"ADBE Glo2-0006","nm":"Glow Operation","ix":6,"v":{"a":0,"k":3,"ix":6}},{"ty":7,"mn":"ADBE Glo2-0007","nm":"Glow Colors","ix":7,"v":{"a":0,"k":1,"ix":7}},{"ty":7,"mn":"ADBE Glo2-0008","nm":"Color Looping","ix":8,"v":{"a":0,"k":3,"ix":8}},{"ty":0,"mn":"ADBE Glo2-0009","nm":"Color Loops","ix":9,"v":{"a":0,"k":1,"ix":9}},{"ty":0,"mn":"ADBE Glo2-0010","nm":"Color Phase","ix":10,"v":{"a":0,"k":0,"ix":10}},{"ty":0,"mn":"ADBE Glo2-0011","nm":"A & B Midpoint","ix":11,"v":{"a":0,"k":0.5,"ix":11}},{"ty":2,"mn":"ADBE Glo2-0012","nm":"Color A","ix":12,"v":{"a":0,"k":[1,1,1,0],"ix":12}},{"ty":2,"mn":"ADBE Glo2-0013","nm":"Color B","ix":13,"v":{"a":0,"k":[0,0,0,0],"ix":13}},{"ty":7,"mn":"ADBE Glo2-0014","nm":"Glow Dimensions","ix":14,"v":{"a":0,"k":1,"ix":14}}]}],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Polystar 1","ix":1,"cix":2,"np":3,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[24.165,-547.77],[714.907,-304.213],[1248.564,302.725],[810.566,618.78],[-0.001,631.499],[-797.819,610.275],[-1244.543,311.377],[-666.576,-315.749]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.251,0.298,0.3333],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[-591,-456],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":1,"parent":0}],"id":"comp_2","fr":30},{"nm":"","mn":"","layers":[{"ty":3,"nm":"Null 1","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[50,50,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[1106,1106,0],"ix":2},"rz":{"a":1,"k":[{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":0},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[1800],"t":410}],"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":0,"ix":11},"rx":{"a":0,"k":122,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,0],"ix":7}},"ef":[],"ind":0},{"ty":4,"nm":"Bottom-2","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,686,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,0],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":1,"parent":0},{"ty":4,"nm":"Bottom-3","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-500.792,368,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,60],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":2,"parent":0},{"ty":4,"nm":"Bottom-4","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-500.792,-268,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,120],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":3,"parent":0},{"ty":4,"nm":"Bottom-5","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,-586,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,180],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":4,"parent":0},{"ty":4,"nm":"Bottom-6","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[600.792,-268,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,240],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":5,"parent":0},{"ty":4,"nm":"Bottom-1","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,50,0],"ix":1},"s":{"a":0,"k":[419,848,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[600.792,368,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":60,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,300],"ix":7}},"ef":[],"shapes":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"ir":{"a":0,"k":50,"ix":6},"is":{"a":0,"k":0,"ix":8},"pt":{"a":0,"k":3,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":100,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":6,"parent":0},{"ty":4,"nm":"Side-02","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,-577.5,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,0],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":7,"parent":0},{"ty":4,"nm":"Side-03","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[593.431,-263.75,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,60],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":8,"parent":0},{"ty":4,"nm":"Side-04","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[593.431,363.75,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,120],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":9,"parent":0},{"ty":4,"nm":"Side-05","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,677.5,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,180],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":10,"parent":0},{"ty":4,"nm":"Side-06","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-493.431,363.75,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,240],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":11,"parent":0},{"ty":4,"nm":"Side-01","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[0,-46.739,0],"ix":1},"s":{"a":0,"k":[736,325,736],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[-493.431,-263.75,0],"ix":2},"rz":{"a":0,"k":0,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":45.4,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,300],"ix":7}},"ef":[],"shapes":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[50,-50],[31.804,50.09],[-31.837,49.983],[-50,-50]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}}],"ind":12,"parent":0},{"ty":4,"nm":"Top 2","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[-871,-744,0],"ix":1},"s":{"a":0,"k":[50,50,50],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,50,223],"ix":2},"rz":{"a":0,"k":630,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":0,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,0],"ix":7}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Polystar 1","ix":1,"cix":2,"np":3,"it":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"pt":{"a":0,"k":6,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":940.944,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":2},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[-871,-744],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":13,"parent":0},{"ty":4,"nm":"Top","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":1,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[-871,-744,0],"ix":1},"s":{"a":0,"k":[78,78,78],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[50,50,0],"ix":2},"rz":{"a":0,"k":330,"ix":10},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"rx":{"a":0,"k":0,"ix":8},"ry":{"a":0,"k":0,"ix":9},"or":{"a":0,"k":[0,0,0],"ix":7}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Polystar 1","ix":1,"cix":2,"np":3,"it":[{"ty":"sr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Star","nm":"Polystar Path 1","ix":1,"d":1,"pt":{"a":0,"k":6,"ix":3},"p":{"a":0,"k":[0,0],"ix":4},"or":{"a":0,"k":940.944,"ix":7},"os":{"a":0,"k":0,"ix":9},"r":{"a":0,"k":0,"ix":5},"sy":2},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":1,"lj":1,"ml":4,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"d":[],"c":{"a":0,"k":[0,0,0],"ix":3}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[0.1294,0.6196,0.7373],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[-871,-744],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":14,"parent":0}],"id":"comp_3","fr":30},{"nm":"","mn":"","layers":[{"ty":4,"nm":"Layer 6 Outlines 3","mn":"","sr":1,"st":12,"op":1812,"ip":12,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[697.212,615.437,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[820.885,1968.85,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.862,839.084],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 2","ix":2,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[-19.843,-61.434],[0,-36.637],[0.861,-9.77],[178.693,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[-15.004,-174.787],[0,-9.98],[10.677,-33.101],[48.175,-40.754]],"o":[[10.677,33.101],[0,9.979],[-15.004,174.786],[0,0],[0,0],[0,0],[37.195,0],[0,-37.171],[0,0],[0,0],[0,0],[37.195,0],[0,-37.172],[0,0],[0,0],[0,0],[178.693,0],[0.861,9.769],[0,36.636],[-19.843,61.433],[48.175,40.731]],"v":[[668.267,155.979],[684.711,261.005],[683.432,290.639],[343.095,602.598],[-279.148,602.598],[-279.148,290.639],[275.777,290.639],[343.095,223.298],[275.777,155.979],[78.822,155.979],[78.822,-155.98],[275.777,-155.98],[343.095,-223.321],[275.777,-290.639],[-684.711,-290.639],[-684.711,-602.598],[343.095,-602.598],[683.432,-290.639],[684.711,-261.004],[668.267,-155.98],[563.497,0.012]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[697.211,615.099],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 3","ix":3,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.85,838.408],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 1","ix":4,"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"s":{"a":0,"k":0,"ix":1},"m":1},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 2","ix":5,"e":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[0],"t":12},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":103}],"ix":2},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[-146],"t":12},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":112}],"ix":3},"s":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.109,"y":1},"s":[0],"t":22},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":112}],"ix":1},"m":1}],"ind":0},{"ty":4,"nm":"Layer 6 Outlines 2","mn":"","sr":1,"st":6,"op":1806,"ip":6,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[697.212,615.437,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[820.885,1968.85,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[{"ty":21,"mn":"ADBE Fill","nm":"Fill","ix":1,"en":1,"ef":[{"ty":10,"mn":"ADBE Fill-0001","nm":"Fill Mask","ix":1,"v":{"a":0,"k":0,"ix":1}},{"ty":7,"mn":"ADBE Fill-0007","nm":"All Masks","ix":2,"v":{"a":0,"k":0,"ix":2}},{"ty":2,"mn":"ADBE Fill-0002","nm":"Color","ix":3,"v":{"a":0,"k":[0.3843,0.3843,0.3843],"ix":3}},{"ty":7,"mn":"ADBE Fill-0006","nm":"Invert","ix":4,"v":{"a":0,"k":0,"ix":4}},{"ty":0,"mn":"ADBE Fill-0003","nm":"Horizontal Feather","ix":5,"v":{"a":0,"k":0,"ix":5}},{"ty":0,"mn":"ADBE Fill-0004","nm":"Vertical Feather","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"mn":"ADBE Fill-0005","nm":"Opacity","ix":7,"v":{"a":0,"k":1,"ix":7}}]}],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.862,839.084],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 2","ix":2,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[-19.843,-61.434],[0,-36.637],[0.861,-9.77],[178.693,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[-15.004,-174.787],[0,-9.98],[10.677,-33.101],[48.175,-40.754]],"o":[[10.677,33.101],[0,9.979],[-15.004,174.786],[0,0],[0,0],[0,0],[37.195,0],[0,-37.171],[0,0],[0,0],[0,0],[37.195,0],[0,-37.172],[0,0],[0,0],[0,0],[178.693,0],[0.861,9.769],[0,36.636],[-19.843,61.433],[48.175,40.731]],"v":[[668.267,155.979],[684.711,261.005],[683.432,290.639],[343.095,602.598],[-279.148,602.598],[-279.148,290.639],[275.777,290.639],[343.095,223.298],[275.777,155.979],[78.822,155.979],[78.822,-155.98],[275.777,-155.98],[343.095,-223.321],[275.777,-290.639],[-684.711,-290.639],[-684.711,-602.598],[343.095,-602.598],[683.432,-290.639],[684.711,-261.004],[668.267,-155.98],[563.497,0.012]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[697.211,615.099],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 3","ix":3,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.85,838.408],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 1","ix":4,"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"s":{"a":0,"k":0,"ix":1},"m":1},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 2","ix":5,"e":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[0],"t":6},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":97}],"ix":2},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[-146],"t":6},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":106}],"ix":3},"s":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.109,"y":1},"s":[0],"t":16},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":106}],"ix":1},"m":1}],"ind":1},{"ty":4,"nm":"Layer 6 Outlines","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[697.212,615.437,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[820.885,1968.85,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[{"ty":21,"mn":"ADBE Fill","nm":"Fill","ix":1,"en":1,"ef":[{"ty":10,"mn":"ADBE Fill-0001","nm":"Fill Mask","ix":1,"v":{"a":0,"k":0,"ix":1}},{"ty":7,"mn":"ADBE Fill-0007","nm":"All Masks","ix":2,"v":{"a":0,"k":0,"ix":2}},{"ty":2,"mn":"ADBE Fill-0002","nm":"Color","ix":3,"v":{"a":0,"k":[1,1,1],"ix":3}},{"ty":7,"mn":"ADBE Fill-0006","nm":"Invert","ix":4,"v":{"a":0,"k":0,"ix":4}},{"ty":0,"mn":"ADBE Fill-0003","nm":"Horizontal Feather","ix":5,"v":{"a":0,"k":0,"ix":5}},{"ty":0,"mn":"ADBE Fill-0004","nm":"Vertical Feather","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"mn":"ADBE Fill-0005","nm":"Opacity","ix":7,"v":{"a":0,"k":1,"ix":7}}]}],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.862,839.084],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 2","ix":2,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[-19.843,-61.434],[0,-36.637],[0.861,-9.77],[178.693,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[0,37.195],[37.195,0],[0,0],[0,0],[0,0],[-15.004,-174.787],[0,-9.98],[10.677,-33.101],[48.175,-40.754]],"o":[[10.677,33.101],[0,9.979],[-15.004,174.786],[0,0],[0,0],[0,0],[37.195,0],[0,-37.171],[0,0],[0,0],[0,0],[37.195,0],[0,-37.172],[0,0],[0,0],[0,0],[178.693,0],[0.861,9.769],[0,36.636],[-19.843,61.433],[48.175,40.731]],"v":[[668.267,155.979],[684.711,261.005],[683.432,290.639],[343.095,602.598],[-279.148,602.598],[-279.148,290.639],[275.777,290.639],[343.095,223.298],[275.777,155.979],[78.822,155.979],[78.822,-155.98],[275.777,-155.98],[343.095,-223.321],[275.777,-290.639],[-684.711,-290.639],[-684.711,-602.598],[343.095,-602.598],[683.432,-290.639],[684.711,-261.004],[668.267,-155.98],[563.497,0.012]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[697.211,615.099],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 3","ix":3,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[342.35,-379.289],[342.35,-67.33],[-30.368,-67.33],[-30.368,379.289],[-342.35,379.289],[-342.35,-379.289]]},"ix":2}},{"ty":"st","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Stroke","nm":"Stroke 1","lc":2,"lj":1,"ml":10,"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"d":[],"c":{"a":0,"k":[0.1373,0.1216,0.1255],"ix":3}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[354.85,838.408],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 1","ix":4,"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"s":{"a":0,"k":0,"ix":1},"m":1},{"ty":"tm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Trim","nm":"Trim Paths 2","ix":5,"e":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[0],"t":0},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":91}],"ix":2},"o":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.174,"y":1},"s":[-146],"t":0},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[0],"t":100}],"ix":3},"s":{"a":1,"k":[{"o":{"x":0.333,"y":0},"i":{"x":0.109,"y":1},"s":[0],"t":10},{"o":{"x":0.167,"y":0.167},"i":{"x":0.833,"y":0.833},"s":[100],"t":100}],"ix":1},"m":1}],"ind":2}],"id":"comp_4","fr":30},{"nm":"","mn":"","layers":[{"ty":4,"nm":"Layer 4 Outlines","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[960.953,263.944,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[3912.731,1947.219,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[210.954,263.693],[-210.954,263.693],[-210.954,-263.693],[210.954,-263.693],[210.954,-187.34],[-125.942,-187.34],[-125.942,-39.357],[199.147,-39.357],[199.147,36.21],[-125.942,36.21],[-125.942,187.34],[210.954,187.34]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[1710.702,263.944],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 2","ix":2,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[210.954,263.693],[-210.954,263.693],[-210.954,-263.693],[210.954,-263.693],[210.954,-187.34],[-125.942,-187.34],[-125.942,-39.357],[199.147,-39.357],[199.147,36.21],[-125.942,36.21],[-125.942,187.34],[210.954,187.34]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[1222.677,263.944],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 3","ix":3,"cix":2,"np":4,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[-17.318,13.91],[0,32.544],[17.316,14.44],[36.209,0]],"o":[[0,0],[0,0],[36.209,0],[17.316,-13.899],[0,-31.485],[-17.318,-14.426],[0,0]],"v":[[-153.886,-187.341],[-153.886,-6.297],[34.241,-6.297],[114.529,-27.156],[140.505,-96.819],[114.529,-165.694],[34.241,-187.341]]},"ix":2}},{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 2","ix":2,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[-34.377,-27.279],[0,-48.803],[27.281,-25.705],[49.848,-2.62],[0,0],[-11.548,-10.491],[-10.504,-18.891],[0,0],[0,0],[0,0],[15.744,8.659],[32.002,0],[0,0]],"o":[[0,0],[0,0],[0,0],[60.34,0],[34.363,27.291],[0,43.563],[-27.291,25.717],[0,0],[18.891,5.252],[11.538,10.504],[0,0],[0,0],[0,0],[-12.594,-22.04],[-15.742,-8.658],[0,0],[0,0]],"v":[[-153.886,263.693],[-238.899,263.693],[-238.899,-263.693],[35.027,-263.693],[177.108,-222.762],[228.664,-108.626],[187.733,-4.723],[72.024,37.783],[72.024,43.293],[117.678,66.907],[150.739,110.987],[238.899,263.693],[141.292,263.693],[57.067,118.071],[14.56,72.023],[-57.068,59.036],[-153.886,59.036]]},"ix":2}},{"ty":"mm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Merge","nm":"Merge Paths 1","mm":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[721.661,263.944],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 4","ix":4,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-124.762,263.693],[-209.775,263.693],[-209.775,-263.693],[209.775,-263.693],[209.775,-187.34],[-124.762,-187.34],[-124.762,-23.614],[197.965,-23.614],[197.965,51.952],[-124.762,51.952]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[210.024,263.944],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":0},{"ty":4,"nm":"Layer 5 Outlines","mn":"","sr":1,"st":0,"op":1800,"ip":0,"hd":false,"cl":"","ln":"","ddd":0,"bm":0,"tt":0,"hasMask":false,"td":0,"ao":0,"ks":{"a":{"a":0,"k":[1119.555,271.815,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6},"sk":{"a":0,"k":0},"p":{"a":0,"k":[4070.748,2601.421,0],"ix":2},"sa":{"a":0,"k":0},"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10}},"ef":[],"shapes":[{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 1","ix":1,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[50.895,0],[47.229,49.59],[0,81.863],[-50.906,49.332],[-84.496,0],[-46.713,-35.679],[0,-60.34],[0,0],[0,0],[0,0],[108.095,0],[30.699,-30.698],[0,-65.592],[-29.91,-30.699],[-62.455,0],[-27.033,18.891],[0,44.609],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[33.061,-21.253]],"o":[[-77.668,0],[-47.228,-49.59],[0,-82.38],[50.894,-49.319],[75.564,0],[46.699,35.693],[0,0],[0,0],[0,0],[0,-77.657],[-63.502,0],[-30.7,30.699],[0,65.074],[29.912,30.698],[62.971,0],[27.022,-18.892],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-11.021,38.841],[-33.06,21.253]],"v":[[-8.265,271.565],[-195.605,197.18],[-266.449,0.001],[-190.095,-197.573],[12.989,-271.565],[196.393,-218.039],[266.449,-73.991],[266.449,-67.694],[172.778,-67.694],[172.778,-73.991],[10.628,-190.489],[-130.665,-144.441],[-176.714,0.001],[-131.847,143.655],[6.692,189.703],[141.686,161.365],[182.225,66.121],[182.225,59.037],[-12.988,59.037],[-12.988,-5.509],[266.449,-5.509],[266.449,263.694],[189.309,263.694],[189.309,149.558],[183.799,149.558],[117.677,239.686]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[1972.41,271.814],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 2","ix":2,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-156.641,263.693],[-241.654,263.693],[-241.654,-263.693],[-150.344,-263.693],[85.8,44.868],[154.281,143.26],[160.578,143.26],[156.642,48.016],[156.642,-263.693],[241.654,-263.693],[241.654,263.693],[150.344,263.693],[-87.372,-44.079],[-153.493,-136.963],[-159.79,-136.963],[-156.641,-45.654]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[1401.344,271.814],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 3","ix":3,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[42.506,263.693],[-42.506,263.693],[-42.506,-263.693],[42.506,-263.693]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[1038.479,271.814],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 4","ix":4,"cix":2,"np":2,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[205.051,263.693],[-205.051,263.693],[-205.051,-263.693],[-120.039,-263.693],[-120.039,187.34],[205.051,187.34]]},"ix":2}},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[735.83,271.814],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]},{"ty":"gr","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Group","nm":"Group 5","ix":5,"cix":2,"np":5,"it":[{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 1","ix":1,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[-15.226,12.864],[0,28.337],[62.444,0]],"o":[[0,0],[0,0],[32.003,0],[15.215,-12.853],[0,-52.469],[0,0]],"v":[[-153.493,27.549],[-153.493,187.339],[59.035,187.339],[129.878,168.056],[152.706,106.265],[59.035,27.549]]},"ix":2}},{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 2","ix":2,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[0,0],[0,0],[0,0],[0,48.286],[57.719,0]],"o":[[0,0],[0,0],[59.294,0],[0,-48.274],[0,0]],"v":[[-153.493,-187.342],[-153.493,-42.506],[45.654,-42.506],[134.602,-114.923],[48.016,-187.342]]},"ix":2}},{"ty":"sh","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Shape - Group","nm":"Path 3","ix":3,"d":1,"ks":{"a":0,"k":{"c":true,"i":[[54.042,0],[0,0],[0,0],[0,0],[-28.338,-22.557],[0,-41.448],[18.891,-20.723],[38.83,-4.723],[0,0],[-22.569,-22.298],[0,-35.679],[29.383,-25.705]],"o":[[0,0],[0,0],[0,0],[50.894,0],[28.336,22.569],[0,32.543],[-18.893,20.737],[0,0],[44.081,5.252],[22.556,22.311],[0,45.655],[-29.395,25.718]],"v":[[68.482,263.693],[-237.717,263.693],[-237.717,-263.693],[57.461,-263.693],[176.321,-229.847],[218.827,-133.814],[190.489,-53.921],[103.903,-15.744],[103.903,-10.234],[203.871,31.092],[237.717,118.071],[193.637,225.122]]},"ix":2}},{"ty":"mm","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Filter - Merge","nm":"Merge Paths 1","mm":1},{"ty":"fl","bm":0,"cl":"","ln":"","hd":false,"mn":"ADBE Vector Graphic - Fill","nm":"Fill 1","c":{"a":0,"k":[1,1,1],"ix":4},"r":1,"o":{"a":0,"k":100,"ix":5}},{"ty":"tr","a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"sk":{"a":0,"k":0,"ix":4},"p":{"a":0,"k":[237.967,271.815],"ix":2},"r":{"a":0,"k":0,"ix":6},"sa":{"a":0,"k":0,"ix":5},"o":{"a":0,"k":100,"ix":7}}]}],"ind":1}],"id":"comp_5","fr":30}]}');
;// CONCATENATED MODULE: ./components/Marketing/Layouts/Footer.tsx





function Footer() {
    return /*#__PURE__*/ jsx_runtime_.jsx("footer", {
        className: "flex justify-between w-full bg-darkGreen/30 py-20",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "max-w-6xl px-5 md:px-0 mx-auto w-full",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "flex items-center justify-between py-[30px] space-x-20 w-full",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            className: "relative block h-16 w-32 flex-shrink-0 cursor-pointer",
                            href: "/",
                            children: /*#__PURE__*/ jsx_runtime_.jsx((external_lottie_react_default()), {
                                animationData: logo_namespaceObject,
                                loop: false
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "hidden md:block text-white",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "max-w-[359px]",
                                children: "Web3 Marketing & Rewards Platform"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "inline-flex items-center space-x-6 text-white text-sm uppercase",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "hover:text-yellow hover:animate-bounce",
                                    href: "https://fb.com/freebling",
                                    target: "_blank",
                                    rel: "noreferrer",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_spring_.animated.img, {
                                        style: {
                                            width: 24,
                                            height: 24
                                        },
                                        src: "/Marketing/imgs/icon-facebook.svg",
                                        alt: "Freebling Facebook"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "hover:text-yellow hover:animate-bounce",
                                    href: "https://instagram.com/freebling",
                                    target: "_blank",
                                    rel: "noreferrer",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_spring_.animated.img, {
                                        style: {
                                            width: 24,
                                            height: 24
                                        },
                                        src: "/Marketing/imgs/icon-instagram.svg",
                                        alt: "Freebling Instagram"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "hover:text-yellow hover:animate-bounce",
                                    href: "https://twitter.com/freeblingio",
                                    target: "_blank",
                                    rel: "noreferrer",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_spring_.animated.img, {
                                        style: {
                                            width: 24,
                                            height: 24
                                        },
                                        src: "/Marketing/imgs/icon-twitter.svg",
                                        alt: "Freebling Twitter"
                                    })
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "inline-flex border-y border-white/20 justify-center items-center py-[30px] space-x-10 text-white text-sm capitalize w-full",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            className: "LinkTxt",
                            href: "https://learn.freebling.io/privacy-policy",
                            target: "_blank",
                            children: "Privacy Policy"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            className: "LinkTxt",
                            href: "https://learn.freebling.io/terms-and-conditions",
                            target: "_blank",
                            children: "Terms & Conditions"
                        })
                    ]
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "inline-flex justify-between items-center py-[30px] space-x-10 w-full",
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                        className: "mx-auto text-center text-white text-sm",
                        children: [
                            "Copyright \xa9 2023 ",
                            /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: "/",
                                children: "FreeBling.io"
                            }),
                            ". All rights reserved."
                        ]
                    })
                })
            ]
        })
    });
}
/* harmony default export */ const Layouts_Footer = (Footer);


/***/ }),

/***/ 2962:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ NavbarMobile)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(467);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1664);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__]);
_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];





function NavbarMobile(props) {
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
    const currentRoute = router.pathname;
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: props.UserType === "company" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "fixed bottom-0 right-0 left-0 lg:hidden",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "w-full h-[88px] bg-fbblack-100",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                    className: "flex items-center justify-between h-full px-5",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "content-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                className: "",
                                href: props.UserType === "company" ? "/company/dashboard" : "/users/dashboard",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.HomeIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/company/live-events",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.GiftIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/company/giveaway",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.PlusCircleIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/company/followers",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.UserGroupIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/company/stats",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.ChartPieIcon, {
                                    className: "NavIcon"
                                })
                            })
                        })
                    ]
                })
            })
        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "fixed bottom-0 right-0 left-0 lg:hidden",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "w-full h-[88px] bg-fbblack-100",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                    className: "flex items-center justify-between h-full px-5",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "content-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                className: "",
                                href: props.UserType === "user" ? "/users/dashboard" : "/users/dashboard",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.HomeIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/users/my-entries",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.GiftIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/users/favorites",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.StarIcon, {
                                    className: "NavIcon"
                                })
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "text-center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_4___default()), {
                                href: "/users/profile",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_2__.UserIcon, {
                                    className: "NavIcon"
                                })
                            })
                        })
                    ]
                })
            })
        })
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 8005:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ SideBar)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lottie_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6164);
/* harmony import */ var lottie_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lottie_react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_assets_anim_logo_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9269);
/* harmony import */ var _heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(467);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8762);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1664);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(7495);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_reactour_tour__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2505);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1492);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(236);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_12__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__, firebase_firestore__WEBPACK_IMPORTED_MODULE_10__, _firebase__WEBPACK_IMPORTED_MODULE_11__]);
([_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__, _context_authcontext__WEBPACK_IMPORTED_MODULE_5__, _Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__, firebase_firestore__WEBPACK_IMPORTED_MODULE_10__, _firebase__WEBPACK_IMPORTED_MODULE_11__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);













function SideBar() {
    const { logOut  } = (0,_context_authcontext__WEBPACK_IMPORTED_MODULE_5__/* .useAuth */ .a)();
    const { setSteps , setIsOpen , setCurrentStep  } = (0,_reactour_tour__WEBPACK_IMPORTED_MODULE_8__.useTour)();
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_6__.useRouter)();
    const currentRoute = router.pathname;
    const handleLogout = async ()=>{
        try {
            await logOut();
            router.push("/login");
        } catch (error) {
            console.log(error.message);
        }
    };
    const launchBusinessTutorial = ()=>{
        setSteps?.(_Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__/* .businessTutorial */ .Br);
        setCurrentStep(0);
        setIsOpen(true);
    };
    const launchCustomerTutorial = async ()=>{
        const q = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_11__.db, "giveaway"));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_10__.getDocs)(q);
        const giveaways = querySnapshot.docs.map((doc)=>doc.data());
        if (giveaways.length > 0) {
            setSteps?.(_Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__/* .customerTutorialWithGiveaway */ .$L);
        } else {
            setSteps?.(_Tutorials_Tutorials__WEBPACK_IMPORTED_MODULE_9__/* .customerTutorialWithoutGiveaway */ .jg);
        }
        setCurrentStep(0);
        setIsOpen(true);
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "sideBar",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mx-auto w-[128px] mb-6 md:mb-[45px]",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                    href: "/",
                    className: "block",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((lottie_react__WEBPACK_IMPORTED_MODULE_2___default()), {
                        animationData: _public_assets_anim_logo_json__WEBPACK_IMPORTED_MODULE_3__,
                        loop: false
                    })
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "flex flex-col items-center justify-center h-full",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                    className: "w-full h-full space-y-2",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                                href: "/",
                                className: "SideNavLink",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__.HomeIcon, {
                                        className: "UtilityIcon sidenav-home"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt",
                                        children: "Home"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                                href: "/giveaways",
                                className: "SideNavLink",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__.GiftIcon, {
                                        className: "UtilityIcon sidenav-giveaways"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt",
                                        children: "Giveaways"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                                href: "#",
                                className: "SideNavLink cursor-not-allowed",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_12___default()), {
                                        src: "/assets/icons/icon-airdrops.svg",
                                        className: "UtilityIcon sidenav-giveaways",
                                        alt: "Airdrop icon",
                                        width: 5,
                                        height: 5
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt Disabled",
                                        children: "Airdrops"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                                href: "#",
                                className: "SideNavLink cursor-not-allowed",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_12___default()), {
                                        src: "/assets/icons/icon-rewards.svg",
                                        className: "UtilityIcon sidenav-giveaways",
                                        alt: "Reward icon",
                                        width: 5,
                                        height: 5
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt Disabled",
                                        children: "Rewards"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_link__WEBPACK_IMPORTED_MODULE_7___default()), {
                                href: "#",
                                className: "SideNavLink cursor-not-allowed",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_12___default()), {
                                        src: "/assets/icons/icon-cashout.svg",
                                        className: "UtilityIcon sidenav-giveaways",
                                        alt: "Cashout icon",
                                        width: 5,
                                        height: 5
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt Disabled",
                                        children: "Cashout"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "group first-step",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                href: "https://learn.freebling.io",
                                target: "_blank",
                                className: "SideNavLink",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_24_outline__WEBPACK_IMPORTED_MODULE_4__.AcademicCapIcon, {
                                        className: "UtilityIcon sidenav-giveaways"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "LinkTxt",
                                        children: "Knowledge Base"
                                    })
                                ]
                            })
                        })
                    ]
                })
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 2357:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep1 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Business HomePage",
        description: "On business home page you have overview of your current giveaways all of the winners from past giveaways.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep1);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 9080:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep2 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Create your giveaway",
        description: "On this page you can select your template and start creating your giveaways.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep2);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 4342:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep3 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Giveaways",
        description: "On this page you have a total review of all of your live, ended and giveaway drafts, as well as giveaway statistics.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep3);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7123:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep4 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Public Business Profile",
        description: "On this page you have a total overview of information about the company, followers number and all giveaways.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep4);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 2433:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep5 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Bussines admin panel",
        description: "On this page you have a total overview of all giveaway statistics devided by categories and filters.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep5);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 4164:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const BusinessTutorialStep6 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Start creating giveaways",
        description: "You can select any of the pre-made giveaway templates or you can custom create your own giveaway.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BusinessTutorialStep6);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 1490:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7495);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_reactour_tour__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1492);
/* harmony import */ var _context_userDataHook__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1083);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(236);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_firestore__WEBPACK_IMPORTED_MODULE_2__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_3__, _firebase__WEBPACK_IMPORTED_MODULE_4__]);
([firebase_firestore__WEBPACK_IMPORTED_MODULE_2__, _context_userDataHook__WEBPACK_IMPORTED_MODULE_3__, _firebase__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);





const TutorialContent = ({ title , description , currentStep , setCurrentStep , setIsOpen  })=>{
    const { steps  } = (0,_reactour_tour__WEBPACK_IMPORTED_MODULE_1__.useTour)();
    const { userData , updateUserData  } = (0,_context_userDataHook__WEBPACK_IMPORTED_MODULE_3__/* .useUserData */ .v)();
    const updateIsNew = async ()=>{
        const qry = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.query)((0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.collection)(_firebase__WEBPACK_IMPORTED_MODULE_4__.db, "users"), (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.where)("uid", "==", userData?.uid));
        const querySnapshot = await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.getDocs)(qry);
        const temp = querySnapshot.docs.map((doc)=>{
            // console.log doc ref.id
            return doc.ref.id;
        });
        const ref = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.doc)(_firebase__WEBPACK_IMPORTED_MODULE_4__.db, "users", temp[0]);
        await (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_2__.updateDoc)(ref, {
            isNew: false
        });
        updateUserData();
    };
    const handleNext = ()=>{
        if (currentStep + 1 === steps.length) {
            setIsOpen(false);
            updateIsNew();
            return;
        } else if (currentStep + 1 < steps.length) {
            setCurrentStep((step)=>step + 1);
        }
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "text-lg font-medium mb-[16px]",
                children: title
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "text-base opacity-60 mb-[14px]",
                children: description
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex justify-between",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "text-xs",
                        children: [
                            currentStep + 1,
                            "/",
                            steps.length
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "text-xs underline uppercase cursor-pointer",
                        onClick: handleNext,
                        children: currentStep + 1 === steps.length ? "finish onboarding" : "next"
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TutorialContent);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 2505:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "$L": () => (/* binding */ customerTutorialWithGiveaway),
/* harmony export */   "Br": () => (/* binding */ businessTutorial),
/* harmony export */   "jg": () => (/* binding */ customerTutorialWithoutGiveaway)
/* harmony export */ });
/* harmony import */ var _BusinessTutorials_Step1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2357);
/* harmony import */ var _BusinessTutorials_Step2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9080);
/* harmony import */ var _BusinessTutorials_Step3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4342);
/* harmony import */ var _BusinessTutorials_Step4__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7123);
/* harmony import */ var _BusinessTutorials_Step5__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2433);
/* harmony import */ var _BusinessTutorials_Step6__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4164);
/* harmony import */ var _UserTutorials_Step1__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5359);
/* harmony import */ var _UserTutorials_Step2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7017);
/* harmony import */ var _UserTutorials_Step3__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5559);
/* harmony import */ var _UserTutorials_Step4__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5515);
/* harmony import */ var _UserTutorials_Step5__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(8054);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_BusinessTutorials_Step1__WEBPACK_IMPORTED_MODULE_0__, _BusinessTutorials_Step2__WEBPACK_IMPORTED_MODULE_1__, _BusinessTutorials_Step3__WEBPACK_IMPORTED_MODULE_2__, _BusinessTutorials_Step4__WEBPACK_IMPORTED_MODULE_3__, _BusinessTutorials_Step5__WEBPACK_IMPORTED_MODULE_4__, _BusinessTutorials_Step6__WEBPACK_IMPORTED_MODULE_5__, _UserTutorials_Step1__WEBPACK_IMPORTED_MODULE_6__, _UserTutorials_Step2__WEBPACK_IMPORTED_MODULE_7__, _UserTutorials_Step3__WEBPACK_IMPORTED_MODULE_8__, _UserTutorials_Step4__WEBPACK_IMPORTED_MODULE_9__, _UserTutorials_Step5__WEBPACK_IMPORTED_MODULE_10__]);
([_BusinessTutorials_Step1__WEBPACK_IMPORTED_MODULE_0__, _BusinessTutorials_Step2__WEBPACK_IMPORTED_MODULE_1__, _BusinessTutorials_Step3__WEBPACK_IMPORTED_MODULE_2__, _BusinessTutorials_Step4__WEBPACK_IMPORTED_MODULE_3__, _BusinessTutorials_Step5__WEBPACK_IMPORTED_MODULE_4__, _BusinessTutorials_Step6__WEBPACK_IMPORTED_MODULE_5__, _UserTutorials_Step1__WEBPACK_IMPORTED_MODULE_6__, _UserTutorials_Step2__WEBPACK_IMPORTED_MODULE_7__, _UserTutorials_Step3__WEBPACK_IMPORTED_MODULE_8__, _UserTutorials_Step4__WEBPACK_IMPORTED_MODULE_9__, _UserTutorials_Step5__WEBPACK_IMPORTED_MODULE_10__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);











const businessTutorial = [
    {
        selector: ".business-step1",
        content: _BusinessTutorials_Step1__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".business-step2",
        content: _BusinessTutorials_Step2__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".business-step3",
        content: _BusinessTutorials_Step3__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".business-step4",
        content: _BusinessTutorials_Step4__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".business-step5",
        content: _BusinessTutorials_Step5__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".business-step6",
        content: _BusinessTutorials_Step6__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z,
        position: "right"
    }
];
const customerTutorialWithGiveaway = [
    {
        selector: ".customer-step1",
        content: _UserTutorials_Step1__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step2",
        content: _UserTutorials_Step2__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step3",
        content: _UserTutorials_Step3__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step4",
        content: _UserTutorials_Step4__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step5",
        content: _UserTutorials_Step5__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z,
        position: "right"
    }
];
const customerTutorialWithoutGiveaway = [
    {
        selector: ".customer-step1",
        content: _UserTutorials_Step1__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step2",
        content: _UserTutorials_Step2__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step3",
        content: _UserTutorials_Step3__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z,
        position: "right"
    },
    {
        selector: ".customer-step4",
        content: _UserTutorials_Step4__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z,
        position: "right"
    }
];

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5359:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const CustomerTutorialStep1 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Home",
        description: "On home page you have overview of all live giveaways and all of the companies that host them.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerTutorialStep1);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7017:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const CustomerTutorialStep2 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Entered competitions",
        description: "On this page you have a total overview of all of the giveaways that you entered that are still live or are ended.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerTutorialStep2);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5559:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const CustomerTutorialStep3 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Live Competitions",
        description: "On this page you have a total overview of all of the giveaways that are live, upcoming, featured or ended.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerTutorialStep3);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5515:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const CustomerTutorialStep4 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Account",
        description: "This is your account, where you can customize your information and overall platform experience.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerTutorialStep4);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 8054:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1490);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_TutorialContent__WEBPACK_IMPORTED_MODULE_1__]);
_TutorialContent__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];


const CustomerTutorialStep5 = (props)=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_TutorialContent__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
        title: "Giveaway",
        description: "Here you can look through ll of the avaliable giveaways and entre the ones that suit you best.",
        ...props
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerTutorialStep5);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;