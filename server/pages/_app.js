(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 5484:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _reactour_popover_dist_index_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3214);
/* harmony import */ var _reactour_popover_dist_index_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_reactour_popover_dist_index_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7495);
/* harmony import */ var _reactour_tour__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_reactour_tour__WEBPACK_IMPORTED_MODULE_2__);



const TutorialsWrapper = ({ children  })=>{
    const opositeSide = {
        top: "bottom",
        bottom: "top",
        right: "left",
        left: "right"
    };
    const makeArrow = (position, verticalAlign, horizontalAlign)=>{
        if (!position || position === "custom") {
            return {};
        }
        const width = 16;
        const height = 12;
        const color = "#257D86";
        const isVertical = position === "top" || position === "bottom";
        const spaceFromSide = 10;
        const obj = {
            [`--rtp-arrow-${isVertical ? opositeSide[horizontalAlign] : verticalAlign}`]: height + spaceFromSide + "px",
            [`--rtp-arrow-${opositeSide[position]}`]: -height + 2 + "px",
            [`--rtp-arrow-border-${isVertical ? "left" : "top"}`]: `${width / 2}px solid transparent`,
            [`--rtp-arrow-border-${isVertical ? "right" : "bottom"}`]: `${width / 2}px solid transparent`,
            [`--rtp-arrow-border-${position}`]: `${height}px solid ${color}`
        };
        return obj;
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_reactour_tour__WEBPACK_IMPORTED_MODULE_2__.TourProvider, {
        steps: [],
        disableFocusLock: true,
        disableKeyboardNavigation: true,
        disableDotsNavigation: true,
        scrollSmooth: true,
        disableInteraction: true,
        styles: {
            popover: (base, state)=>({
                    ...base,
                    minWidth: "320px",
                    backgroundColor: "#257D86",
                    borderRadius: "8px",
                    padding: "16px",
                    ...makeArrow(state?.position, state?.verticalAlign, state?.horizontalAlign)
                }),
            dot: (base)=>({
                    ...base,
                    display: "none"
                }),
            close: (base)=>({
                    ...base,
                    display: "none"
                }),
            badge: (base)=>({
                    ...base,
                    display: "none"
                }),
            controls: (base)=>({
                    ...base,
                    display: "none"
                })
        },
        children: children
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TutorialsWrapper);


/***/ }),

/***/ 3847:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6764);
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _context_authcontext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8762);
/* harmony import */ var react_hot_toast__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6201);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_Tutorials_TutorialsWrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5484);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var wagmi__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8998);
/* harmony import */ var wagmi_providers_public__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8577);
/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1649);
/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_auth_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var wagmi_chains__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(7697);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authcontext__WEBPACK_IMPORTED_MODULE_2__, react_hot_toast__WEBPACK_IMPORTED_MODULE_3__, wagmi__WEBPACK_IMPORTED_MODULE_7__, wagmi_providers_public__WEBPACK_IMPORTED_MODULE_8__, wagmi_chains__WEBPACK_IMPORTED_MODULE_10__]);
([_context_authcontext__WEBPACK_IMPORTED_MODULE_2__, react_hot_toast__WEBPACK_IMPORTED_MODULE_3__, wagmi__WEBPACK_IMPORTED_MODULE_7__, wagmi_providers_public__WEBPACK_IMPORTED_MODULE_8__, wagmi_chains__WEBPACK_IMPORTED_MODULE_10__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);











const { publicClient , webSocketPublicClient  } = (0,wagmi__WEBPACK_IMPORTED_MODULE_7__.configureChains)([
    wagmi_chains__WEBPACK_IMPORTED_MODULE_10__.mainnet
], [
    (0,wagmi_providers_public__WEBPACK_IMPORTED_MODULE_8__.publicProvider)()
]);
const config = (0,wagmi__WEBPACK_IMPORTED_MODULE_7__.createConfig)({
    autoConnect: true,
    publicClient,
    webSocketPublicClient
});
function MyApp({ Component , pageProps  }) {
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_4__.useRouter)();
    (0,react__WEBPACK_IMPORTED_MODULE_6__.useEffect)(()=>{
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.register("../firebase-messaging-sw.js").then((registration)=>{
                console.log("Service Worker registered with scope:", registration.scope);
            }).catch((error)=>{
                console.error("Service Worker registration failed:", error);
            });
        }
    }, []);
    if (router.asPath == "/" || router.asPath == "/signup" || router.asPath == "/forgot-password" || router.asPath == "/login" || router.asPath == "/reset-password") {
        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_context_authcontext__WEBPACK_IMPORTED_MODULE_2__/* .AuthContextProvider */ .H, {
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(wagmi__WEBPACK_IMPORTED_MODULE_7__.WagmiConfig, {
                        config: config,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_auth_react__WEBPACK_IMPORTED_MODULE_9__.SessionProvider, {
                            session: pageProps.session,
                            refetchInterval: 0,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {
                                ...pageProps
                            })
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_hot_toast__WEBPACK_IMPORTED_MODULE_3__.Toaster, {})
            ]
        });
    } else {
        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_context_authcontext__WEBPACK_IMPORTED_MODULE_2__/* .AuthContextProvider */ .H, {
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Tutorials_TutorialsWrapper__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(wagmi__WEBPACK_IMPORTED_MODULE_7__.WagmiConfig, {
                            config: config,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_auth_react__WEBPACK_IMPORTED_MODULE_9__.SessionProvider, {
                                session: pageProps.session,
                                refetchInterval: 0,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {
                                    ...pageProps
                                })
                            })
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_hot_toast__WEBPACK_IMPORTED_MODULE_3__.Toaster, {})
            ]
        });
    }
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 3214:
/***/ (() => {



/***/ }),

/***/ 6764:
/***/ (() => {



/***/ }),

/***/ 7495:
/***/ ((module) => {

"use strict";
module.exports = require("@reactour/tour");

/***/ }),

/***/ 1649:
/***/ ((module) => {

"use strict";
module.exports = require("next-auth/react");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3745:
/***/ ((module) => {

"use strict";
module.exports = import("firebase/app");;

/***/ }),

/***/ 401:
/***/ ((module) => {

"use strict";
module.exports = import("firebase/auth");;

/***/ }),

/***/ 1492:
/***/ ((module) => {

"use strict";
module.exports = import("firebase/firestore");;

/***/ }),

/***/ 3392:
/***/ ((module) => {

"use strict";
module.exports = import("firebase/storage");;

/***/ }),

/***/ 6201:
/***/ ((module) => {

"use strict";
module.exports = import("react-hot-toast");;

/***/ }),

/***/ 8998:
/***/ ((module) => {

"use strict";
module.exports = import("wagmi");;

/***/ }),

/***/ 7697:
/***/ ((module) => {

"use strict";
module.exports = import("wagmi/chains");;

/***/ }),

/***/ 8577:
/***/ ((module) => {

"use strict";
module.exports = import("wagmi/providers/public");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [762], () => (__webpack_exec__(3847)));
module.exports = __webpack_exports__;

})();