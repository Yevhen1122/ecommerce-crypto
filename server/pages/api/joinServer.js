"use strict";
(() => {
var exports = {};
exports.id = 151;
exports.ids = [151];
exports.modules = {

/***/ 9648:
/***/ ((module) => {

module.exports = import("axios");;

/***/ }),

/***/ 9527:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ handler)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__]);
axios__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

const BOT_TOKEN = "MTA1ODc2OTg0MDQ2ODQxMDQzOQ.Gy2MWU.ld3ZDBoPk9J59SOXsu2py4CiPf-qg091d8OP1k";
const HTTP_STATUS_NO_CONTENT = 204;
const HTTP_STATUS_CREATED = 201;
const HTTP_STATUS_OK = 200;
const HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
async function handler(req, res) {
    const { userId , token , serverId  } = req.body;
    const payload = {
        access_token: token
    };
    const discord_api_url = `https://discord.com/api/guilds/${serverId}/members/${userId}`;
    const headers = {
        Authorization: `Bot ${BOT_TOKEN}`,
        "Content-Type": "application/json"
    };
    try {
        const response = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].put(discord_api_url, payload, {
            headers
        });
        console.log(response);
        const status = response.status;
        switch(status){
            case HTTP_STATUS_NO_CONTENT:
                return res.status(HTTP_STATUS_OK).json({
                    message: "User is already a member",
                    hasError: false,
                    data: null
                });
            case HTTP_STATUS_CREATED:
                return res.json({
                    message: "User has joined the server",
                    hasError: false,
                    data: response.data
                });
            default:
                return res.status(status).end();
        }
    } catch (error) {
        console.error(error);
        return res.status(error?.response?.status || HTTP_STATUS_INTERNAL_SERVER_ERROR).send({
            hasError: true,
            message: error?.response?.data?.message || error,
            // "An error occurred. try again later or Your Are owner of the server",
            data: null
        });
    }
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(9527));
module.exports = __webpack_exports__;

})();