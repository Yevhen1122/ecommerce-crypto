"use strict";
(() => {
var exports = {};
exports.id = 846;
exports.ids = [846];
exports.modules = {

/***/ 8784:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ email)
});

;// CONCATENATED MODULE: external "@sendgrid/mail"
const mail_namespaceObject = require("@sendgrid/mail");
var mail_default = /*#__PURE__*/__webpack_require__.n(mail_namespaceObject);
;// CONCATENATED MODULE: ./pages/api/email.ts

async function email(req, res) {
    const { to , from , subject , text , html  } = req.body;
    try {
        mail_default().setApiKey(process.env.NEXT_PUBLIC_SENDGRID_API_KEY);
        const msg = {
            to,
            from,
            subject,
            text,
            html
        };
        // Make the SendGrid API request
        await mail_default().send(msg);
        // Return a success message
        res.status(200).json({
            message: "Email sent successfully",
            status: "success"
        });
    } catch (error) {
        const err = error;
        console.log(error);
        if (err.response) {
            console.error(err.response.body);
        }
        // Return an error message
        res.status(500).json({
            message: "Error sending email",
            error: err.response.body.errors[0].message,
            status: "error"
        });
    }
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(8784));
module.exports = __webpack_exports__;

})();