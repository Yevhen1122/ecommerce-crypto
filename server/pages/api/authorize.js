"use strict";
(() => {
var exports = {};
exports.id = 4;
exports.ids = [4];
exports.modules = {

/***/ 2902:
/***/ ((module) => {

module.exports = require("twitter-api-v2");

/***/ }),

/***/ 6382:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ handler)
});

// EXTERNAL MODULE: external "twitter-api-v2"
var external_twitter_api_v2_ = __webpack_require__(2902);
;// CONCATENATED MODULE: ./utils/twitterClient.ts

const CLIENT_SECRET = "_d7jKEsFoRIgbKpDnb3t8WXUHG2ph7OPbP0Bh-OmBVSJp5-idh";
const CLIENT_ID = "UEtSQ2JoVHlGcUpDZmgwSGs0U2U6MTpjaQ";
const twitterClient = new external_twitter_api_v2_.TwitterApi({
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET
});
/* harmony default export */ const utils_twitterClient = (twitterClient);

;// CONCATENATED MODULE: ./pages/api/authorize.ts

async function handler(req, res) {
    const { method  } = req;
    const CALLBACK_URL = process.env.NEXT_PUBLIC_TWITTER_AUTH_URL || "";
    switch(method){
        case "GET":
            const details = await utils_twitterClient.generateOAuth2AuthLink(CALLBACK_URL, {
                scope: [
                    "tweet.read",
                    "tweet.write",
                    "users.read",
                    "offline.access",
                    "follows.read",
                    "follows.write"
                ]
            });
            res.status(200).json(details);
            break;
        case "POST":
            const { codeVerifier , state , code , sessionState  } = req.body;
            if (!codeVerifier || !state || !sessionState || !code) {
                return res.status(400).send("You denied the app or your session expired!");
            }
            if (state !== sessionState) {
                return res.status(400).send("Stored tokens didnt match!");
            }
            // Obtain access token
            utils_twitterClient.loginWithOAuth2({
                code,
                codeVerifier,
                redirectUri: CALLBACK_URL
            }).then(async ({ client: loggedClient , accessToken , refreshToken , expiresIn  })=>{
                console.log(accessToken, "accessToken");
                const { data: userObject  } = await loggedClient.v2.me();
                console.log(userObject);
                return res.status(200).json({
                    client: loggedClient,
                    accessToken,
                    refreshToken,
                    expiresIn,
                    currentUser: userObject
                });
            }).catch((err)=>res.status(403).json({
                    msg: "Invalid verifier or access tokens!",
                    err
                }));
            break;
    }
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(6382));
module.exports = __webpack_exports__;

})();