"use strict";
(() => {
var exports = {};
exports.id = 500;
exports.ids = [500];
exports.modules = {

/***/ 9648:
/***/ ((module) => {

module.exports = import("axios");;

/***/ }),

/***/ 7862:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ handler)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9648);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([axios__WEBPACK_IMPORTED_MODULE_0__]);
axios__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

const key = process.env.NEXT_PUBLIC_FIREBASE_MESSAGING_KEY;
async function handler(req, res) {
    const { to , notificationPayload  } = req.body;
    try {
        const data = await sendNotification(key, to, notificationPayload);
        res.status(200).json({
            message: "Notification sent successfully",
            data
        });
    } catch (error) {
        res.status(500).json({
            error: error.message
        });
    }
}
const sendNotification = async (key, to, notification)=>{
    try {
        const response = await axios__WEBPACK_IMPORTED_MODULE_0__["default"].post("https://fcm.googleapis.com/fcm/send", {
            notification,
            to
        }, {
            headers: {
                Authorization: "key=" + key,
                "Content-Type": "application/json"
            }
        });
        console.log(response.data);
        return response.data;
    } catch (error) {
        console.error(error);
    }
};

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(7862));
module.exports = __webpack_exports__;

})();