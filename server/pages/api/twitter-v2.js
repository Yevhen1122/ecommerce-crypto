"use strict";
(() => {
var exports = {};
exports.id = 936;
exports.ids = [936];
exports.modules = {

/***/ 2902:
/***/ ((module) => {

module.exports = require("twitter-api-v2");

/***/ }),

/***/ 7533:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ handler)
});

// EXTERNAL MODULE: external "twitter-api-v2"
var external_twitter_api_v2_ = __webpack_require__(2902);
;// CONCATENATED MODULE: ./utils/twitterMethods.ts

const initializeClient = (token)=>{
    return new external_twitter_api_v2_.TwitterApi(token);
};
/* harmony default export */ const twitterMethods = ({
    async tweet (status, token) {
        const twitterClient = await initializeClient(token);
        const createdTweet = await twitterClient.v2.tweet(status);
        return createdTweet;
    },
    async retweet (loggedUserId, tweetId, token) {
        debugger;
        console.log(token, loggedUserId, tweetId);
        const twitterClient = await initializeClient(token);
        const reTweet = await twitterClient.v2.retweet(loggedUserId, tweetId);
        return reTweet;
    },
    async followUser (loggedUserId, userId, token) {
        console.log(token);
        const twitterClient = await initializeClient(token);
        const followedUser = await twitterClient.v2.follow(loggedUserId, userId);
        return followedUser;
    },
    async getUserID (token, username) {
        const twitterClient = await initializeClient(token);
        const userName = await twitterClient.v2.userByUsername(username);
        return userName;
    },
    async isFollowingUser (token, userId) {
        debugger;
        const twitterClient = await initializeClient(token);
        const followingUser = await twitterClient.v2.following(userId);
        return followingUser;
    },
    async getRefreshToken (refreshToken, token) {
        debugger;
        const twitterClient = await initializeClient(token);
        const refreshedClient = await twitterClient.refreshOAuth2Token(refreshToken);
        return refreshedClient;
    }
});

;// CONCATENATED MODULE: ./pages/api/twitter-v2.ts

async function handler(req, res) {
    try {
        const { method  } = req.body;
        switch(method){
            case "userInfo":
                {
                    const { username , token  } = req.body;
                    const userName = await twitterMethods.getUserID(token, username);
                    res.status(200).json(userName);
                    break;
                }
            case "refreshToken":
                {
                    const { refreshToken , token: token1  } = req.body;
                    const userName1 = await twitterMethods.getRefreshToken(refreshToken, token1);
                    res.status(200).json(userName1);
                    break;
                }
            case "tweet":
                {
                    const { status , token: token2  } = req.body;
                    console.log(token2, "tweet");
                    const createdTweet = await twitterMethods.tweet(status, token2);
                    res.status(201).json(createdTweet);
                    break;
                }
            case "retweet":
                {
                    console.log("inside retweet");
                    const { loggedUserId , tweetId , token: token3  } = req.body;
                    const reTweet = await twitterMethods.retweet(loggedUserId, tweetId, token3);
                    res.status(200).json(reTweet);
                    break;
                }
            case "followUser":
                {
                    const { loggedUserId: loggedUserId1 , userId , token: token4  } = req.body;
                    const followedUser = await twitterMethods.followUser(loggedUserId1, userId, token4);
                    res.status(200).json(followedUser);
                    break;
                }
            case "isFollowing":
                {
                    const { userId: userId1 , token: token5  } = req.body;
                    const followedUser1 = await twitterMethods.isFollowingUser(token5, userId1);
                    res.status(200).json(followedUser1);
                    break;
                }
            default:
                res.setHeader("Allow", [
                    "POST",
                    "PUT",
                    "PATCH"
                ]);
                res.status(405).end(`Method ${method} Not Allowed`);
        }
    } catch (error) {
        debugger;
        console.error(error);
        res.status(500).json({
            error
        });
    }
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(7533));
module.exports = __webpack_exports__;

})();