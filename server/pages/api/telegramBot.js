"use strict";
(() => {
var exports = {};
exports.id = 650;
exports.ids = [650];
exports.modules = {

/***/ 2509:
/***/ ((module) => {

module.exports = require("firebase-admin");

/***/ }),

/***/ 832:
/***/ ((module) => {

module.exports = require("telegraf");

/***/ }),

/***/ 3263:
/***/ ((module) => {

module.exports = import("firebase-admin/app");;

/***/ }),

/***/ 2929:
/***/ ((module) => {

module.exports = import("firebase-admin/firestore");;

/***/ }),

/***/ 144:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "d": () => (/* binding */ firebaseAdminDb)
/* harmony export */ });
/* unused harmony export firebaseAdminApp */
/* harmony import */ var firebase_admin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2509);
/* harmony import */ var firebase_admin__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_admin__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3263);
/* harmony import */ var firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2929);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__, firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__]);
([firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__, firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);
const keys = __webpack_require__(5117);



const firebaseAdminApp = (0,firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__.getApps)().length > 0 ? (0,firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__.getApps)()[0] : (0,firebase_admin_app__WEBPACK_IMPORTED_MODULE_1__.initializeApp)({
    credential: firebase_admin__WEBPACK_IMPORTED_MODULE_0__.credential.cert(keys)
});
const firebaseAdminDb = (0,firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__.getFirestore)(firebaseAdminApp);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7433:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var telegraf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(832);
/* harmony import */ var telegraf__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(telegraf__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _firebase_admin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(144);
/* harmony import */ var firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2929);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_firebase_admin__WEBPACK_IMPORTED_MODULE_1__, firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__]);
([_firebase_admin__WEBPACK_IMPORTED_MODULE_1__, firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);



const bot = new telegraf__WEBPACK_IMPORTED_MODULE_0__.Telegraf(process.env.NEXT_PUBLIC_TELEGRAM_BOT_TOKEN ?? "");
// Keep track of users who have sent the correct referral ID
const userIdsMap = new Map();
const findReferalIdsUserByReferralId = async (key)=>{
    const usersRef = _firebase_admin__WEBPACK_IMPORTED_MODULE_1__/* .firebaseAdminDb.collection */ .d.collection("users");
    const querySnapshot = await usersRef.where(`referralIds.${key}`, "==", false).get();
    if (querySnapshot.empty) {
        return null;
    }
    const userDoc = querySnapshot.docs[0];
    return userDoc.data().referralIds;
};
const updateReferralId = async (key)=>{
    try {
        const usersRef = _firebase_admin__WEBPACK_IMPORTED_MODULE_1__/* .firebaseAdminDb.collection */ .d.collection("users");
        const querySnapshot = await usersRef.where(`referralIds.${key}`, "==", false).get();
        if (!querySnapshot.empty) {
            const userDoc = querySnapshot.docs[0];
            await userDoc.ref.update({
                [`referralIds.${key}`]: true
            });
        } else {
            console.log("No user found with referral key", key);
        }
    } catch (error) {
        console.log("updating error", error);
    }
};
const updateEntries = async (key)=>{
    try {
        const usersRef = _firebase_admin__WEBPACK_IMPORTED_MODULE_1__/* .firebaseAdminDb.collection */ .d.collection("users");
        const giveawayRef = _firebase_admin__WEBPACK_IMPORTED_MODULE_1__/* .firebaseAdminDb.collection */ .d.collection("giveaway");
        const querySnapshotUser = await usersRef.where(`referralIds.${key}`, "==", true).get();
        const querySnapshotGiveaway = await giveawayRef.where(`referralIds.${key}`, "==", false).get();
        if (!querySnapshotUser.empty && !querySnapshotGiveaway.empty) {
            const userDocId = querySnapshotUser.docs[0].id;
            const userDocRef = usersRef.doc(userDocId);
            const giveawayDocId = querySnapshotGiveaway.docs[0].id;
            const giveawayDocRef = await giveawayRef.doc(giveawayDocId).get();
            const giveaway = giveawayDocRef.data();
            const numberOfEntries = giveaway?.tasks.filter((task)=>task.title === "Follow on Telegram")[0]?.noOfEntries;
            await usersRef.doc(userDocId).update({
                participatedGiveaways: firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__.FieldValue.arrayUnion({
                    taskTitle: "Follow on Telegram",
                    noOfEntries: numberOfEntries,
                    giveawayId: giveawayDocId
                })
            });
            console.log("update user", (await userDocRef.get()).data());
            const currentUser = (await userDocRef.get()).data();
            console.log({
                currentUser
            });
            const userEntries = Array.isArray(giveaway?.participatedUsers) ? giveaway?.participatedUsers.length && giveaway?.participatedUsers.length > 0 ? giveaway?.participatedUsers.filter((user)=>user.userId === currentUser?.uid)[0]?.userEntries : undefined : undefined;
            await giveawayRef.doc(giveawayDocId).update({
                totalEntries: firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__.FieldValue.increment(Number(numberOfEntries)),
                participatedUsers: firebase_admin_firestore__WEBPACK_IMPORTED_MODULE_2__.FieldValue.arrayUnion({
                    userId: currentUser?.uid,
                    name: currentUser?.name,
                    email: currentUser?.email,
                    userEntries: userEntries || 0 + numberOfEntries
                })
            });
            console.log("update giveaway", giveawayDocRef.data());
        } else {
            console.log("user not found");
        }
    } catch (error) {
        console.log("error while updating entries", error);
    }
};
bot.on("new_chat_members", (context)=>{
    const newMembers = context.message.new_chat_members || [];
    const welcomMessage = `Welcome ${newMembers.map((member)=>member.first_name).join(", ")}`;
    context.reply(welcomMessage);
    context.reply("What is your referral id?");
    // Add user id to referral ID map with initial value of false
    newMembers.forEach((member)=>{
        userIdsMap.set(member.id, false);
    });
});
bot.hears(/^.+$/, async (context)=>{
    const text = context.message.text;
    const userId = context.from?.id;
    const chatId = context.chat?.id;
    if (context.chat?.type === "private") {
        return;
    }
    if (!userId) {
        return;
    }
    if (chatId) {
        const chatMember = await context.telegram.getChatMember(chatId, userId);
        if (chatMember.status === "administrator" || chatMember.status === "creator") {
            return;
        }
    }
    const hasSentCorrectReferralId = userIdsMap.get(userId) ?? false;
    if (new RegExp(/^\/join\/(.+)$/).test(text)) {
        const referralId = text.split("/join/")[1].trim();
        const referralIds = await findReferalIdsUserByReferralId(referralId);
        if (referralIds) {
            const len = Object.values(referralIds).length;
            if (len > 0 && referralIds[referralId] === false) {
                userIdsMap.set(userId, true);
                await updateReferralId(referralId);
                await updateEntries(referralId);
                context.reply("Thank you");
            } else {
                context.reply("Please enter the referral id with /join");
            }
        }
    } else if (!hasSentCorrectReferralId) {
        const referralIds1 = await findReferalIdsUserByReferralId(text);
        console.log({
            referralIds: referralIds1
        });
        if (referralIds1) {
            const len1 = Object.values(referralIds1).length;
            if (len1 > 0 && referralIds1[text] === false) {
                userIdsMap.set(userId, true);
                await updateReferralId(text);
                await updateEntries(text);
                context.reply("Thank you");
            } else {
                context.reply("Please enter the referral id");
            }
        }
    }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    await bot.handleUpdate(req.body);
    res.status(200).json({
        ok: true
    });
});

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [117], () => (__webpack_exec__(7433)));
module.exports = __webpack_exports__;

})();